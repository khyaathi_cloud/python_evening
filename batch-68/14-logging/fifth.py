#!/usr/bin/python
# disk monitor

import logging

# create logger
logger = logging.getLogger('disk_monitor') # logger
logger.setLevel(logging.DEBUG)               # filter for logger.

# create console handler and set level to debug
ch = logging.FileHandler('disk_log.txt')                 # handler
ch.setLevel(logging.DEBUG)                   # filter for handler

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter) # handler and formatter

# add ch to logger
logger.addHandler(ch)      # logger and handler

'''
l.basicConfig(filename='disk_log.txt',filemode='a',level=l.DEBUG,
			  format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
			  datefmt='%c')
* Loggers expose the interface that application code directly uses.
logging module -> root
l.basicConfig -> root is our logger.

* Handlers send the log records (created by loggers) to the appropriate destination.
ex:filename='disk_log.txt',filemode='a'
reference: https://docs.python.org/2/howto/logging.html#useful-handlers	

* Filters provide a finer grained facility for determining which log records to output.
ex: level=l.DEBUG,

* Formatters specify the layout of log records in the final output.
ex:format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',datefmt='%c'

'''

disk_size = int(raw_input("please enter  your disk size:"))

if disk_size < 60:
	logger.info("Your disk looks healthy at {}.".format(disk_size))
elif disk_size < 80:
	logger.warning("Buddy!! your disk is getting fat - {}.".format(disk_size))
elif disk_size < 90:
	logger.error("Buddy!! you disk is feeling sick - {}.".format(disk_size))
elif disk_size < 99:
	logger.critical("Buddy!! you disk is dead - {}.".format(disk_size))