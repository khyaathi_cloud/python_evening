#!/usr/bin/python
# disk monitor
# lh.SysLogHandler?
# https://docs.python.org/2/library/subprocess.html
# crontab or scheduler


import logging
from logging.handlers import SysLogHandler
from subprocess import Popen,PIPE

# create logger
logger = logging.getLogger('disk_monitor') # logger
logger.setLevel(logging.DEBUG)               # filter for logger.

# create console handler and set level to debug
ch = SysLogHandler(address="/dev/log")                 # handler
ch.setLevel(logging.DEBUG)                   # filter for handler

# create formatter
formatter = logging.Formatter('- %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter) # handler and formatter

# add ch to logger
logger.addHandler(ch)      # logger and handler

# df -h /|tail -n 1|awk '{print $5}'|sed -e 's#%##g'

# disk_size = int(raw_input("please enter  your disk size:"))

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_size = int(p2.communicate()[0].split()[-2].split('%')[0])

if disk_size < 60:
	logger.info("Your disk looks healthy at {}.".format(disk_size))
elif disk_size < 80:
	logger.warning("Buddy!! your disk is getting fat - {}.".format(disk_size))
elif disk_size < 90:
	logger.error("Buddy!! you disk is feeling sick - {}.".format(disk_size))
elif disk_size < 99:
	logger.critical("Buddy!! you disk is dead - {}.".format(disk_size))