#!/usr/bin/python
# bookmyshow or bar shop
# i need to borrow Exception class
# Exception is my parent class.
# InvalidAge is my child class.

# child class/sub class
class InvalidAge(Exception):
	def __init__(self,age):
		self.age = age


def validate_age(age):
	if age >= 18:
		return "Buddy!! you have come to age!!"
	else:
		raise InvalidAge(age)

## Main
age = input("please enter your age:")

try:
	validate_age(age)
except Exception as e:
	print "Buddy you are yet to come to age - {}".format(e.age)
else:
	print validate_age(age)


'''
In [1]: raise SyntaxError
  File "<string>", line unknown
SyntaxError


In [2]: raise santosh
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-2-460ec4d018fb> in <module>()
----> 1 raise santosh

NameError: name 'santosh' is not defined

In [3]: raise InvalidAge
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-3-475778e0fcbc> in <module>()
----> 1 raise InvalidAge

NameError: name 'InvalidAge' is not defined

'''




