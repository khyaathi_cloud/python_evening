#!/usr/bin/python

balance = 0

def my_deposit():
	global balance
	balance = balance + 1000
	return balance

def my_withdraw():
	global balance
	balance = balance - 200
	return balance


## main

# navya
print "Inital balance of navya - {}".format(balance)
my_deposit()
print "Balance of navya after deposit - {}".format(balance)
my_withdraw()
print "Balance of navya after withdraw - {}".format(balance)

# naresh
print "Inital balance of naresh - {}".format(balance)