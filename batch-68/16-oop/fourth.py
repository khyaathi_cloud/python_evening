#!/usr/bin/python
# inheritance
# the constructor caannot he inherited implicitly,
# you have to explicitly call it.

### General account.
# parent class/super class
class Account:							#classobj/class/blueprint
	def __init__(self):					# special method/constructor
		self.balance = 0				# data/instance variable
	def my_withdraw(self,amount):				# method
		self.balance = self.balance - amount
		return self.balance
	def my_deposit(self,amount):
		self.balance = self.balance + amount
		return self.balance
	def my_balance(self):
		return "my balance is {}".format(self.balance)

### Min balance account - 1000 ###
# child class/sub class
class MinBalanceAccount(Account):
	def __init__(self):
		Account.__init__(self)    # please inherit the constructor from parent class.
	def my_withdraw(self,amount):
		self.amount = amount
		if self.balance - amount < 1000:
			return "Buddy!!! time to call you daddy."
		else:
			Account.my_withdraw(self,amount)


###
# asish   - working - zero balance account for salaried people.
asish = Account()
print dir(asish)
# salary time
asish.my_deposit(10000)
# balance
print asish.my_balance()
# holi
asish.my_withdraw(3000)
asish.my_withdraw(3000)
asish.my_withdraw(3000)
asish.my_withdraw(3000)
asish.my_withdraw(3000)
# balance
print asish.my_balance()


# akaarsh - student - NO zero balance account.
akaarsh = MinBalanceAccount()
print dir(akaarsh)
# daddy time
akaarsh.my_deposit(10000)
# balance
print akaarsh.my_balance()
# summer sale.
akaarsh.my_withdraw(3000)
# balance
print akaarsh.my_balance()
# summer sale.
akaarsh.my_withdraw(3000)
# summer sale.
akaarsh.my_withdraw(3000)
# balance
print akaarsh.my_balance()
# my_withdraw
print akaarsh.my_withdraw(3000)