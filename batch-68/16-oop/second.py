#!/usr/bin/python

def my_account():
	return {'balance':0}

def my_deposit(account):
	account['balance'] = account['balance'] + 1000
	return account['balance']

def my_withdraw(account):
	account['balance'] = account['balance']  - 200
	return account['balance']

## main
navya = my_account()
print navya
print "Initial balance of navya - {}".format(navya['balance'])
my_deposit(navya)
print "Balance of navya after deposit - {}".format(navya['balance'])
my_withdraw(navya)
print "Balance of navya after withdraw - {}".format(navya['balance'])

naresh = my_account()
print naresh