#!/usr/bin/python
# http://radek.io/2011/07/21/private-protected-and-public-in-python/
# https://importantshock.wordpress.com/2006/11/03/adventures-in-pythonic-encapsulation/

# example on private
class Cup:
    def __init__(self, color):
        self._color = color    # protected variable
        self.__content = None  # private variable

    def fill(self, beverage):
        self.__content = beverage

    def empty(self):
        self.__content = None

    def half_full(self):
    	return "my {} cup is having {}".format(self._color,self.__content)

## Case I

Redcup = Cup('red')
Redcup.fill('chai')
print Redcup.half_full()

## Case II:
print Redcup._color
print Redcup._Cup__content #  name mangling.
print Redcup.__content
