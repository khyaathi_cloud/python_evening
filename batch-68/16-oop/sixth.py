#!/usr/bin/python
# reference:https://rszalski.github.io/magicmethods/
# polymorphism

'''
__str__(self)
    Defines behavior for when str() is called on an instance of your class.
__repr__(self)
    Defines behavior for when repr() is called on an instance of your class. The major difference between 
    str() and repr() is intended audience. repr() is intended to produce output that is mostly machine-readable
    (in many cases, it could be valid Python code even), whereas str() is intended to be human-readable.


In [5]: 1 + 1
Out[5]: 2

In [6]: '1' + '1'
Out[6]: '11'

In [7]: 1/2 + 1/3
Out[7]: 0

In [8]: # 1/2 + 1/3 = 3 + 2/(2*3) = 5/6

In [9]: 1.0/2 + 1.0/3
Out[9]: 0.8333333333333333

In [10]: type(1)
Out[10]: int

In [11]: # 1 is an instance of integer class

In [12]: type('a')
Out[12]: str

In [13]: # a is an instance of string class

In [14]: 1 + 1
Out[14]: 2

In [15]: 1.__add__(1)
  File "<ipython-input-15-5b24dfd0e180>", line 1
    1.__add__(1)
            ^
SyntaxError: invalid syntax


In [16]: a = 1

In [17]: b = 1

In [18]: a.__add__(b)
Out[18]: 2
'''

class RationalNumber:
    """
    Rational Numbers with support for arthmetic operations.

        >>> a = RationalNumber(1, 2)
        >>> b = RationalNumber(1, 3)
        >>> a + b
        5/6
    """
    def __init__(self, numerator, denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self, other):
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)

        n = self.n * other.d + self.d * other.n
        d = self.d * other.d
        return RationalNumber(n, d)

    def __str__(self):
        return "%s/%s" % (self.n, self.d)

    __repr__ = __str__


## MAIN

## case 1:
a = RationalNumber(1, 2)  # self.n = 1, self.d = 2
b = RationalNumber(1, 3)  # other.n = 1, other.d = 3
						  # n = self.n * other.d + self.d * other.n = 5
						  # d = self.d * other.d = 6
						  # RationalNumber(n, d) => self.n=5,self.d=6 => 5/6

print a + b ## a.__add__(b)  => a.__add__(other)

## case 2:

c = 12
d = 13
print c + d

## case 3:
e = RationalNumber(1, 2)  # self.n = 1, self.d = 2 => 1/2
f = 12  # 12 is an instance of a integer class. => f = RationalNumber(12,1)   # 12/1              
print e + f # e.__add__(f)