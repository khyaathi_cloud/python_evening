#!/usr/bin/python
# Abstraction

# blueprint or a class
# Account is a class.
# constructor is the method which gets called implicitly the minute you create instance.
# if we want to pass arguments to a instance we can create a constructor.

# class Account(object):
class Account:							#classobj/class/blueprint
	def __init__(self):					# special method/constructor
		self.balance = 0				# data/instance variable
	def my_withdraw(self):				# method
		self.balance = self.balance - 200
		return self.balance
	def my_deposit(self):
		self.balance = self.balance + 1000
		return self.balance
	def my_balance(self):
		return "my balance is {}".format(self.balance)


# main

raju = Account()
raju.my_deposit()
raju.my_deposit()
raju.my_deposit()
raju.my_deposit()
print raju.my_balance()

balu = Account()
print balu.my_balance()