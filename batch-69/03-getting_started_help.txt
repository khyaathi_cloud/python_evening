-- my first program --

khyaathi@khyaathipython:~$ pwd
/home/khyaathi
khyaathi@khyaathipython:~$ ls
a1.py  app1.py  a.py     Desktop    Downloads         hr.py  Pictures  pur.py          sales.py   test.py
a.csv  app.py   demo.py  Documents  examples.desktop  Music  Public    pythonexamples  Templates  Videos
khyaathi@khyaathipython:~$ cd Documents/
khyaathi@khyaathipython:~/Documents$ ls
git_repos
khyaathi@khyaathipython:~/Documents$ cd git_repos/
khyaathi@khyaathipython:~/Documents/git_repos$ ls
bitbucket  projects  python-batches
khyaathi@khyaathipython:~/Documents/git_repos$ cd bitbucket/
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket$ ls
python-evenings  python-weekend
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket$ cd python-evenings/
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings$ ls
archieves  batch-68  batch-69
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings$ cd batch-69/
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ls
01-Opensource.txt  02-Installation.txt  03-getting_started_help.txt  first.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ python first.py
hello world !!
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ perl first.py
hello world !!khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ 
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ 
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ls -l
total 12
-rw-r--r-- 1 khyaathi khyaathi 1478 Mar  7 20:09 01-Opensource.txt
-rw-r--r-- 1 khyaathi khyaathi  895 Mar  9 18:05 02-Installation.txt
-rw-r--r-- 1 khyaathi khyaathi    0 Mar  9 18:05 03-getting_started_help.txt
-rw-r--r-- 1 khyaathi khyaathi   22 Mar  9 18:11 first.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ chmod +x first.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ls -l
total 12
-rw-r--r-- 1 khyaathi khyaathi 1478 Mar  7 20:09 01-Opensource.txt
-rw-r--r-- 1 khyaathi khyaathi  895 Mar  9 18:05 02-Installation.txt
-rw-r--r-- 1 khyaathi khyaathi    0 Mar  9 18:05 03-getting_started_help.txt
-rwxr-xr-x 1 khyaathi khyaathi   22 Mar  9 18:11 first.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ./first.py 
Error: no such file "hello world !!"
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ python --version
Python 2.7.12+
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ which python
/usr/bin/python
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ /usr/bin/python --version
Python 2.7.12+
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ./first.py 
hello world !!
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ./first.py 
hello world !!
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ which python3
/usr/bin/python3
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ./first.py 
  File "./first.py", line 2
    print "hello world !!"
                         ^
SyntaxError: Missing parentheses in call to 'print'
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ./first.py 
hello world !!
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ ./first.py 
hello world !!
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ 


-- getting started and help --- 

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ python
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
[GCC 6.2.0 20160914] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> help()

Welcome to Python 2.7!  This is the online help utility.

If this is your first time using Python, you should definitely check out
the tutorial on the Internet at http://docs.python.org/2.7/tutorial/.

Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules.  To quit this help utility and
return to the interpreter, just type "quit".

To get a list of available modules, keywords, or topics, type "modules",
"keywords", or "topics".  Each module also comes with a one-line summary
of what it does; to list the modules whose summaries contain a given word
such as "spam", type "modules spam".

help> topics

Here is a list of available topics.  Enter any topic name to get more help.

ASSERTION           DEBUGGING           LITERALS            SEQUENCEMETHODS2
ASSIGNMENT          DELETION            LOOPING             SEQUENCES
ATTRIBUTEMETHODS    DICTIONARIES        MAPPINGMETHODS      SHIFTING
ATTRIBUTES          DICTIONARYLITERALS  MAPPINGS            SLICINGS
AUGMENTEDASSIGNMENT DYNAMICFEATURES     METHODS             SPECIALATTRIBUTES
BACKQUOTES          ELLIPSIS            MODULES             SPECIALIDENTIFIERS
BASICMETHODS        EXCEPTIONS          NAMESPACES          SPECIALMETHODS
BINARY              EXECUTION           NONE                STRINGMETHODS
BITWISE             EXPRESSIONS         NUMBERMETHODS       STRINGS
BOOLEAN             FILES               NUMBERS             SUBSCRIPTS
CALLABLEMETHODS     FLOAT               OBJECTS             TRACEBACKS
CALLS               FORMATTING          OPERATORS           TRUTHVALUE
CLASSES             FRAMEOBJECTS        PACKAGES            TUPLELITERALS
CODEOBJECTS         FRAMES              POWER               TUPLES
COERCIONS           FUNCTIONS           PRECEDENCE          TYPEOBJECTS
COMPARISON          IDENTIFIERS         PRINTING            TYPES
COMPLEX             IMPORTING           PRIVATENAMES        UNARY
CONDITIONAL         INTEGER             RETURNING           UNICODE
CONTEXTMANAGERS     LISTLITERALS        SCOPING             
CONVERSIONS         LISTS               SEQUENCEMETHODS1    

help> LISTS

Related help topics: LISTLITERALS

help> keywords

Here is a list of the Python keywords.  Enter any keyword to get more help.

and                 elif                if                  print
as                  else                import              raise
assert              except              in                  return
break               exec                is                  try
class               finally             lambda              while
continue            for                 not                 with
def                 from                or                  yield
del                 global              pass                

help> modules

Please wait a moment while I gather a list of all available modules...
-- truncated -- 

cherrypy            linecache           requests_oauthlib   yaml
chunk               locale              resource            zdaemon
classytags          logbook             rlcompleter         zinnia
click               logging             rmagic              zipapp
click_plugins       lsb_release         rsa                 zipfile
cligj               lxml                ruffus              zipimport
clonevirtualenv     lzma                runpy               zipline
cmath               macpath             sched               zlib
cmd                 macurl2path         scipy               zmq
cms                 magic               scratchpad          zodbpickle
Enter any module name to get more help.  Or, type "modules spam" to search
for modules whose name or summary contain the string "spam".

help> quit

You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)".  Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.

-- ipython --

In [1]: # variables in python don't need typecasting

In [2]: my_string = "python"

In [3]: print type(my_string)
<type 'str'>

In [4]: my_value = 1.0

In [5]: print type(my_value)
<type 'float'>

In [6]: my_value = 2

In [7]: print type(my_value)
<type 'int'>

In [8]: my_value = '2.0'

In [9]: print type(my_value)
<type 'str'>

In [10]: my_string.<tab>
                my_string.capitalize my_string.endswith   my_string.isalnum    my_string.istitle    my_string.lstrip      
                my_string.center     my_string.expandtabs my_string.isalpha    my_string.isupper    my_string.partition   
                my_string.count      my_string.find       my_string.isdigit    my_string.join       my_string.replace    >
                my_string.decode     my_string.format     my_string.islower    my_string.ljust      my_string.rfind       
                my_string.encode     my_string.index      my_string.isspace    my_string.lower      my_string.rindex      

In [10]: my_string.upper?
Docstring:
S.upper() -> string

Return a copy of the string S converted to uppercase.
Type:      builtin_function_or_method

In [11]: my_string.upper()
Out[11]: 'PYTHON'

In [12]: my_string.capitalize?
Docstring:
S.capitalize() -> string

Return a copy of the string S with only its first character
capitalized.
Type:      builtin_function_or_method

In [13]: my_string.capitalize()
Out[13]: 'Python'

In [14]: print my_string
python

n [17]: my_value=2.0

In [18]: my_value.
                   my_value.as_integer_ratio my_value.hex              my_value.real             
                   my_value.conjugate        my_value.imag                                       
                   my_value.fromhex          my_value.is_integer                                 
In [19]: my_value = 2

In [20]: my_value.
                   my_value.bit_length  my_value.imag        
                   my_value.conjugate   my_value.numerator   
                   my_value.denominator my_value.real        



-- python --

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69$ python
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
[GCC 6.2.0 20160914] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> my_string="python"
>>> print dir(my_string)
['__add__', '__class__', '__contains__', '__delattr__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__getslice__', '__gt__', '__hash__', '__init__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '_formatter_field_name_split', '_formatter_parser', 'capitalize', 'center', 'count', 'decode', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'index', 'isalnum', 'isalpha', 'isdigit', 'islower', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']
>>> 
>>> print help(my_string.upper)

None
>>> print help(my_string.center)

None
>>> my_string.center(10,'*')
'**python**'
>>> my_string.center(11,'*')
'***python**'
>>> 

## garbage collector