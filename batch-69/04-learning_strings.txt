In [1]: my_string = "python"

In [2]: print type(my_string)
<type 'str'>

In [3]: # indexing

In [4]: # p  y  t  h  o  n

In [5]: # 0  1  2  3  4  5  # +ve index # left to right

In [6]: # -6 -5 -4 -3 -2 -1 # -ve index # right to left

In [7]: my_string[2]
Out[7]: 't'

In [8]: my_string[-1]
Out[8]: 'n'

In [9]: my_string[0]
Out[9]: 'p'

In [10]: my_string[0]='P'
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-10-fa91f1c6e022> in <module>()
----> 1 my_string[0]='P'

TypeError: 'str' object does not support item assignment

In [11]: # slicing

In [12]: my_string[0:3] # 0 till 3 ( 3 excluded)
Out[12]: 'pyt'

In [13]: my_string[:3] # 0 till 3 ( 3 excluded)
Out[13]: 'pyt'

In [14]: my_string[3:6]
Out[14]: 'hon'

In [15]: my_string[3:]
Out[15]: 'hon'

In [16]: my_string[-1:-3]
Out[16]: ''

In [17]: my_string[-3:]
Out[17]: 'hon'

In [18]: my_string[-6:-4]
Out[18]: 'py'

In [19]: my_string[-6:-3]
Out[19]: 'pyt'

In [20]: # concatination

In [21]: my_string[:3] + my_string[3:]
Out[21]: 'python'

In [25]: # task1

In [26]: my_string = "python"

In [27]: # output : Tython

In [28]: 'T' + my_string
Out[28]: 'Tpython'

In [29]: 'T' + my_string[1:]
Out[29]: 'Tython'

In [30]: my_string[2].upper() + my_string[1:]
Out[30]: 'Tython'

In [31]: my_string.replace?
Docstring:
S.replace(old, new[, count]) -> string

Return a copy of string S with all occurrences of substring
old replaced by new.  If the optional argument count is
given, only the first count occurrences are replaced.
Type:      builtin_function_or_method

In [32]: my_string.replace('p','T')
Out[32]: 'Tython'

In [33]: '1' + '1'
Out[33]: '11'

In [34]: 1 + 1
Out[34]: 2

In [35]: '1' + 2
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-35-20b2518fe802> in <module>()
----> 1 '1' + 2

TypeError: cannot concatenate 'str' and 'int' objects

In [37]: # slicing

In [38]: my_string
Out[38]: 'python'

In [39]: my_string[:]
Out[39]: 'python'

In [40]: my_string[0:]
Out[40]: 'python'

In [41]: my_string[0:6]
Out[41]: 'python'

In [42]: my_string[0:6:1]
Out[42]: 'python'

In [43]: my_string[0:6:2]
Out[43]: 'pto'

In [44]: my_string[1:6:2]
Out[44]: 'yhn'

In [45]: my_string[::-1]
Out[45]: 'nohtyp'

In [46]: # i=6;i<=0;i--

In [47]: my_string[::-2]
Out[47]: 'nhy'

In [48]: my_string[-2]
Out[48]: 'o'

In [66]: # printing

In [67]: my_school = "De Paul"

In [68]: another_school = "St. xaviers"

In [69]: my_town = "township"

In [70]: my_beach = "blue"

In [71]: commute = "bus"

In [72]: print "my school name is my_school"
my school name is my_school

In [73]: print 'my school name is my_school'
my school name is my_school

In [74]: print 'my school name is ', my_school , "there used to be another school", another_school , "we used to live in a 
    ...: small " , my_town , 'we have a beach which is having color', my_beach , "we used to commute on ", commute
my school name is  De Paul there used to be another school St. xaviers we used to live in a small  township we have a beach which is having color blue we used to commute on  bus

In [75]: # typecasting - %d,%f,%r,%s

In [76]: print "my school name is %s.There used to be another school %s.We used to live in a small %s.We have a beach which
    ...:  has color %s.We used to commute on %s." %(my_school,another_school,my_town,my_beach,commute)
my school name is De Paul.There used to be another school St. xaviers.We used to live in a small township.We have a beach which has color blue.We used to commute on bus.

In [77]: print "my school name is %s.I love my %s" %(my_school,my_school)
my school name is De Paul.I love my De Paul

In [78]: print "my school name is %s.I love my %s" %(my_school,another_school)
my school name is De Paul.I love my St. xaviers

In [79]: # format

In [80]: print "my school name is {}.i love my {}".format(my_school,another_school)
my school name is De Paul.i love my St. xaviers

In [82]: print "my school name is {}.i love my {}".format(my_school,another_school)
my school name is De Paul.i love my St. xaviers

In [83]: print "my school name is {0}.i love my {0}".format(my_school,another_school)
my school name is De Paul.i love my De Paul

In [84]: print "my school name is {1}.i love my {1}".format(my_school,another_school)
my school name is St. xaviers.i love my St. xaviers

In [85]: print "my school name is {1}.i love my {2}".format(my_school,another_school)
---------------------------------------------------------------------------
IndexError                                Traceback (most recent call last)
<ipython-input-85-5b39fa0f11a2> in <module>()
----> 1 print "my school name is {1}.i love my {2}".format(my_school,another_school)

IndexError: tuple index out of range

In [86]: print "my school name is {ms}.i love my {ms}".format(ms=my_school,ans=another_school)
my school name is De Paul.i love my De Paul

In [87]: print "my school name is {ans}.i love my {ans}".format(ms=my_school,ans=another_school)
my school name is St. xaviers.i love my St. xaviers

In [89]: # raw_input and input

In [90]: name = raw_input("please enter your name:")
please enter your name:kumar

In [91]: print name,type
kumar <type 'type'>

In [92]: print name,type(name)
kumar <type 'str'>

In [93]: age = raw_input("what is your age:")
what is your age:56

In [94]: print age,type(age)
56 <type 'str'>

In [95]: # int(),str(),float(),repr()

In [96]: age = int(raw_input("what is your age?")
    ...: )
what is your age?56

In [97]: print age,type(age)
56 <type 'int'>

In [98]: # raw_input => 2.x not in 3.x

In [99]: # input

In [100]: age = input("please enter your age:")
please enter your age:56

In [101]: print age,type(age)
56 <type 'int'>

In [102]: name = input("please enter your name:")
please enter your name:"vijay"

In [103]: print name
vijay

In [104]: print name,type(name)
vijay <type 'str'>

In [105]: name = input("please enter your name:")
please enter your name:name

In [106]: print name
vijay

In [107]: my_string="python"

In [108]: name = input("please enter your name:")
please enter your name:my_string

In [109]: print name,type(name)
python <type 'str'>

