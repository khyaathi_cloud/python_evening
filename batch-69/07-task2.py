#!/usr/bin/python
my_fruits = ['apple','banana','apple','banana','apple','banana','cherry']
# hardcopy => my_fruits[:]
print id(my_fruits)
print id(my_fruits[:])
print my_fruits
print my_fruits[:]
'''
output
my_dupli = ['apple','banana']
my_fruits = ['apple','banana','cherry']
'''

my_dupli = []
for value in my_fruits[:]:
	if my_fruits.count(value) > 1:
		if value not in my_dupli:
			my_dupli.append(value)
		my_fruits.remove(value)

print "my_dupli:{}".format(my_dupli)
print "my_fruits:{}".format(my_fruits)