
# coding: utf-8

# In[1]:


# Function
# A function is a block of statement,which can be called n number of times.


# In[3]:


# defination of the function
def my_fun():
    print "hello world!!"


# In[20]:


print my_fun
print type(my_fun)
my_fun


# In[4]:


# A function always have a return value.
# if your function is not returning a value.. its going to return None.
print my_fun()


# In[6]:


# defination of the function
def my_fun():
    return "hello world!!"


# In[7]:


# A function always have a return value.
# if your function is not returning a value.. its going to return None.
# return marks the end of function.
# return is not a print statement.
print my_fun()


# In[8]:


# defination of the function
def my_fun():
    return "hello world!!"
    print "line1"
    print "line2"
    print "line3"
    print "line4"


# In[9]:


print my_fun()


# In[10]:


# Local variables/scope/namespace
# any variable you define within a function,is restricted to the function.
# the variable is available during the run time of the function.
# syntatically ,there is no way you can access the local variable of any function.


# In[13]:


def my_fun1():
    x = 10
    return x


# In[14]:


print my_fun1() # 10


# In[15]:


print x #10,error,


# In[16]:


# locals() - inbuild function called locals()
def my_fun1():
    x = 10
    print locals()
    return x


# In[21]:


print my_fun1()


# In[18]:


##
y = 10
def my_fun1():
    print locals()
    return y


# In[19]:


# locally , i dont have any variable called y.
# if my local space has no value, i will look for my global namespace.
print my_fun1()


# In[22]:


## global namespace is also a dictionary containing all values.
# globals()
'''
In [1]: x = 1

In [2]: def foo():
   ...:     pass
   ...: 
In [4]: globals()
Out[4]: 
{'In': ['',
  u'x = 1',
  u'def foo():\n    pass',
  u'print globals()',
  u'globals()'],
 'Out': {},
 '_': '',
 '__': '',
 '___': '',
 '__builtin__': <module '__builtin__' (built-in)>,
 '__builtins__': <module '__builtin__' (built-in)>,
 '__doc__': 'Automatically created module for IPython interactive environment',
 '__name__': '__main__',
 '_dh': [u'/home/khyaathi'],
 '_i': u'print globals()',
 '_i1': u'x = 1',
 '_i2': u'def foo():\n    pass',
 '_i3': u'print globals()',
 '_i4': u'globals()',
 '_ih': ['',
  u'x = 1',
  u'def foo():\n    pass',
  u'print globals()',
  u'globals()'],
 '_ii': u'def foo():\n    pass',
 '_iii': u'x = 1',
 '_oh': {},
 '_sh': <module 'IPython.core.shadowns' from '/usr/local/lib/python2.7/dist-packages/IPython/core/shadowns.pyc'>,
 'exit': <IPython.core.autocall.ExitAutocall at 0x7f2918c221d0>,
 'foo': <function __main__.foo>,
 'get_ipython': <bound method TerminalInteractiveShell.get_ipython of <IPython.terminal.interactiveshell.TerminalInteractiveShell object at 0x7f2918c5d550>>,
 'quit': <IPython.core.autocall.ExitAutocall at 0x7f2918c221d0>,
 'x': 1}

'''


# In[23]:


## use case III:
# local variables are given higher precedence then global variables.
y = 10
def my_fun1():
    y = 2
    print locals()
    return y


# In[24]:


print my_fun1() # 2,(2,10)


# In[25]:


print y # 10


# In[1]:


# global keyword


# In[2]:


balance = 0

def my_deposit():
    balance = balance + 1000
    return balance

def my_withdraw():
    balance = balance - 200
    return balance

print my_deposit()


# In[4]:


# use case - II : not happy with the balance.
balance = 0

def my_deposit():
    balance = 0
    balance = balance + 1000
    return balance

def my_withdraw():
    balance = 0
    balance = balance - 200
    return balance

print my_deposit()
print my_withdraw()


# In[7]:


#case III: global keyword

balance = 0

def my_deposit():
    global balance
    print locals()
    balance = balance + 1000
    return balance

def my_withdraw():
    global balance
    print locals()
    balance = balance - 200
    return balance

print balance
print my_deposit()
print balance
print my_withdraw()
print balance


# In[11]:


# functional parameters

def my_add(a,b):
    return a + b

# passing arguments position wise
print my_add(10,11)
print my_add('python',' rocks')
print my_add(' rocks','python')
# passing arguments key wise
print my_add(b=' rocks',a='python')


# In[12]:


# default arguments
# note default is not a keyword its a variable.
def multi(num,default=10):
    for value in range(1,default+1):
        print "{} * {} = {}".format(num,value,num*value)


# In[15]:


multi(5)
multi(3,5)


# In[ ]:


# putty
def putty(ip,port=22):
    pass

#  putty(google.com)
#  putty(google.com,23)


# In[ ]:


# *,**,*args,**kwargs


# In[16]:


def my_add(a,b):
    return a + b


# In[17]:


# *
my_values = [11,12]
# a = my_value[0],b=my_value[1]
# my_add(a,b)
print my_add(*my_values)


# In[19]:


print my_add(my_values)


# In[ ]:


# task - google.com
# the arguments passed to the function are passed as objects not as reference
# or values


# In[21]:


def my_add(a,b):
    return a + b

# **
my_values = {'a':11,'b':22}
print my_add(my_values)


# In[22]:


print my_add(**my_values)


# In[23]:


# *args


# In[24]:


print help(max)


# In[27]:


print max(1,55,72,43,4)
print max(-1,-4)
print max(-1,-4,0)


# In[31]:


# defination
def gmax(*args):
    return args


# In[32]:


print gmax(1,55,72,43,4)
print gmax(-1,-4)
print gmax(-1,-4,0)


# In[35]:


def gmax(*args):
    big=-1
    for value in args:
        if value > big:
            big = value
    return big


# In[36]:


print gmax(1,55,72,43,4)
print gmax(-1,-4)
print gmax(-1,-4,0)


# In[38]:


## **kwargs
def callme(**kwargs):
    return kwargs

print callme(name='kumar',gender='m')
print callme(name='kumar',maiden='vijaya')
print callme(city='hyd',location='kphb')


# In[41]:


def callme(**kwargs):
    if 'name' in kwargs:
        print "my name is {}".format(kwargs['name'])
    if 'gender' in kwargs:
        print "my gender is {}".format(kwargs['gender'])
    if 'maiden' in kwargs:
        print "my mother name is {}".format(kwargs['maiden'])
    if 'city' in kwargs:
        print "my city name is {}".format(kwargs['city'])
    if 'location' in kwargs:
        print "my location is {}".format(kwargs['location'])
    return '---------'


# In[43]:


callme(name='kumar',gender='m')
callme(name='kumar',maiden='vijaya')
callme(city='hyd',location='kphb',name='satish')


# In[45]:


## function within a function

def upper():
    x = 1
    def lower(): # defination of lower function
        return x
    return lower() # we are calling the lower function

# Main
print upper() # 1


# In[46]:


print lower()


# In[47]:


## function closures - decorators
# the function remembers both the global and local variables during the
# defination of a function.
def upper():
    x = 1
    def lower():
        return x
    return lower # address of the lower


# In[50]:


# the return value of a function can be saved in a variable
new = upper()
print type(new)
print new


# In[51]:


'''
    def lower():
        return x
'''
print new() # now i am calling the function lower.


# In[1]:


## map,filter and lambda


# In[2]:


# map
print help(map)


# In[3]:


def square(a):
    return a * a


# In[5]:


print square(2)
print square(5)


# In[6]:


map(square,[22,33,44,55,66,77,88,99])


# In[7]:


## filter


# In[8]:


print help(filter)


# In[9]:


def my_even(a):
    if a % 2 == 0:
        return 'even'


# In[11]:


print my_even(2) # true
print my_even(3) # false


# In[12]:


print filter(my_even,[22,33,44,55,66,77,88,99])


# In[13]:


print filter(square,[22,33,44,55,66,77,88,99])


# In[14]:


print map(my_even,[22,33,44,55,66,77,88,99])


# In[17]:


# lambda - creating functions on fly - nameless functions.

'''
def square(a):
    return a * a
'''

print map(square,[22,33,44,55,66,77,88,99])
print map(lambda a:a*a,[22,33,44,55,66,77,88,99])


# In[19]:


'''
def my_even(a):
    if a % 2 == 0:
        return 'even'

'''
print filter(my_even,[22,33,44,55,66,77,88,99])
print filter(lambda a:a%2==0,[22,33,44,55,66,77,88,99])

