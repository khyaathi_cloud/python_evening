#!/usr/bin/python

version = 2.0

def my_add(a,b):
	''' This is for addition of num and strings '''
	return a + b

def my_sub(a,b):
	''' This is for substraction of two numbers '''
	if a > b:
		return a - b
	else:
		return b - a

def my_div(a,b):
	''' This is for division of two numbers'''
	return a/b

def my_multi(a,b):
	''' This is for multiplication of two numbers '''
	return a * b

## Main
if __name__ == '__main__':  # everything above this line get imported.
							# everything below this line doesnt get imported.
	print "DHOOM .. BOMB LAUNCHED."