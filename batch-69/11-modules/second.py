#!/usr/bin/python

import sys
sys.path.insert(0,'/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-69/11-modules/extra')

import first as f

def my_add(a,b):
	''' add both integers a,b '''
	a = int(a)
	b = int(b)
	return a + b

# main

print "addition of two numbers is {}".format(my_add(11,12))
print "addition of two string is {}".format(f.my_add('linux',' rocks'))