
# coding: utf-8

# In[ ]:


# exceptions
# unexpected erros
# not to exit out of the program.


# In[1]:


print 1/0


# In[2]:


print name


# In[3]:


import exceptions as e


# In[4]:


print dir(e)


# In[5]:


# try..else..except..finally


# In[6]:


# case I
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print "result is {}".format(result)


# In[7]:


# case II
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print "result is {}".format(result)


# In[8]:


# case III
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print "result is {}".format(result)


# In[9]:


# try - which contains code which is mandatory or can give errors.
# else - if try is true(no exceptions).. then go to else.
# except - what to do if i hit on an exception. 


# In[12]:


# case I
# except is a broad term
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except: 
    print "please make sure your numbers are integers and non-zero"
else:
    print "result is {}".format(result)


# In[16]:


# case II
# except is a broad term
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except (ValueError,ZeroDivisionError): 
    print "please make sure your numbers are integers and non-zero"
else:
    print "result is {}".format(result)


# In[18]:


# case III
# except is a broad term
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please make sure your numbers are integers."
except ZeroDivisionError: 
    print "Please make sure  your denominator is non-zero"
else:
    print "result is {}".format(result)


# In[ ]:


# try - which contains code which is mandatory or can give errors.
# else - if try is true(no exceptions).. then go to else.
# except - what to do if i hit on an exception. 
# finally - 
# case I: all the values are valid - try..else..finally.
# case II: values are invalid.. but handled by exception - try..except..finally
# case III:values are invalid..not handled by exceptions - try..finally..bomed with error.


# In[23]:


try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please make sure your numbers are integers."
else:
    print "result is {}".format(result)
finally:
    print "all is well"
    # db.close()
    # socket.close()
    # file.close()


# In[24]:


## raise
## we can only raise for values which are part of exception class.


# In[25]:


raise SyntaxError


# In[26]:


raise SyntaxError,"please clean your glasses"


# In[27]:


raise santosh


# In[29]:


class santosh(Exception):
    pass


# In[30]:


raise santosh,"i am back"


# In[ ]:


#reference: https://docs.python.org/2.7/tutorial/errors.html

