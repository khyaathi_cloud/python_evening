#!/usr/bin/python
#  l.basicConfig?
#  l.Formatter?
# CRITICAL > ERROR > WARNING > INFO > DEBUG
#  subprocess - os based commands
# handlers: https://docs.python.org/2/howto/logging.html#useful-handlers
import logging
from subprocess import Popen,PIPE

#l.basicConfig(filename='disk_logs.txt',filemode='a',level=l.DEBUG
#			  ,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
#			  ,datefmt='%c')


'''
# Loggers expose the interface that application code directly uses.
# ex: root
# Handlers send the log records (created by loggers) to the appropriate destination.
# ex:filename='disk_logs.txt',filemode='a' 
# Filters provide a finer grained facility for determining which log records to output.
# ex:level=l.DEBUG
# Formatters specify the layout of log records in the final output.
# ex: format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',datefmt='%c'
'''

# create logger
logger = logging.getLogger('Disk Monitor') # logger
logger.setLevel(logging.DEBUG) 			   # filter for logger.

# create console handler and set level to debug
ch = logging.StreamHandler()  # handler
ch.setLevel(logging.DEBUG)    # filter at handler level also.

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter) # handler and formatter

# add ch to logger
logger.addHandler(ch)      # logger and handler


# disk_size = $(df -h  / | tail -n 1|awk '{print $5}'|sed -e 's#%##g')

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_size = int(p2.communicate()[0].split()[-2].split('%')[0])
#print disk_size


if __name__ == '__main__':
	#disk_size = int(raw_input("please enter the disk size:"))
	if disk_size < 50:
		logger.info("your disk looks happy at - {}.".format(disk_size))
	elif disk_size < 75:
		logger.warning("Seems your disk is getting heavy - {}.".format(disk_size))
	elif disk_size < 90:
		logger.error("Seems  your application is about to sleep - {}.".format(disk_size))
	elif disk_size < 100:
		logger.critical("Your disk is in coma - {}".format(disk_size))


'''
* i can only redirect my logs to a file or a console.
* i cannot redirect multiple application logs to a single file.
'''


