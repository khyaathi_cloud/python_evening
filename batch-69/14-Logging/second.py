#!/usr/bin/python
#  l.basicConfig?
#  l.Formatter?
#  man date or time.strftime().
# CRITICAL > ERROR > WARNING > INFO > DEBUG
# crontab or scheduler
import logging as l

l.basicConfig(filename='mylogs.txt',filemode='a',level=l.DEBUG
			  ,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
			  ,datefmt='%c')

l.debug("This is a debug message.")
l.info("This is a information message.")
l.warning("This is a warning message.")
l.error("This is a error message.")
l.critical("This is a critical message.")


'''
hyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69/14-Logging$ python first.py 
WARNING:root:This is a warning message.
ERROR:root:This is a error message.
CRITICAL:root:This is a critical message.
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-69/14-Logging$ 

The default level is WARNING, which means that only events of this level and
above will be tracked, unless the logging package is configured to do otherwise.


'''