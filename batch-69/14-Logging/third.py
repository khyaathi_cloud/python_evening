#!/usr/bin/python
#  l.basicConfig?
#  l.Formatter?
# CRITICAL > ERROR > WARNING > INFO > DEBUG
#  subprocess - os based commands
import logging as l
from subprocess import Popen,PIPE

l.basicConfig(filename='disk_logs.txt',filemode='a',level=l.DEBUG
			  ,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
			  ,datefmt='%c')

# disk_size = $(df -h  / | tail -n 1|awk '{print $5}'|sed -e 's#%##g')

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_size = int(p2.communicate()[0].split()[-2].split('%')[0])
#print disk_size


if __name__ == '__main__':
	#disk_size = int(raw_input("please enter the disk size:"))
	if disk_size < 50:
		l.info("your disk looks happy at - {}.".format(disk_size))
	elif disk_size < 75:
		l.warning("Seems your disk is getting heavy - {}.".format(disk_size))
	elif disk_size < 90:
		l.error("Seems  your application is about to sleep - {}.".format(disk_size))
	elif disk_size < 100:
		l.critical("Your disk is in coma - {}".format(disk_size))


'''
* i can only redirect my logs to a file or a console.
* i cannot redirect multiple application logs to a single file.
'''


