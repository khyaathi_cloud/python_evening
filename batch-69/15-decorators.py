#!/usr/bin/python
# function
# function closures
# @ is a decorator
#  http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
# https://github.com/tuxfux-hlp/Python-examples/blob/master/devops/fabfile.py

# function closures

def upper(my_func):
	def lower(*args,**kwargs):
		try:
			my_func(*args,**kwargs)
		except Exception as e:
			return "exception - {}".format(e)
		else:
			return my_func(*args,**kwargs)
	return lower


@upper
def my_add(a,b):
	return a + b

@upper
def my_div(a,b):
	return a/b
	
@upper
def my_multi(a,b):
	return a * b

# old school
# my_add = upper(my_add)
# print my_add,type(my_add)

print my_add(1,3)
print my_add(b=' rocks',a='linux')
print my_add(a=1,b="rocks")

print my_div(10,2)
print my_div(10,0)

print my_multi(2,3)
print my_multi("linux","rocks")


'''
def my_add(a,b):
	try:
		a + b
	except Exception as e:
		return "exception - {}".format(e)
	else:
		return a + b

def my_div(a,b):
	try:
		a/b
	except Exception as e:
		return "exception - {}".format(e)
	else:
		return a/b


## main

print my_div(10,2)
print my_div(10,0)

print my_add(1,3)
print my_add(b=' rocks',a='linux')
print my_add(a=1,b="rocks")

'''

