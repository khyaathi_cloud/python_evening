Files
- persistent storage of data.
- read,write and append.

In [1]: pwd
Out[1]: u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-69/16-files'

In [2]: ls
16-Learning_files.txt  file1.txt

In [3]: f = open('file1.txt')

In [4]: print f,type(f)
<open file 'file1.txt', mode 'r' at 0x7f4878a8be40> <type 'file'>

In [5]: 


Modes:

r -> read - you can only read the file.
w -> write -> you can write into a file.
* If a file doesnt exits it get created.
* if there a file existing it get truncated to zero.
a -> append mode - end of the file.
b -> binary mode. (ex: notepad or wordpad)
* rb,wb,ab
r+ -> read and write mode.

In [6]: f = open("file1.txt","r")

In [7]: # f.read

In [8]: f.read?
Type:        builtin_function_or_method
String form: <built-in method read of file object at 0x7f4878a8bed0>
Docstring:
read([size]) -> read at most size bytes, returned as a string.

If the size argument is negative or omitted, read until EOF is reached.
Notice that when in non-blocking mode, less data than what was requested
may be returned, even if no size parameter was given.

In [9]: f.read(2)
Out[9]: 'Th'

In [10]: f.read(2)
Out[10]: 'is'

In [11]: f.read(2)
Out[11]: ' i'

In [12]: f.read()
Out[12]: 's my line1.\nThis is my line2.\nThis is my line3.\nThis is my line4.\nThis is my line5.'

In [13]: f.read()
Out[13]: ''

In [14]: print f.read()


In [15]: # f.seek

In [16]: f.seek?
Type:        builtin_function_or_method
String form: <built-in method seek of file object at 0x7f4878a8bed0>
Docstring:
seek(offset[, whence]) -> None.  Move to new file position.

Argument offset is a byte count.  Optional argument whence defaults to
0 (offset from start of file, offset should be >= 0); other values are 1
(move relative to current position, positive or negative), and 2 (move
relative to end of file, usually negative, although many platforms allow
seeking beyond the end of a file).  If the file is opened in text mode,
only offsets returned by tell() are legal.  Use of other offsets causes
undefined behavior.
Note that not all file objects are seekable.

In [17]: f.seek(0)

In [18]: # f.tell

In [19]: f.tell?
Type:        builtin_function_or_method
String form: <built-in method tell of file object at 0x7f4878a8bed0>
Docstring:   tell() -> current file position, an integer (may be a long integer).

In [20]: f.tell()
Out[20]: 0

In [21]: f.read()
Out[21]: 'This is my line1.\nThis is my line2.\nThis is my line3.\nThis is my line4.\nThis is my line5.'

In [22]: f.tell()
Out[22]: 89


In [24]: # f.readline

In [25]: f.readline?
Type:        builtin_function_or_method
String form: <built-in method readline of file object at 0x7f4878a8bed0>
Docstring:
readline([size]) -> next line from the file, as a string.

Retain newline.  A non-negative size argument limits the maximum
number of bytes to return (an incomplete line may be returned then).
Return an empty string at EOF.

In [26]: f.tell()
Out[26]: 89

In [27]: f.seek(0)

In [28]: f.tell()
Out[28]: 0

In [29]: f.readline()
Out[29]: 'This is my line1.\n'

In [30]: f.readline()
Out[30]: 'This is my line2.\n'

In [31]: f.readline()
Out[31]: 'This is my line3.\n'

In [32]: f.readline()
Out[32]: 'This is my line4.\n'

In [33]: f.readline()
Out[33]: 'This is my line5.'

In [34]: f.readline()
Out[34]: ''

In [36]: # f.readlines

In [37]: f.readlines?
Type:        builtin_function_or_method
String form: <built-in method readlines of file object at 0x7f4878a8bed0>
Docstring:
readlines([size]) -> list of strings, each a line from the file.

Call readline() repeatedly and return a list of the lines so read.
The optional size argument, if given, is an approximate bound on the
total number of bytes in the lines returned.

In [38]: f.seek(0)

In [39]: f.tell()
Out[39]: 0

In [40]: my_lines = f.readlines()

In [41]: print my_lines
['This is my line1.\n', 'This is my line2.\n', 'This is my line3.\n', 'This is my line4.\n', 'This is my line5.']

In [42]: my_lines[-2:]
Out[42]: ['This is my line4.\n', 'This is my line5.']

In [44]: # f.write

In [45]: f.write?
Type:        builtin_function_or_method
String form: <built-in method write of file object at 0x7f4878a8bed0>
Docstring:
write(str) -> None.  Write string str to file.

Note that due to buffering, flush() or close() may be needed before
the file on disk reflects the data written.

In [46]: 

In [46]: g = open("file2.txt","w")

In [48]: g = open("file2.txt","w")

In [49]: # g.write

In [50]: g.write?
Type:        builtin_function_or_method
String form: <built-in method write of file object at 0x7f4878a8bf60>
Docstring:
write(str) -> None.  Write string str to file.

Note that due to buffering, flush() or close() may be needed before
the file on disk reflects the data written.

In [51]: g.write("This is page1.\nThis is page2.\nThis is page3.\nThis is page4.\nThis is page5")

In [52]: # input(keyboard) ->  buffer(i/O) -> cpu -> buffer(i/o) -> output(console/file)

In [53]: g
Out[53]: <open file 'file2.txt', mode 'w' at 0x7f4878a8bf60>

In [54]: g.flush()

In [55]: g.flush?
Type:        builtin_function_or_method
String form: <built-in method flush of file object at 0x7f4878a8bf60>
Docstring:   flush() -> None.  Flush the internal I/O buffer.

In [56]: # g.writelines

In [57]: g.writelines?
Type:        builtin_function_or_method
String form: <built-in method writelines of file object at 0x7f4878a8bf60>
Docstring:
writelines(sequence_of_strings) -> None.  Write the strings to the file.

Note that newlines are not added.  The sequence can be any iterable object
producing strings. This is equivalent to calling write() for each string.

In [58]: my_lines
Out[58]: 
['This is my line1.\n',
 'This is my line2.\n',
 'This is my line3.\n',
 'This is my line4.\n',
 'This is my line5.']

In [59]: f
Out[59]: <open file 'file1.txt', mode 'r' at 0x7f4878a8bed0>

In [60]: g
Out[60]: <open file 'file2.txt', mode 'w' at 0x7f4878a8bf60>

In [61]: g.writelines(my_lines)

In [62]: # g.flush or g.close

In [63]: g.close?
Type:        builtin_function_or_method
String form: <built-in method close of file object at 0x7f4878a8bf60>
Docstring:
close() -> None or (perhaps) an integer.  Close the file.

Sets data attribute .closed to True.  A closed file cannot be used for
further I/O operations.  close() may be called more than once without
error.  Some kinds of file objects (for example, opened by popen())
may return an exit status upon closing.

In [64]: g.close()

In [65]: f
Out[65]: <open file 'file1.txt', mode 'r' at 0x7f4878a8bed0>

In [66]: g
Out[66]: <closed file 'file2.txt', mode 'w' at 0x7f4878a8bf60>

In [67]: g.writelines(my_lines)
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-67-2c88fb7dc048> in <module>()
----> 1 g.writelines(my_lines)

ValueError: I/O operation on closed file

In [69]: g
Out[69]: <closed file 'file2.txt', mode 'w' at 0x7f4878a8bf60>

In [70]: g.write("this is my newline")
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-70-0738821258b1> in <module>()
----> 1 g.write("this is my newline")

ValueError: I/O operation on closed file

In [71]: # g.closed

In [72]: g
Out[72]: <closed file 'file2.txt', mode 'w' at 0x7f4878a8bf60>

In [73]: g.closed
Out[73]: True

In [74]: f
Out[74]: <open file 'file1.txt', mode 'r' at 0x7f4878a8bed0>

In [75]: f.closed
Out[75]: False

In [76]: if g.closed:
   ....:     print "the file - {} is closed".format(g.name)
   ....: else:
   ....:     print "the file - {} is not closed".format(g.name)
   ....:     
the file - file2.txt is closed

In [77]: #try..except..else..finally

In [78]: try:
   ....:     g.write("This is my newline")
   ....: except ValueError:
   ....:     print "we cannot write into a closed file"
   ....: else:
   ....:     g.write("This is my newline")
   ....:     g.flush()
   ....: 
we cannot write into a closed file

In [79]: # with

In [80]: g
Out[80]: <closed file 'file2.txt', mode 'w' at 0x7f4878a8bf60>

In [81]: with open("file2.txt","a") as h:
   ....:     h.write("This is a newline")
   ....:     

In [82]: h
Out[82]: <closed file 'file2.txt', mode 'a' at 0x7f48780810c0>

In [83]: g
Out[83]: <closed file 'file2.txt', mode 'w' at 0x7f4878a8bf60>

In [84]: 

In [85]: f.
f.close       f.errors      f.isatty      f.newlines    f.readinto    f.seek        f.truncate    f.xreadlines  
f.closed      f.fileno      f.mode        f.next        f.readline    f.softspace   f.write       
f.encoding    f.flush       f.name        f.read        f.readlines   f.tell        f.writelines  

In [85]: # f.encoding,f.errors,f.fileno,f.newlines,f.softspace

In [86]: # f.xreadlines

In [87]: f.xreadlines?
Type:        builtin_function_or_method
String form: <built-in method xreadlines of file object at 0x7f4878a8bed0>
Docstring:
xreadlines() -> returns self.

For backward compatibility. File objects now include the performance
optimizations previously implemented in the xreadlines module.

In [88]: # f.readinto

In [89]: f.readinto?
Type:        builtin_function_or_method
String form: <built-in method readinto of file object at 0x7f4878a8bed0>
Docstring:   readinto() -> Undocumented.  Don't use this; it may go away.

In [90]: f
Out[90]: <open file 'file1.txt', mode 'r' at 0x7f4878a8bed0>

In [91]: f.seek(0)

In [92]: f.next()
Out[92]: 'This is my line1.\n'

In [93]: f.next()
Out[93]: 'This is my line2.\n'

In [94]: f.next()
Out[94]: 'This is my line3.\n'

In [95]: f.next()
Out[95]: 'This is my line4.\n'

In [96]: f.next()
Out[96]: 'This is my line5.'

In [97]: f.next()
---------------------------------------------------------------------------
StopIteration                             Traceback (most recent call last)
<ipython-input-97-c3e65e5362fb> in <module>()
----> 1 f.next()

StopIteration: 

In [98]: # f.name

In [99]: f.name
Out[99]: 'file1.txt'

In [100]: # f.mode

In [101]: f.mode
Out[101]: 'r'

In [102]: f
Out[102]: <open file 'file1.txt', mode 'r' at 0x7f4878a8bed0>

In [103]: f.isatty()
Out[103]: False

In [104]: g
Out[104]: <closed file 'file2.txt', mode 'w' at 0x7f4878a8bf60>

In [105]: i = open('/dev/pts/4','a')

In [106]: i.write("\n hey there .. having some fun \n")

In [107]: i.flush()

In [108]: i.isatty()
Out[108]: True


## working with excel files.

khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-69/16-files$ ls
16-Learning_files.txt  file1.txt  file2.txt  namesdb.xls
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-69/16-files$ virtualenv myenv
New python executable in /home/khyaathi/Documents/bit-tuxfux/python-evening/batch-69/16-files/myenv/bin/python
Installing setuptools, pip, wheel...done.

khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-69/16-files$ source myenv/bin/activate
(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-69/16-files$ 
(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-69/16-files$ pip install excel
Collecting excel
Collecting xlrd (from excel)
  Using cached xlrd-1.1.0-py2.py3-none-any.whl
Installing collected packages: xlrd, excel
Successfully installed excel-1.0.0 xlrd-1.1.0
(myenv) khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-69/16-files$ 

## saving an object into a file.

In [110]: my_trainings = ['python','django','devops','kubernates']

In [111]: import pickle

In [112]: # pickling

In [113]: # unpickling

In [114]: pickle.load?
Type:        function
String form: <function load at 0x7f487a2b6848>
File:        /usr/lib/python2.7/pickle.py
Definition:  pickle.load(file)
Docstring:   <no docstring>

In [115]: pickle.dump?
Type:        function
String form: <function dump at 0x7f487a2b6758>
File:        /usr/lib/python2.7/pickle.py
Definition:  pickle.dump(obj, file, protocol=None)
Docstring:   <no docstring>

In [116]: f = open("my_training.txt","ab")

In [117]: pickle.dump(my_trainings,f)

In [118]: f.close()

In [119]: # unpickling

In [120]: g = open("my_training.txt","r")

In [121]: new_trainings = pickle.load(g)

In [122]: print new_trainings
['python', 'django', 'devops', 'dockers']

In [123]: # saving your objects into a file.

ex: json,yaml,xml

# json example -- provide reference - TODO
restapi + json


In [125]: import json

In [126]: jsonData = '{"name": "Frank", "age": 39}'

In [127]: jsonToPython = json.loads(jsonData)

In [128]: print jso
json          jsonData      jsonToPython  

In [128]: print jsonToPython
{u'age': 39, u'name': u'Frank'}

In [129]: print type(jsonData)
<type 'str'>

In [130]: print type(jsonToPython)
<type 'dict'>

In [131]: pythonDictionary = {'name':'Bob', 'age':44, 'isEmployed':True}

In [132]: print type(pythonDictionary)
<type 'dict'>

In [133]: dictionaryToJson = json.dumps(pythonDictionary)

In [134]: print dictionaryToJson
{"age": 44, "isEmployed": true, "name": "Bob"}

In [135]: print type(dic
dict              dictionaryToJson  

In [135]: print type(dictionaryToJson)
<type 'str'>


