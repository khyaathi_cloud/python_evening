#!/usr/bin/python

from excel import OpenExcel
f = OpenExcel('namesdb.xls')

print f.read('A') # read 'A' row
print f.read('B') # f.read('1'), read '1' column
print "{} gender is {}".format(f.read('A8'),f.read('B8')) # read 'A5' position 