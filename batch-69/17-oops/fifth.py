#!/usr/bin/python
# constructor is a method
# its the first function which get called implicitly.

class Account:
	def __init__(self):  # constructor
		self.balance = 0 # instance variable
	def my_deposit(self):
		self.balance = self.balance + 5000
		return self.balance
	def my_withdraw(self,amount):
		self.amount=amount
		self.balance = self.balance - self.amount
		return self.balance
	def my_balance(self):
		return "my balance is {}".format(self.balance)

class MinBalAccount(Account):
	def __init__(self):
		Account.__init__(self)
	def my_withdraw(self,amount):
		if self.balance - amount < 1000:
			return "please reach out to your daddy!!"
		else:
			return Account.my_withdraw(self,amount)

# hari - employee
hari = Account()
hari.my_deposit()
hari.my_withdraw(2000) # rent
hari.my_withdraw(2000) # jeans
hari.my_withdraw(2000) # miss
hari.my_withdraw(2000) # miss
print hari.my_balance()

# satish - student

satish = MinBalAccount()
satish.my_deposit()
satish.my_withdraw(2000)
satish.my_withdraw(2000)
print satish.my_withdraw(2000)
print satish.my_balance()