#!/usr/bin/python

balance = 0

def my_deposit():
	global balance
	balance = balance + 5000
	return balance

def my_withdraw():
	global balance
	balance = balance - 300
	return balance

# santosh
print "initial balance of santosh is {}".format(balance)
my_deposit()
print "balance of santosh after deposit {}".format(balance)
my_withdraw()
print "balance of santosh after withdraw {}".format(balance)

# satish
print "initial balance of satish is {}".format(balance)