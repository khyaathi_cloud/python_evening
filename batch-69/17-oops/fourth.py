#!/usr/bin/python
# constructor is a method
# its the first function which get called implicitly.

class Account:
	def __init__(self):  # constructor
		self.balance = 0 # instance variable
	def my_deposit(self):
		self.balance = self.balance + 5000
		return self.balance
	def my_withdraw(self):
		self.balance = self.balance - 1000
		return self.balance
	def my_balance(self):
		return "my balance is {}".format(self.balance)

#print Account,dir(Account)

# shiva
shiva = Account()
shiva.balance = 10000
print shiva.my_balance()
shiva.my_deposit()
print shiva.my_balance()

# priya
priya = Account()
print priya.my_balance()
priya.my_deposit()
priya.my_deposit()
print priya.my_balance()