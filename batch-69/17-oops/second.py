#!/usr/bin/python

def account():
	return {'balance':0}

def my_deposit(account):
	account['balance'] = account["balance"] + 5000
	return account['balance']

def my_withdraw(account):
	account['balance'] = account["balance"] - 300
	return account['balance']

# santosh
santosh = account()
print "initial balance of santosh is {}".format(santosh['balance'])
my_deposit(santosh)
print "balance of santosh after deposit {}".format(santosh['balance'])
my_withdraw(santosh)
print "balance of santosh after withdraw {}".format(santosh['balance'])

# satish
satish = account()
print satish