#!/usr/bin/python
# https://anandology.com/python-practice-book/object_oriented_programming.html#special-class-methods
# https://rszalski.github.io/magicmethods/

class RationalNumber:
    """
    Rational Numbers with support for arthmetic operations.

        >>> a = RationalNumber(1, 2)
        >>> b = RationalNumber(1, 3)
        >>> a + b => a.__add__(b)
        5/6
    """
    def __init__(self, numerator, denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self, other):
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)

        n = self.n * other.d + self.d * other.n
        d = self.d * other.d
        return RationalNumber(n, d)

    def __str__(self):
        return "%s/%s" % (self.n, self.d)

    __repr__ = __str__


## main

# case I
a = RationalNumber(1, 2) # a.n = 1,a.d = 2
b = RationalNumber(1, 3) # b.n = 1,b.d = 3
print a + b

# case II
a = 1
b = 2
print a + b

# case III
a = RationalNumber(1,2)
b = 3 # RationalNumber(3,1)
print a + b

'''
In [36]: isinstance?
Docstring:
isinstance(object, class-or-type-or-tuple) -> bool

Return whether an object is an instance of a class or of a subclass thereof.
With a type as second argument, return whether that is the object's type.
The form using a tuple, isinstance(x, (A, B, ...)), is a shortcut for
isinstance(x, A) or isinstance(x, B) or ... (etc.).
Type:      builtin_function_or_method

In [37]: a = 1

In [38]: type(a)
Out[38]: int

In [39]: isinstance(a,int)
Out[39]: True

In [40]: b = "hari"

In [41]: type(b)
Out[41]: str

In [42]: isinstance(b,int)
Out[42]: False
'''
