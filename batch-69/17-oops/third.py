#!/usr/bin/python

# class Account(object):
class Account:
	balance = 0  # class variable
	def my_bal(self): # method
		return "my balance is {}".format(self.balance)

## challengesr
# i can access variables withour creating a instance.
print Account.balance

# hari
hari = Account()
print hari,type(hari),dir(hari)
print Account,type(Account),dir(Account)

print hari.balance
print hari.my_bal()
'''
# run 1
Traceback (most recent call last):
  File "third.py", line 15, in <module>
    print hari.my_bal()
TypeError: my_bal() takes no arguments (1 given)
## run 2
Traceback (most recent call last):
  File "third.py", line 15, in <module>
    print hari.my_bal()
  File "third.py", line 7, in my_bal
    return "my balance is {}".format(balance)
NameError: global name 'balance' is not defined

'''
# megha
megha = Account()
megha.balance=2000
print megha.my_bal()