#!/usr/bin/python
import MySQLdb as mdb
con = mdb.connect('localhost','user69','user69','batch69')
cur = con.cursor()
cur.execute("create table student (name varchar(10),gender varchar(10))")
con.close()  # closing the connection.


'''

mysql> help create table;
mysql> show tables;
Empty set (0.00 sec)

# python create_table.py

mysql> show tables;
+-------------------+
| Tables_in_batch69 |
+-------------------+
| student           |
+-------------------+
1 row in set (0.00 sec)

mysql> 

mysql> desc student;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| name   | varchar(10) | YES  |     | NULL    |       |
| gender | varchar(10) | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

'''
