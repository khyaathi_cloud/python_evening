#!/usr/bin/python
# usage: guess the right number.
# break: break us out of the while loop.Note: not out of program.
# sys.exit: take us out of the program.

import sys

answer = raw_input("do you want to play the game - y/n :")
if answer == 'n':
	sys.exit()

number = 7  # number in my palm
#test = True

# while loop works on the truth of a condition
#while test:  # test=True , we go into the block.
while True:
	guess_number = input("please guess the number 1-10:")
	if guess_number < number:
		print "Buddy!! the number guessed is slightly smaller."
	elif guess_number > number:
		print "Buddy!! the number guessed is slighly larger."
	elif guess_number == number:
		print "Congo!! you guessed the right number."
		break
		#test = False 

print "Thanks for playing the game!!"

'''
Loop1
 Loop2 
   break

Loop1
 Loop2
   exit


'''