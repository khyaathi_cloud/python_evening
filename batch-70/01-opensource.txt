proprietory vs opensource

proprietory ( owned by someone)
-----------

Company A - product A
          - requirements -> email to the company ( we will get back to you)
                         -> product ( we cannot modify it)
                         -> newsgroups/intenet
                         
* customer retention.
* Frustrated customers.

Opensource
-----------

Company A - product A (GPL) -> published the source code in internet.
          - requirements -> email to the company ( we will get back to you)
                         -> product ( we cannot modify it)
                         -> newsgroups/intenet
                         
GuyB/GalB -> download code -> modification -> Product B(GPL) 
                            * published the source code to internet
                            * internet that i came few modification and a product b
                           -> v.good/good/bad/v.bad
                           
Fast Forward six months:

Company A -> product A.3 ( 100 + 400 ) modification
- homogeneous
- hetrogeneous

- retention of the customer.
- marketing.
- cost cutting.
1 - 1L + 1L (2L) - 400 * 2 = 800L = 8Cr

Money
-------
* support  -> platinum/diamond/gold/silver/bronze
* training -> earning

Licence:
GPL - GNU General Public License
* Any modification done to the code should go back to GPL.
* The code should be published in internet space.
OSF - open source foundation ( 1960)

Principles
----------
* FREEdom to modify the product is called opensource.
* There is nothing like piracy.
* you can modify and redistribute.
* Community support.

Products:
Language: java,.net,python,php
web Frameworks:structs,spring,django,flask,bottle
databases: mariadb,cassendra,mongodb 

Setup done:
- oracle virtualbox (opensource)
- ubuntu (opensource)
- python ( opensource) 

* It available to everyone. - TRUE
* anyone can customize. - TRUE
* Free of cost. - FALSE
* support is easy. - TRUE
* its a freeware,doesnt need license. - FALSE
* we can modify source code. - TRUE
* extenbility of product is good. - TRUE
* community support. - TRUE
* No restriction to edit the code. - TRUE


 - To get access to a application we need  license. - TRUE
 - we dont have control over the source code. - TRUE
 - we need to buy. - TRUE
