1) what is a function.
function is a block of code.

2) why do we need a function.
to stop repetative stuff.

In [234]: def my_func():
     ...:     print "hello world!!!"
     ...:     

In [235]: print my_func()
hello world!!!
None

In [236]: # Every function has a return value.

In [237]: # If you have no return value ,we usually a None.

In [238]: # return marks the end of the function.

In [239]: def my_func():
     ...:     return "Hello world!!!"
     ...: 

In [240]: print my_func()
Hello world!!!

In [241]: # return is not a print statement

In [242]: def my_func():
     ...:     return "hello world"
     ...:     print "hey this is my line1"
     ...:     print "hey this is my line2"
     ...:     print "hey this is my line3"
     ...: 

In [243]: print my_func()
hello world

In [244]: # take the control from the function to the main program.

In [246]: # namespace/local variables/global variables

In [247]: def my_func():
     ...:     z = 1
     ...:     return z
     ...: 

In [248]: print my_func()
1

In [249]: print z
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-249-dac9faf44c36> in <module>()
----> 1 print z

NameError: name 'z' is not defined

In [250]: # z is a local variable.

In [251]: # Local variable is available to us during the runtime of the function.

In [252]: #  there is no syntax in place to get the local variable values outside a function.

In [253]: def my_func():
     ...:     z = 1
     ...:     print locals()
     ...:     return z
     ...: 

In [254]: print my_func()
{'z': 1}
1

In [256]: y = 10

In [257]: def my_func():
     ...:     print locals()
     ...:     return 
     ...: 

In [258]: def my_func():
     ...:     print locals()
     ...:     return y
     ...: 
     ...: 

In [259]: print my_func()
{}
10

In [260]: # if i am not having a local variable,i will look into global space

In [261]: z = 10

In [262]: def my_func():
     ...:     z = 2
     ...:     print locals()
     ...:     return z
     ...: 

In [263]: print my_func() 
{'z': 2}
2

In [264]: # local is given higher precedence then global

In [265]: 

In [256]: y = 10
     ...: 

In [258]: def my_func():
     ...:     print locals()
     ...:     return y
     ...: 
     ...: 

In [259]: print my_func()
{}
10

In [260]: # if i am not having a local variable,i will look into global space

In [261]: z = 10

In [262]: def my_func():
     ...:     z = 2
     ...:     print locals()
     ...:     return z
     ...: 

In [263]: print my_func() 
{'z': 2}
2

In [264]: # local is given higher precedence then global

In [265]: 

In [1]: # globals

In [2]: y = 10

In [3]: globals()
Out[3]: 
{'In': ['', u'# globals', u'y = 10', u'globals()'],
 'Out': {},
 '_': '',
 '__': '',
 '___': '',
 '__builtin__': <module '__builtin__' (built-in)>,
 '__builtins__': <module '__builtin__' (built-in)>,
 '__doc__': 'Automatically created module for IPython interactive environment',
 '__name__': '__main__',
 '_dh': [u'/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70'],
 '_i': u'y = 10',
 '_i1': u'# globals',
 '_i2': u'y = 10',
 '_i3': u'globals()',
 '_ih': ['', u'# globals', u'y = 10', u'globals()'],
 '_ii': u'# globals',
 '_iii': u'',
 '_oh': {},
 '_sh': <module 'IPython.core.shadowns' from '/usr/local/lib/python2.7/dist-packages/IPython/core/shadowns.pyc'>,
 'exit': <IPython.core.autocall.ExitAutocall at 0x7f879d43c2d0>,
 'get_ipython': <bound method TerminalInteractiveShell.get_ipython of <IPython.terminal.interactiveshell.TerminalInteractiveShell object at 0x7f879d4773d0>>,
 'quit': <IPython.core.autocall.ExitAutocall at 0x7f879d43c2d0>,
 'y': 10}

In [4]: print y
10

In [5]: print z
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-5-dac9faf44c36> in <module>()
----> 1 print z

NameError: name 'z' is not defined

In [266]: # global

In [267]: balance = 0

In [268]: def my_deposit():
     ...:     balance = balance + 1000
     ...:     return balance
     ...: 

In [269]: def my_withdraw():
     ...:     balance = balance - 300
     ...:     return balance
     ...: 

In [270]: # hari

In [271]: my_deposit()
---------------------------------------------------------------------------
UnboundLocalError                         Traceback (most recent call last)
<ipython-input-271-a78f69abe684> in <module>()
----> 1 my_deposit()

<ipython-input-268-09abee693763> in my_deposit()
      1 def my_deposit():
----> 2     balance = balance + 1000
      3     return balance

UnboundLocalError: local variable 'balance' referenced before assignment

In [274]: def my_deposit():
     ...:     balance = 0
     ...:     balance = balance + 1000
     ...:     return balance
     ...: 

In [275]: def my_withdraw():
     ...:     balance = 0
     ...:     balance = balance - 300
     ...:     return balance
     ...: 

In [276]: # hari

In [277]: my_deposit()
Out[277]: 1000

In [278]: print balance
0

In [279]: my_withdraw()
Out[279]: -300

In [280]: ## global

In [281]: def my_deposit():
     ...:     global balance
     ...:     print locals()
     ...:     balance = balance + 1000
     ...:     return balance
     ...: 

In [282]: def my_withdraw():
     ...:     global balance
     ...:     print locals()    
     ...:     balance = balance - 300
     ...:     return balance
     ...: 
     ...: 

In [283]: my_deposit()
{}
Out[283]: 1000

In [284]: print balance
1000

In [285]: my_withdraw()
{}
Out[285]: 700

In [286]: print balance
700

In [287]: print my_deposit()
{}
1700

In [288]: print balance
1700

In [289]: 

In [290]: # functional arguments

In [291]: def my_add(a,b):
     ...:     print locals()
     ...:     return a + b
     ...: 

In [292]: print my_add(11,12)
{'a': 11, 'b': 12}
23

In [293]: print my_add("Linux"," rocks")
{'a': 'Linux', 'b': ' rocks'}
Linux rocks

In [294]: print my_add(" rocks","linux")
{'a': ' rocks', 'b': 'linux'}
 rockslinux

In [295]: # key based

In [296]: print my_add(b="linux",a=" rocks")
{'a': ' rocks', 'b': 'linux'}
 rockslinux

In [297]: print my_add(a="linux",c=" rocks")
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-297-75fb482ff6ff> in <module>()
----> 1 print my_add(a="linux",c=" rocks")

TypeError: my_add() got an unexpected keyword argument 'c'

In [298]: # how are arguments passed in python - as a value,as a reference.

In [299]: # the arguments are passed as objects

In [302]: # default arguments

In [303]: def my_multi(num,default=10): # default is not a keyword.its a variable
     ...:     for value in range(1,default+1):
     ...:         print "{0:2d} * {1:2d} ={2:2d}".format(num,value,num*value)
     ...:         

In [304]: my_multi(2)
 2 *  1 = 2
 2 *  2 = 4
 2 *  3 = 6
 2 *  4 = 8
 2 *  5 =10
 2 *  6 =12
 2 *  7 =14
 2 *  8 =16
 2 *  9 =18
 2 * 10 =20

In [305]: my_multi(2,5)
 2 *  1 = 2
 2 *  2 = 4
 2 *  3 = 6
 2 *  4 = 8
 2 *  5 =10

In [306]: # putty

In [307]: # def putty(hostname,port=22)

In [308]: # putty(google.com)

In [309]: # putty(google.com,23)

In [310]: 

In [311]: def my_doubt(hostname,default=10,my_a):
     ...:     return hostname,my_a
  File "<ipython-input-311-f1639b867a78>", line 1
    def my_doubt(hostname,default=10,my_a):
SyntaxError: non-default argument follows default argument


In [312]: 

In [312]: def my_doubt(hostname,my_a,default=10):
     ...:     return hostname,my_a
     ...: 

In [312]: def my_doubt(hostname,my_a,default=10):
     ...:     return hostname,my_a
     ...: 

In [313]: def my_doubt(default=10,hostname,my_a):
     ...:     return hostname,my_a
     ...: 
     ...: 
  File "<ipython-input-313-34649f5f907f>", line 1
    def my_doubt(default=10,hostname,my_a):
SyntaxError: non-default argument follows default argument


In [314]: def my_doubt(hostname,my_a,default=10,default=11):
     ...:     return hostname,my_a
     ...: 
     ...: 
  File "<ipython-input-314-7eaf2c2defe1>", line 1
    def my_doubt(hostname,my_a,default=10,default=11):
SyntaxError: duplicate argument 'default' in function definition


In [315]: def my_doubt(hostname,my_a,default=10,default1=11):
     ...:     return hostname,my_a
     ...: 
     ...: 
     ...: 

In [317]: # *,**,*args,**kwargs

In [318]: # *

In [319]: def my_add(a,b):
     ...:     return a + b
     ...: 

In [320]: my_values = [11,22]

In [321]: print my_add(*my_values)
33

In [322]: my_values1 = [11,22,33]

In [323]: print my_add(*my_values1)
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-323-2b9a8c68eeb9> in <module>()
----> 1 print my_add(*my_values1)

TypeError: my_add() takes exactly 2 arguments (3 given)

In [324]: # **

In [325]: def  my_add(a,b):
     ...:     return a + b
     ...: 

In [326]: my_values = {'a':1,'b':2}

In [327]: print my_add(**my_values)
3

In [328]: my_values = {'a':1,'c':2}

In [329]: print my_add(**my_values)
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-329-8e94aabde286> in <module>()
----> 1 print my_add(**my_values)

TypeError: my_add() got an unexpected keyword argument 'c'

In [330]: 

In [335]: # *args

In [336]: max?
Docstring:
max(iterable[, key=func]) -> value
max(a, b, c, ...[, key=func]) -> value

With a single iterable argument, return its largest item.
With two or more arguments, return the largest argument.
Type:      builtin_function_or_method

In [337]: max(22,33)
Out[337]: 33

In [338]: max(-1,-5,-10)
Out[338]: -1

In [339]: max(22,32,55,33,77,91)
Out[339]: 91

In [340]: def my_big(*args):
     ...:     return args
     ...: 

In [341]: print my_big(22,33)
(22, 33)

In [342]: print my_big(-1,-5,-10)
(-1, -5, -10)

In [343]: print my_big(22,32,55,33,77,91)
(22, 32, 55, 33, 77, 91)

In [344]: def my_big(*args):
     ...:     big = -1
     ...:     for value in args:
     ...:         if value > big:
     ...:             big = value
     ...:     return big
     ...: 

In [345]: print my_big(22,33)
33

In [346]: print my_big(22,32,55,33,77,91)
91

In [347]: print my_big(-1,-5,-10)
-1

In [348]: 

In [349]: # **kwargs

In [350]: def my_callme(**kwargs):
     ...:     return kwargs
     ...: 

In [351]: print my_callme(name='priya',gender='f')
{'gender': 'f', 'name': 'priya'}

In [352]: print my_callme(maiden='vijaya',place='hyd',gender='f')
{'gender': 'f', 'place': 'hyd', 'maiden': 'vijaya'}

In [353]: print my_callme(place='hyd',mobile='x.y.x.z')
{'mobile': 'x.y.x.z', 'place': 'hyd'}

In [354]: def my_callme(**kwargs):
     ...:     if 'name' in kwargs:
     ...:         print "my name is {}".format(kwargs['name'])
     ...:     if 'gender' in kwargs:
     ...:         print "my gender is {}".format(kwargs['gender'])
     ...:     if 'place' in kwargs:
     ...:         print "my place is {}".format(kwargs['place'])
     ...:     if 'maiden' in kwargs:
     ...:         print "my maiden is {}".format(kwargs['maiden'])
     ...:     if 'mobile' in kwargs:
     ...:         print "my mobile is {}".format(kwargs['mobile'])
     ...:     return True
     ...: 

In [355]: print my_callme(place='hyd',mobile='x.y.x.z')
my place is hyd
my mobile is x.y.x.z
True

In [356]: print my_callme(name='priya',gender='f')
my name is priya
my gender is f
True

In [357]: 

In [362]: # function is a first class object

In [363]: def foo():
     ...:     pass
     ...: 

In [364]: print foo
<function foo at 0x7f8a9880c9b0>

In [365]: foo
Out[365]: <function __main__.foo>

In [366]: print type(foo)
<type 'function'>

In [367]: def my_extra(my_func,x,y):
     ...:     return my_func(x,y)
     ...: 

In [368]: def my_add(x,y):
     ...:     return x + y
     ...: 

In [369]: def my_sub(x,y):
     ...:     return x - y
     ...: 

In [370]: print my_extra(my_add,11,22)
33

In [375]: # function within function

In [376]: def my_upper():
     ...:     y = 10
     ...:     def my_lower():
     ...:         return y
     ...:     return my_lower()
     ...: 
     ...: 

In [377]: print my_upper() # 10
10

In [378]: print my_lower() # error,None,{}
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-378-f3720a0d290e> in <module>()
----> 1 print my_lower() # error,None,{}

NameError: name 'my_lower' is not defined

In [379]: 


In [380]: # closure

In [381]: def my_upper():
     ...:     y = 1
     ...:     def my_lower():
     ...:         return y
     ...:     return my_lower
     ...: 

In [382]: def foo():
     ...:     pass
     ...: 

In [383]: foo
Out[383]: <function __main__.foo>

In [384]: print foo
<function foo at 0x7f8a988e9410>

In [385]: print foo()
None

In [386]: new = my_upper()

In [387]: print new
<function my_lower at 0x7f8a9896f398>

In [388]: print new() # error,1,
1

In [389]: ## During the defination of a function,the function remembers all the variable available to it during defination.
     ...: 

In [390]: 


In [391]: # map,filter and lambda

In [392]: def my_square(a):
     ...:     return a * a
     ...: 

In [393]: print my_square(2)
4

In [394]: print my_square(25)
625

In [395]: # Truth of a function - If my function is returning a valid value.

In [396]: map?
Docstring:
map(function, sequence[, sequence, ...]) -> list

Return a list of the results of applying the function to the items of
the argument sequence(s).  If more than one sequence is given, the
function is called with an argument list consisting of the corresponding
item of each sequence, substituting None for missing values when not all
sequences have the same length.  If the function is None, return a list of
the items of the sequence (or a list of tuples if more than one sequence).
Type:      builtin_function_or_method

In [397]: map(my_square,[11,12,13,14,15,16,17])
Out[397]: [121, 144, 169, 196, 225, 256, 289]


In [399]: # filter

In [400]: def my_even(a):
     ...:     if a % 2 == 0:
     ...:         return 'even'
     ...:     

In [401]: print my_even(2)
even

In [402]: print my_even(3)
None

In [403]: filter?
Docstring:
filter(function or None, sequence) -> list, tuple, or string

Return those items of sequence for which function(item) is true.  If
function is None, return the items that are true.  If sequence is a tuple
or string, return the same type, else return a list.
Type:      builtin_function_or_method

In [404]: filter(my_even,[11,12,13,14,15,16,17])
Out[404]: [12, 14, 16]

In [405]: 

In [407]: filter(my_even,[11,12,13,14,15,16,17])
Out[407]: [12, 14, 16]

In [408]: map(my_even,[11,12,13,14,15,16,17])
Out[408]: [None, 'even', None, 'even', None, 'even', None]

In [409]: filter(my_square,[11,12,13,14,15,16,17])
Out[409]: [11, 12, 13, 14, 15, 16, 17]

In [410]: # lambda

In [411]: # creating function on fly

In [412]: # nameless function

In [413]: def my_square(a):
     ...:     return a * a
     ...: 

In [414]: def my_even(a):
     ...:     if a % 2 == 0:
     ...:         return 'even'
     ...:     

In [415]: map(my_square,[11,12,13,14,15,16,17])
Out[415]: [121, 144, 169, 196, 225, 256, 289]

In [416]: map(lambda a:a*a,[11,12,13,14,15,16,17])
Out[416]: [121, 144, 169, 196, 225, 256, 289]

In [417]: filter(my_even,[11,12,13,14,15,16,17])
Out[417]: [12, 14, 16]

In [418]: filter(lambda a:a%2==0,[11,12,13,14,15,16,17])
Out[418]: [12, 14, 16]

In [419]: filter(lambda a:a%2,[11,12,13,14,15,16,17])
Out[419]: [11, 13, 15, 17]

In [420]: filter(lambda a:a%2!=0,[11,12,13,14,15,16,17])
Out[420]: [11, 13, 15, 17]

In [429]: def my_add(a,b):
     ...:     return a + b
     ...: 

In [430]: map?
Docstring:
map(function, sequence[, sequence, ...]) -> list

Return a list of the results of applying the function to the items of
the argument sequence(s).  If more than one sequence is given, the
function is called with an argument list consisting of the corresponding
item of each sequence, substituting None for missing values when not all
sequences have the same length.  If the function is None, return a list of
the items of the sequence (or a list of tuples if more than one sequence).
Type:      builtin_function_or_method

In [431]: map(my_add,[11,22,33,44],[11,22,33,44])
Out[431]: [22, 44, 66, 88]

In [432]: map(lambda a,b:a+b,[11,22,33,44],[11,22,33,44])
Out[432]: [22, 44, 66, 88]

In [433]: 

https://github.com/zhiwehu/Python-programming-exercises/blob/master/100%2B%20Python%20challenging%20programming%20exercises.txt