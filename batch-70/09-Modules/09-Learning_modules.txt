1. What is a modules ?
ans) Module is nothing but a python program.

P1 - f1,f2,f3
P2 - f1,f2,f3,P1.f1


2. why do we need a module ?
ans) - Code reusability.
     - you are not worried about the variable namespace.


3. why do we save our program as a .py format 
ans) if you want to use your program as a module in future. You need to save it in .py format.

khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70$ ipython
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
Type "copyright", "credits" or "license" for more information.

IPython 2.4.1 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: help()

Welcome to Python 2.7!  This is the online help utility.

If this is your first time using Python, you should definitely check out
the tutorial on the Internet at http://docs.python.org/2.7/tutorial/.

Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules.  To quit this help utility and
return to the interpreter, just type "quit".

To get a list of available modules, keywords, or topics, type "modules",
"keywords", or "topics".  Each module also comes with a one-line summary
of what it does; to list the modules whose summaries contain a given word
such as "spam", type "modules spam".

help> modules

Please wait a moment while I gather a list of all available modules...

No handlers could be found for logger "OpenGL.Tk"
/usr/lib/python2.7/dist-packages/matplotlib/cbook.py:137: MatplotlibDeprecationWarning: The matplotlib.delaunay module was deprecated in version 1.4. Use matplotlib.tri.Triangulation instead.
  warnings.warn(message, mplDeprecation, stacklevel=1)
/usr/lib/python2.7/pkgutil.py:110: FutureWarning: The pandas.rpy module is deprecated and will be removed in a future version. We refer to external packages like rpy2. 
See here for a guide on how to port your code to rpy2: http://pandas.pydata.org/pandas-docs/stable/r_interface.html
  __import__(name)

--- truncated output (list of modules) ---

help> quit

You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)".  Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.


2) we can also list the modules by running the following command.
# pip freeze 

###
# How do you import the modules.
###

In [1]: import math

In [2]: math.
math.acos       math.atanh      math.e          math.factorial  math.hypot      math.log10      math.sin        
math.acosh      math.ceil       math.erf        math.floor      math.isinf      math.log1p      math.sinh       
math.asin       math.copysign   math.erfc       math.fmod       math.isnan      math.modf       math.sqrt       
math.asinh      math.cos        math.exp        math.frexp      math.ldexp      math.pi         math.tan        
math.atan       math.cosh       math.expm1      math.fsum       math.lgamma     math.pow        math.tanh       
math.atan2      math.degrees    math.fabs       math.gamma      math.log        math.radians    math.trunc      


In [22]: print dir(math)
['__doc__', '__name__', '__package__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'copysign', 'cos', 'cosh', 'degrees', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'hypot', 'isinf', 'isnan', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'modf', 'pi', 'pow', 'radians', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'trunc']

In [23]: 



In [2]: math.pi?
Type:        float
String form: 3.14159265359
Docstring:
float(x) -> floating point number

Convert a string or number to a floating point number, if possible.

In [3]: math.pi
Out[3]: 3.141592653589793

In [4]: math.pow?
Type:        builtin_function_or_method
String form: <built-in function pow>
Docstring:
pow(x, y)

Return x**y (x to the power of y).

In [5]: math.pow(2,3)
Out[5]: 8.0


In [7]: # importing modules with alias names

In [8]: import math as m

In [9]: m.
m.acos       m.atan2      m.cosh       m.exp        m.fmod       m.isinf      m.log10      m.radians    m.tanh       
m.acosh      m.atanh      m.degrees    m.expm1      m.frexp      m.isnan      m.log1p      m.sin        m.trunc      
m.asin       m.ceil       m.e          m.fabs       m.fsum       m.ldexp      m.modf       m.sinh       
m.asinh      m.copysign   m.erf        m.factorial  m.gamma      m.lgamma     m.pi         m.sqrt       
m.atan       m.cos        m.erfc       m.floor      m.hypot      m.log        m.pow        m.tan        

In [9]: m.pi
Out[9]: 3.141592653589793

In [10]: m.pow(2,3)
Out[10]: 8.0

In [12]: # import only the variables which are needed.

In [13]: # you want to use module names space as your local namespace.

In [14]: from math import pi,pow

In [15]: pi
Out[15]: 3.141592653589793

In [16]: pow?
Type:        builtin_function_or_method
String form: <built-in function pow>
Docstring:
pow(x, y)

Return x**y (x to the power of y).

In [17]: pow(2,3)
Out[17]: 8.0

In [18]: 

In [18]: # DONT TRY THIS AT OFFICE OR HOME

In [19]: # from math import *

In [20]: 

khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ ipython
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
Type "copyright", "credits" or "license" for more information.

IPython 2.4.1 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: ls
09-Learning_modules.txt  first.py

In [2]: pwd
Out[2]: u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules'

In [3]: import first as f

In [4]: f.
f.my_add    f.my_div    f.my_multi  f.my_sub    f.version   

In [4]: f.version
Out[4]: 1.0

In [5]: f.my_add?
Type:        function
String form: <function my_add at 0x7f2210946ed8>
File:        /home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules/first.py
Definition:  f.my_add(a, b)
Docstring:   Add two numbers or string inside the program 

In [6]: f.my_add(11,12)
Out[6]: 23

In [7]: f.my_add("python","rocks")
Out[7]: 'pythonrocks'

In [8]: f.my_multi(22,23)
Out[8]: 506

In [9]: f.version
Out[9]: 1.0

In [10]: ## how about i make a change in my program - first.py

In [11]: f.version
Out[11]: 1.0

In [12]: import first as f

In [13]: f.version
Out[13]: 1.0

In [14]: # 1) you exit  out of ipython and loging again and import the first

In [15]: # not a great way.. because you will loose all your work.

In [16]:  # 2) reload

In [17]: ls
09-Learning_modules.txt  first.py  first.pyc

In [18]: # .pyc is a byte compiled file

In [19]: # this get created whenever you load a  modulue

In [20]: # you can create it using python command line

In [21]: # you can share to third party to use the functionality but cannot see the code

In [22]: 

In [23]: f.version
Out[23]: 1.0

In [24]: pwd
Out[24]: u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules'

In [25]: ls
09-Learning_modules.txt  first.py  first.pyc

In [26]: import first

In [27]: reload(f)
Out[27]: <module 'first' from 'first.py'>

In [28]: f.version
Out[28]: 2.0


khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ ipython
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
Type "copyright", "credits" or "license" for more information.

IPython 2.4.1 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: ls
09-Learning_modules.txt  first.py  first.pyc

In [2]: pwd
Out[2]: u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules'

In [3]: import first as f
launching a missile.

In [4]: f.
f.my_add    f.my_div    f.my_multi  f.my_sub    f.version   

In [4]: # executing the program and importing fuctionalities

In [5]: 

In [5]: def foo():
   ...:     pass
   ...: 

In [6]: print foo
<function foo at 0x7f52778ce230>

In [7]: foo
Out[7]: <function __main__.foo>

In [8]: f.my_add
Out[8]: <function first.my_add>

In [9]: globals()
Out[9]: 
{'In': ['',
  u"get_ipython().magic(u'ls ')",
  u"get_ipython().magic(u'pwd ')",
  u'import first as f',
  u'# executing the program and importing fuctionalities',
  u'def foo():\n    pass',
  u'print foo',
  u'foo',
  u'f.my_add',
  u'globals()'],
 'Out': {2: u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules',
  7: <function __main__.foo>,
  8: <function first.my_add>},
 '_': <function first.my_add>,
 '_2': u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules',
 '_7': <function __main__.foo>,
 '_8': <function first.my_add>,
 '__': <function __main__.foo>,
 '___': u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules',
 '__builtin__': <module '__builtin__' (built-in)>,
 '__builtins__': <module '__builtin__' (built-in)>,
 '__doc__': 'Automatically created module for IPython interactive environment',
 '__name__': '__main__',
 '__package__': None,
 '_dh': [u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules'],
 '_exit_code': 0,
 '_i': u'f.my_add',
 '_i1': u'ls',
 '_i2': u'pwd',
 '_i3': u'import first as f',
 '_i4': u'# executing the program and importing fuctionalities',
 '_i5': u'def foo():\n    pass',
 '_i6': u'print foo',
 '_i7': u'foo',
 '_i8': u'f.my_add',
 '_i9': u'globals()',
 '_ih': ['',
  u"get_ipython().magic(u'ls ')",
  u"get_ipython().magic(u'pwd ')",
  u'import first as f',
  u'# executing the program and importing fuctionalities',
  u'def foo():\n    pass',
  u'print foo',
  u'foo',
  u'f.my_add',
  u'globals()'],
 '_ii': u'foo',
 '_iii': u'print foo',
 '_oh': {2: u'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-70/09-Modules',
  7: <function __main__.foo>,
  8: <function first.my_add>},
 '_sh': <module 'IPython.core.shadowns' from '/usr/lib/python2.7/dist-packages/IPython/core/shadowns.pyc'>,
 'exit': <IPython.core.autocall.ExitAutocall at 0x7f5277fc2510>,
 'f': <module 'first' from 'first.py'>,
 'foo': <function __main__.foo>,
 'get_ipython': <bound method TerminalInteractiveShell.get_ipython of <IPython.terminal.interactiveshell.TerminalInteractiveShell object at 0x7f527c9469d0>>,
 'quit': <IPython.core.autocall


 ### why is second.py not reading extra/first.py module.

 khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ ls
09-Learning_modules.txt  first.py  first.pyc  second.py
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ mkdir extra
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ ls
09-Learning_modules.txt  extra  first.py  first.pyc  second.py
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ mv first.py extra/
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ tree extra
extra
└── first.py

0 directories, 1 file
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ ls
09-Learning_modules.txt  extra  first.pyc  second.py
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ python second.py
addition of two numbers is 33
addition of two strings is pythonrocks
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ rm -f first.pyc
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ 
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ ls
09-Learning_modules.txt  extra  second.py
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ python second.py
Traceback (most recent call last):
  File "second.py", line 3, in <module>
    import first as f
ImportError: No module named first
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-70/09-Modules$ 


In [12]: import sys

In [13]: sys.path
Out[13]: 
['',
 '/usr/bin',
 '/usr/lib/python2.7',
 '/usr/lib/python2.7/plat-x86_64-linux-gnu',
 '/usr/lib/python2.7/lib-tk',
 '/usr/lib/python2.7/lib-old',
 '/usr/lib/python2.7/lib-dynload',
 '/home/khyaathi/.local/lib/python2.7/site-packages',
 '/usr/local/lib/python2.7/dist-packages',
 '/usr/lib/python2.7/dist-packages',
 '/usr/lib/python2.7/dist-packages/PILcompat',
 '/usr/lib/python2.7/dist-packages/gtk-2.0',
 '/usr/lib/python2.7/dist-packages/IPython/extensions',
 '/home/khyaathi/.ipython']

In [14]: # import <modulename> always looks under the sys.path variabel

In [15]: # if i dump my first.py under the folder of sys.path will my second.py work ? yes 

In [16]: # is it recommended ?

In [17]: # no not recommended

In [18]: ## adding the path of the location to you sys.path variable

In [19]: ## tomorrow

In [20]: 

In [1]: import sys

In [2]: sys.path
Out[2]: 
['',
 '/usr/local/bin',
 '/usr/lib/python2.7',
 '/usr/lib/python2.7/plat-x86_64-linux-gnu',
 '/usr/lib/python2.7/lib-tk',
 '/usr/lib/python2.7/lib-old',
 '/usr/lib/python2.7/lib-dynload',
 '/home/khyaathi/.local/lib/python2.7/site-packages',
 '/usr/local/lib/python2.7/dist-packages',
 '/usr/lib/python2.7/dist-packages',
 '/usr/lib/python2.7/dist-packages/PILcompat',
 '/usr/lib/python2.7/dist-packages/gtk-2.0',
 '/usr/local/lib/python2.7/dist-packages/IPython/extensions',
 '/home/khyaathi/.ipython']

In [3]: # should i insert the path at beginning or at the end.

In [4]: sys.path.insert(0,'/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/extra')

In [5]: print sys.path
['/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/extra', '', '/usr/local/bin', '/usr/lib/python2.7', '/usr/lib/python2.7/plat-x86_64-linux-gnu', '/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/home/khyaathi/.local/lib/python2.7/site-packages', '/usr/local/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages/PILcompat', '/usr/lib/python2.7/dist-packages/gtk-2.0', '/usr/local/lib/python2.7/dist-packages/IPython/extensions', '/home/khyaathi/.ipython']

# install modules in  your system

# OS based
# sudo apt-get install <modulename>
# pip install <modulename>
# easy_install <modulename>
# setuptool 

## virtualenv
reference: http://docs.python-guide.org/en/latest/dev/virtualenvs/
# pip install virtualenv

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ virtualenv myenv
New python executable in /home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/myenv/bin/python
Installing setuptools, pip, wheel...done.
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ ls
11-Learning_modules-day02.txt  11-Learning_modules.txt  extra  myenv  second.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ source myenv/bin/activate
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ which python
/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/myenv/bin/python
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ pip freeze
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ 
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ pip install excel
Collecting excel
  Downloading https://files.pythonhosted.org/packages/9d/5e/1420669f433ca41315685fb9bdc6fe2869a6e525cb6483805f3f4c9d61ad/excel-1.0.0.tar.gz
Collecting xlrd (from excel)
  Downloading https://files.pythonhosted.org/packages/07/e6/e95c4eec6221bfd8528bcc4ea252a850bffcc4be88ebc367e23a1a84b0bb/xlrd-1.1.0-py2.py3-none-any.whl (108kB)
    100% |████████████████████████████████| 112kB 1.0MB/s 
Building wheels for collected packages: excel
  Running setup.py bdist_wheel for excel ... done
  Stored in directory: /home/khyaathi/.cache/pip/wheels/19/76/e3/f46c9f0b4594006300affc1b6477ddbb47d3aea96b1dedd9da
Successfully built excel
Installing collected packages: xlrd, excel
Successfully installed excel-1.0.0 xlrd-1.1.0
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ pip freeze
excel==1.0.0
xlrd==1.1.0
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ 


myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ python
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
[GCC 6.2.0 20160914] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import excel
>>> quit
Use quit() or Ctrl-D (i.e. EOF) to exit
>>> quit()
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ deactivate 
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ 

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ ls
11-Learning_modules-day02.txt  11-Learning_modules.txt  extra  myenv  second.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ vi read_excel.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ python read_excel.py 
Traceback (most recent call last):
  File "read_excel.py", line 1, in <module>
    import excel
ImportError: No module named excel
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ source myenv/bin/activate
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ ls
11-Learning_modules-day02.txt  11-Learning_modules.txt  extra  myenv  read_excel.py  second.py
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ python read_excel.py 
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ 
(myenv) khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ deactivate 
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ 


### modules

lin
 -  ip.py
win
 -  ip.py
sol
 -  ip.py
aix
 -  ip.py
hpux
 -  ip.py

1) create a folder structure for your package.

 khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ mkdir OS
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ mkdir OS/{lin,sol,aix,hpux}
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ 

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ tree OS
OS
├── aix
├── hpux
├── lin
└── sol

4 directories, 0 files


2) create four files first.py,second.py,third.py and fourth.py under the lin folder.

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ tree OS
OS
├── aix
├── hpux
├── lin
│   ├── first.py
│   ├── fourth.py
│   ├── second.py
│   └── third.py
└── sol

4 directories, 4 files

3) try to import the lin folder.

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ pwd
/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ cd OS
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS$ ls
aix  hpux  lin  sol
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS$ ipython
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
Type "copyright", "credits" or "license" for more information.

IPython 5.5.0 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: import lin
---------------------------------------------------------------------------
ImportError                               Traceback (most recent call last)
<ipython-input-1-cb672e599c2e> in <module>()
----> 1 import lin

ImportError: No module named lin

In [2]: 

4) create a __init__.py folder under the lin folder.

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ tree OS
OS
├── aix
├── hpux
├── lin
│   ├── first.py
│   ├── fourth.py
│   ├── __init__.py
│   ├── second.py
│   └── third.py
└── sol

4 directories, 5 files

-- __init__.py -- -

import first
import second
import third
import fourth

5) Now try to import the lin folder.

In [2]: pwd
Out[2]: u'/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS'

In [3]: ls
aix/  hpux/  lin/  sol/

In [4]: import lin

In [5]: lin.
             lin.first  lin.second 
             lin.fourth lin.third  

In [4]: import lin

In [5]: lin.first.my_lin1_
                           lin.first.my_lin1_first  lin.first.my_lin1_second 
                           lin.first.my_lin1_fourth lin.first.my_lin1_third  


In [5]: lin.first.my_lin1_first()
Out[5]: 'this is my first lin1 function'

In [6]: lin.first.my_lin1_second()
Out[6]: 'thi is my second lin1 function'

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ tree OS
OS
├── aix
├── hpux
├── lin
│   ├── first.py
│   ├── first.pyc
│   ├── fourth.py
│   ├── fourth.pyc
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── second.py
│   ├── second.pyc
│   ├── third.py
│   └── third.pyc
└── sol

4 directories, 10 files
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules$ 

6) Lets create files first.py,second.py,third.py,foruth.py in sol folder.

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS/lin$ pwd
/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS/lin
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS/lin$ cp *.py ../sol/.
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS/lin$ cd ../sol/
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS/sol$ f()
> for i in first.py second.py third.py fourth.py;do
> sed -i "s#lin#sol#g" $i
> done
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS/sol$ f
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS/sol$ 

## __init__.py ###

import first
import second
import third

7) import sol module

In [8]: pwd
Out[8]: u'/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/OS'

In [9]: ls
aix/  hpux/  lin/  sol/

In [10]: import sol

In [11]: sol.
              sol.first  
              sol.second 
              sol.third 

In [11]: # in above command we imported first,second,third implicitly

In [12]: from sol import fourth

In [13]: sol.
              sol.first  sol.second 
              sol.fourth sol.third  


# in the above we are importing fourth explicitly.


### a real example of a module

khyaathi@khyaathipython:/usr/lib/python2.7/xml$ pwd
/usr/lib/python2.7/xml
khyaathi@khyaathipython:/usr/lib/python2.7/xml$ ls
dom  etree  __init__.pyc  parsers  sax
khyaathi@khyaathipython:/usr/lib/python2.7/xml$ 


reference:
https://docs.python.org/2/distributing/index.html
https://docs.python.org/2/installing/index.html
https://docs.python.org/2/distutils/

## pydoc

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/extra$ pydoc sys

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/extra$ pydoc math

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/extra$ pydoc first

khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-70/11-modules/extra$ 







