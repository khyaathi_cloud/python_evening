#!/usr/bin/python

version = 2.0

def my_add(a,b):
	'''  Add two numbers or string inside the program '''
	return a + b

def my_sub(a,b):
	''' sub the larger number from smaller number '''
	if a > b:
		return a - b
	else:
		return b - a

def my_div(a,b):
	''' Divide the numbers a/b'''
	return a/b

def my_multi(a,b):
	''' Multiply both the numbers a and b'''
	return a * b


## MAIN

if __name__ == '__main__':   # everyting above this line gets imported. 
							 # everything below this line never gets imported.
	print "launching a missile."