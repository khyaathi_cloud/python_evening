#!/usr/bin/python

import first as f

def my_add(a,b):
	''' this function is for addition of numbers '''
	a = int(a)
	b = int(b)
	return a + b

# MAIN
if __name__ == '__main__':
	print "addition of two numbers is {}".format(my_add(11,22))
	print "addition of two strings is {}".format(f.my_add("python","rocks"))