
# coding: utf-8

# In[1]:


# exception handling
# try..except..else..finally


# In[2]:


num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[3]:


# case I
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[4]:


# case II
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[7]:


# what are different exception python support

import exceptions
print dir(exceptions)


# In[ ]:


# try..except..else..finally
# try - important part of the code or input.
# except - what to do with the exception.
# else: if there is no exeption in try block .. go to else block.


# In[11]:


# case I
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except:
    print "please enter numbers.Make sure denominator is not zero."
else:
    print result


# In[12]:


# case II
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except (ValueError,ZeroDivisionError):
    print "please enter numbers.Make sure denominator is not zero."
else:
    print result


# In[18]:


# case III
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please enter numbers."
except ZeroDivisionError:
    print "Make sure denominator is not zero."
else:
    print result
    


# In[ ]:


# try..except..else..finally
# try - important part of the code or input.
# except - what to do with the exception.
# else: if there is no exeption in try block .. go to else block.
# finally:
# case I : i entered all valid values -> try..else..finally
# case II: i entered a invalid value..handled by exception -> try..except.. finally
# case III: i enter a invalid value..not handled by exception -> try..finally.. exception 


# In[22]:


try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please enter numbers."
else:
    print result
finally:
    print "ALL IS WELL."
    # close all connection.


# In[23]:


# raise

raise SyntaxError


# In[24]:


raise SyntaxError,"Please clean your glasses"


# In[25]:


# santosh is not part of the exception class.
raise santosh


# In[26]:


class santosh(Exception):
    pass


# In[27]:


raise santosh,"I am back!!"

