#!/usr/bin/python
# http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
# https://github.com/tuxfux-hlp/Python-examples/blob/master/devops/fabfile.py


def my_outer(my_func):
	def my_inner(*args,**kwargs):
		try:
			my_func(*args,**kwargs)
		except Exception as e:
			return "we hit on error - {}".format(e)
		else:
			return my_func(*args,**kwargs)
	return my_inner


@my_outer
def my_add(a,b):
	return a + b

@my_outer
def my_div(a,b):
	return a/b

@my_outer
def my_multi(a,b):
	return a * b

@my_outer
def my_sub(a,b):
	return a - b

print my_div(10,2)
print my_div(10,0)

print my_add(10,11)
print my_add(a="python",b="rocks")
print my_add(a=11,b="rocks")

'''
# Case II:

## old school
my_add = my_outer(my_add)
print my_add,type(my_add)


print my_add(10,11)
print my_add(a="python",b="rocks")
print my_add(a=11,b="rocks")



# case I
def my_add(a,b):
	try:
		a + b
	except Exception as e:
		return "we hit on error - {}".format(e)
	else:
		return a + b

def my_div(a,b):
	try:
		a/b
	except Exception as e:
		return "we hit on error - {}".format(e)
	else:
		return a/b

# main

print my_add(10,11)
print my_add(a="python",b="rocks")
print my_add(a=11,b="rocks")

print my_div(10,2)
print my_div(10,0)
'''