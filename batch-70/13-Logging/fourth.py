#!/usr/bin/python
# logging.basicConfig?
# logging.Formatter?
# datefmt - http://linuxcommand.org/lc3_man_pages/date1.html or time.strftime()
# DEBUG<INFO<WARNING<ERROR<CRITICAL
# dd if=/dev/zero of=bigfile count=1024 bs=1M ( to create a file of 1G)
# https://docs.python.org/2/library/subprocess.html

import logging as l
from subprocess import Popen,PIPE

l.basicConfig(filename="mydisk.txt",filemode='a',level=l.DEBUG,
			  format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
			  datefmt='%c')

P1 = Popen(['df','-h','/'],stdout=PIPE)
P2 = Popen(['tail','-n','1'],stdin=P1.stdout,stdout=PIPE)
disk_size = int(P2.communicate()[0].split()[-2].split('%')[0])


# # disk_size = $(df -h /|tail -n 1|awk '{print $5}'|sed -e 's#%##g')
# #disk_size = int(raw_input("please enter the disk size:"))

if disk_size < 60:
	l.info("The disk looks healthy at {}.".format(disk_size))
elif disk_size < 80:
	l.warning("Your disk is showing warning at {}.".format(disk_size))
elif disk_size < 90:
	l.error("Your disk is puking errors at {}".format(disk_size))
elif disk_size < 99:
	l.critical("Your APP is sleeping at {}.".format(disk_size))

'''
# challenges
* basicConfig does not have facility to provide a logging name( application name)
* basicConfig messages can be logged into console,files
* this is for a small batch of 5 members 10 members.

'''