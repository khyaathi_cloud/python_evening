#!/usr/bin/python
# logging.basicConfig?
# logging.Formatter?
# datefmt - http://linuxcommand.org/lc3_man_pages/date1.html or time.strftime()
# DEBUG<INFO<WARNING<ERROR<CRITICAL
# dd if=/dev/zero of=bigfile count=1024 bs=1M ( to create a file of 1G)
# https://docs.python.org/2/library/subprocess.html

import logging
from subprocess import Popen,PIPE

#l.basicConfig(filename="mydisk.txt",filemode='a',level=l.DEBUG,
#			  format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
#			  datefmt='%c')


# Loggers expose the interface that application code directly uses.
# ex: root(logging)
#Handlers send the log records (created by loggers) to the appropriate destination.
# ex: https://docs.python.org/2/howto/logging.html#useful-handlers
# ex: filename="mydisk.txt",filemode='a'
#Filters provide a finer grained facility for determining which log records to output.
# ex: level=l.DEBUG,
# we can associate levels at logger and handler levels.
#Formatters specify the layout of log records in the final output.
# ex: format='%(asctime)s - %(levelname)s - %(name)s - %(message)s'
# ex: datefmt='%c' 

# create logger
logger = logging.getLogger('my disk') # logger 
logger.setLevel(logging.DEBUG)               # filter of logger.

# create console handler and set level to debug
ch = logging.FileHandler(filename="mydisk.txt")  # handler
ch.setLevel(logging.DEBUG)    # filter for handler.


# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter) # handler and formatter

# add ch to logger
logger.addHandler(ch) # logger and handler.


P1 = Popen(['df','-h','/'],stdout=PIPE)
P2 = Popen(['tail','-n','1'],stdin=P1.stdout,stdout=PIPE)
disk_size = int(P2.communicate()[0].split()[-2].split('%')[0])


# # disk_size = $(df -h /|tail -n 1|awk '{print $5}'|sed -e 's#%##g')
# #disk_size = int(raw_input("please enter the disk size:"))

if disk_size < 60:
	logger.info("The disk looks healthy at {}.".format(disk_size))
elif disk_size < 80:
	logger.warning("Your disk is showing warning at {}.".format(disk_size))
elif disk_size < 90:
	logger.error("Your disk is puking errors at {}".format(disk_size))
elif disk_size < 99:
	logger.critical("Your APP is sleeping at {}.".format(disk_size))

'''
# challenges
* basicConfig does not have facility to provide a logging name( application name)
* basicConfig messages can be logged into console,files
* this is for a small batch of 5 members 10 members.

'''