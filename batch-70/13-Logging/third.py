#!/usr/bin/python
# logging.basicConfig?
# logging.Formatter?
# datefmt - http://linuxcommand.org/lc3_man_pages/date1.html
# DEBUG<INFO<WARNING<ERROR<CRITICAL

import logging as l
l.basicConfig(filename="mydisk.txt",filemode='a',level=l.DEBUG,
			  format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
			  datefmt='%c')

disk_size = int(raw_input("please enter the disk size:"))

if disk_size < 60:
	l.info("The disk looks healthy at {}.".format(disk_size))
elif disk_size < 80:
	l.warning("Your disk is showing warning at {}.".format(disk_size))
elif disk_size < 90:
	l.error("Your disk is puking errors at {}".format(disk_size))
elif disk_size < 99:
	l.critical("Your APP is sleeping at {}.".format(disk_size))