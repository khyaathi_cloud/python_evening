Programming:
+ Functional based programming
+ object oriented based programming.

reference:
https://github.com/tuxfux-hlp/Python-examples/blob/master/opps/Good_links.txt

A - Abstraction
P - Polymorphism
I - Inheritance
E - Encapsulation

A - Abstraction

- -> variables
+ -> functions

car        - class/classobj/blueprint
- wheels   - data/variables
- color
- horsepower
+ clutch   - methods/function
+ break
+ accelerate

car models
- honda city.  - objects/instance/models
- swift dezire. - objects/models
- hyundai
- bmw
- ferrari
- audi

ex: python demo
ex: bank account

P - Polymorphism

a single object reprenting many forms.

Interest
 - saving
 - credit
 - home
 - car
 - education

 In [1]: a = 1

In [2]: b = 2

In [3]: a + b
Out[3]: 3

In [4]: c = "python"

In [5]: d = "rocks"

In [6]: c + d
Out[6]: 'pythonrocks'

In [7]: 

I - Inheritance

father - (righty,doctor)
mother - (left,doctor)
|
|
child1 (left,artist)
child2 (right,engineer)

E - Encapsulation
object related function and values bonded into one.
data hiding - public,private and protected.

ex: pendrive



In [8]: # class

In [9]: class my_account:
   ...:     my_balance = 0
   ...:     

In [10]: # my_account is a class

In [11]: # my_balance is a data/variable.

In [12]: print my_account,type(my_account)
__main__.my_account <type 'classobj'>

In [13]: chandra = my_account()

In [14]: print chandra,type(chandra)
<__main__.my_account instance at 0x7f31845066c8> <type 'instance'>

In [15]: print dir(chandra)
['__doc__', '__module__', 'my_balance']

In [16]: print chandra.my_balance
0

In [17]: chandra.my_balance = 1000

In [18]: print chandra.my_balance
1000

In [19]: # santosh

In [20]: santosh = my_account()

In [21]: print santosh.my_balance
0


In [24]: # data can be two types

In [25]: # class based and instance based.

In [26]: class my_account:
    ...:     my_balance = 0
    ...:     

In [27]: 

In [27]: my_account
Out[27]: <class __main__.my_account at 0x7f318441eb48>

In [28]: my_account.my_balance=1000

In [29]: print my_account.my_balance
1000

In [30]: # chandra

In [31]: chandra = my_account()

In [32]: print chandra.my_balance
1000

In [33]: chandra.my_balance = 2000

In [34]: print chandra.my_balance
2000

In [35]: # santosh

In [36]: san = my_account()

In [37]: print san.my_balance
1000

In [38]: print chandra.my_balance
2000

# error 1

n [40]: class my_account:
    ...:     my_balance = 0
    ...:     def my_display():
    ...:         return "my balance is my_balance - {}".format(my_balance)
    ...:     

In [41]: # hari

In [42]: hari = my_account()

In [43]: print dir(hari)
['__doc__', '__module__', 'my_balance', 'my_display']

In [44]: print hari.my_balance
0

In [45]: print hari.my_display()
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-45-83a1da08cd32> in <module>()
----> 1 print hari.my_display()

TypeError: my_display() takes no arguments (1 given)

In [46]: 


## error 2:

In [47]: class my_account:
    ...:     my_balance = 0
    ...:     def my_display(self):
    ...:         return "my balance is my_balance - {}".format(my_balance)
    ...:     
    ...:     

In [48]: # self is just a variable

In [49]: # all your methods(functions inside class) should have a self,and it represents yoru instance

In [50]: # hari

In [51]: hari = my_account()

In [52]: print dir(hari)
['__doc__', '__module__', 'my_balance', 'my_display']

In [53]: hari.my_balance
Out[53]: 0

In [54]: print hari.my_display()
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-54-83a1da08cd32> in <module>()
----> 1 print hari.my_display()

<ipython-input-47-c7d83cf9bdcc> in my_display(self)
      2     my_balance = 0
      3     def my_display(self):
----> 4         return "my balance is my_balance - {}".format(my_balance)
      5 
      6 

NameError: global name 'my_balance' is not defined

## error 3

In [56]: class my_account:
    ...:     my_balance = 0
    ...:     def my_display(self):
    ...:         return "my balance is my_balance - {}".format(self.my_balance)
    ...:     
    ...:     
    ...:     

In [57]: # hari

In [58]: hari = my_account()

In [59]: print dir(hari)
['__doc__', '__module__', 'my_balance', 'my_display']

In [60]: print hari.my_balance
0

In [61]: print hari.my_display
<bound method my_account.my_display of <__main__.my_account instance at 0x7f3184613518>>

In [62]: print hari.my_display()
my balance is my_balance - 0

In [63]: print my_account.my_balance
0

n [65]: # construtor

In [66]: # pre-defined method

In [67]: class my_account:
    ...:     def __init__(self):
    ...:         self.my_balance = 0
    ...:     def my_display(self):
    ...:         return "my balance is my_balance - {}".format(self.my_balance)
    ...:     
    ...:     
    ...:     

In [68]: 

In [68]: # hari

In [69]: hari = my_account

In [70]: hari
Out[70]: <class __main__.my_account at 0x7f3184391c18>

In [71]: print dir(hari)
['__doc__', '__init__', '__module__', 'my_display']

In [72]: hari.my_display()
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-72-923e33cbe146> in <module>()
----> 1 hari.my_display()

TypeError: unbound method my_display() must be called with my_account instance as first argument (got nothing instead)

In [73]: # chandra

In [74]: chandra = my_account()

In [75]: print dir(chandra)
['__doc__', '__init__', '__module__', 'my_balance', 'my_display']

In [76]: print chandra.my_balance
0

In [77]: print chandra.my_display()
my balance is my_balance - 0


In [79]: a = 1

In [80]: print a
1

In [81]: print a,type(a),dir(a)
1 <type 'int'> ['__abs__', '__add__', '__and__', '__class__', '__cmp__', '__coerce__', '__delattr__', '__div__', '__divmod__', '__doc__', '__float__', '__floordiv__', '__format__', '__getattribute__', '__getnewargs__', '__hash__', '__hex__', '__index__', '__init__', '__int__', '__invert__', '__long__', '__lshift__', '__mod__', '__mul__', '__neg__', '__new__', '__nonzero__', '__oct__', '__or__', '__pos__', '__pow__', '__radd__', '__rand__', '__rdiv__', '__rdivmod__', '__reduce__', '__reduce_ex__', '__repr__', '__rfloordiv__', '__rlshift__', '__rmod__', '__rmul__', '__ror__', '__rpow__', '__rrshift__', '__rshift__', '__rsub__', '__rtruediv__', '__rxor__', '__setattr__', '__sizeof__', '__str__', '__sub__', '__subclasshook__', '__truediv__', '__trunc__', '__xor__', 'bit_length', 'conjugate', 'denominator', 'imag', 'numerator', 'real']

In [82]: # <type 'int'> -> a is a instance of class int

In [83]: isinstance?
Docstring:
isinstance(object, class-or-type-or-tuple) -> bool

Return whether an object is an instance of a class or of a subclass thereof.
With a type as second argument, return whether that is the object's type.
The form using a tuple, isinstance(x, (A, B, ...)), is a shortcut for
isinstance(x, A) or isinstance(x, B) or ... (etc.).
Type:      builtin_function_or_method

In [84]: isinstance(a,int)
Out[84]: True

In [85]: isintance(a,str)
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-85-fc68d81afa52> in <module>()
----> 1 isintance(a,str)

NameError: name 'isintance' is not defined

In [86]: isinstance(a,str)
Out[86]: False


### Encapsulation
http://radek.io/2011/07/21/private-protected-and-public-in-python/

In [102]: class Cup:
     ...:     def __init__(self):
     ...:         self.color = None
     ...:         self.content = None
     ...: 
     ...:     def fill(self, beverage):
     ...:         self.content = beverage
     ...: 
     ...:     def empty(self):
     ...:         self.content = None
     ...:         

In [103]: red = Cup()

In [104]: dir(cup)
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-104-2bbd5d62923b> in <module>()
----> 1 dir(cup)

NameError: name 'cup' is not defined

In [105]: dir(red)
Out[105]: ['__doc__', '__init__', '__module__', 'color', 'content', 'empty', 'fill']

In [106]: red.fill('coffee')

In [107]: red.content
Out[107]: 'coffee'

In [108]: red.empty()

In [109]: red.content

In [110]: red.color = "red"

In [111]: red.content = "cofee"

In [112]: print red.color
red

In [113]: print red.content
cofee


In [115]: class Cup:
     ...:     def __init__(self):
     ...:         self.color = None
     ...:         self._content = None # protected variable
     ...: 
     ...:     def fill(self, beverage):
     ...:         self._content = beverage
     ...: 
     ...:     def empty(self):
     ...:         self._content = None
     ...:         

In [116]: blue = Cup()

In [117]: blue.fill("tea")

In [118]: blue._content
Out[118]: 'tea'

In [119]: blue._content = "cofee"


Reference: https://github.com/tuxfux-hlp/Python-examples/blob/master/opps/Good_links.txt