#!/usr/bin/python
# variables in a parent constructor cannot cannot be inherited automatically.
# variables in a parent constructor has to be explicity imported.

class my_account:						# parent class/super class
	def __init__(self):  # construtor
		self.balance = 0
	
	def my_deposit(self,amount):
		self.balance = self.balance + amount
		return self.balance

	def my_withdraw(self,amount):
		self.balance = self.balance - amount
		return self.balance

# TODO: is there a way to restrict the function from parent to child.
class my_mim_account(my_account):  # child class/sub class
	def __init__(self):
		my_account.__init__(self)

	def my_withdraw(self,amount):
		if self.balance - amount < 1000:
			return "Time to call his daddy!!"
		else:
			return my_account.my_withdraw(self,amount)

# chandra - salaried

chandra = my_account()
print "{} has balance of {}".format("chandra",chandra.balance)
chandra.my_deposit(10000)
print "{} has balance of {}".format("chandra",chandra.balance)
chandra.my_withdraw(2000)
chandra.my_withdraw(2000)
chandra.my_withdraw(2000)
chandra.my_withdraw(5000)
print "{} has balance of {}".format("chandra",chandra.balance)

# pradeep - student
pradeep = my_mim_account()
print "{} has balance of {}".format("pradeep",pradeep.balance)
pradeep.my_deposit(5000)
print "{} has balance of {}".format("pradeep",pradeep.balance)
pradeep.my_withdraw(2000)
print "{} has balance of {}".format("pradeep",pradeep.balance)
print pradeep.my_withdraw(2500)
print "{} has balance of {}".format("pradeep",pradeep.balance)
pradeep.my_withdraw(2000)
print "{} has balance of {}".format("pradeep",pradeep.balance)