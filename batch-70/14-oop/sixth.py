#!/usr/bin/python
# https://anandology.com/python-practice-book/object_oriented_programming.html#special-class-methods
# https://rszalski.github.io/magicmethods/

class RationalNumber:
    """
    Rational Numbers with support for arthmetic operations.

        >>> a = RationalNumber(1, 2)
        >>> b = RationalNumber(1, 3)
        >>> a + b
        5/6
    """
    def __init__(self, numerator, denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self, other):
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)

        n = self.n * other.d + self.d * other.n
        d = self.d * other.d
        return RationalNumber(n, d) # self.n=n,self.d=d => return(self.n,self.d)

    def __str__(self):
        return "%s/%s" % (self.n, self.d)

    __repr__ = __str__

# main
a = RationalNumber(1, 2) # a.n = 1,a.d = 2
b = RationalNumber(1, 3) # b.n = 1,b.d = 3
print a + b # a.__add__(b)

a = 1
b = 2
print a + b

a = RationalNumber(1, 2)
b = 2 # b = RationalNumber(2) => b.n = 2,b.d = 1 =>2/1
print a + b