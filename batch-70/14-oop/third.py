#!/usr/bin/python

class my_account:
	def __init__(self):  # construtor
		self.balance = 0
	
	def my_deposit(self):
		self.balance = self.balance + 1000
		return self.balance

	def my_withdraw(self):
		self.balance = self.balance - 300
		return self.balance


# chandra

chandra = my_account()
print "{} has balance of {}".format("chandra",chandra.balance)
chandra.my_deposit()
print "{} has balance of {}".format("chandra",chandra.balance)
chandra.my_withdraw()
print "{} has balance of {}".format("chandra",chandra.balance)

# srini

srini = my_account()
print "{} has balance of {}".format("srini",srini.balance)
srini.my_deposit()
print "{} has balance of {}".format("srini",srini.balance)
srini.my_withdraw()
print "{} has balance of {}".format("srini",srini.balance)
