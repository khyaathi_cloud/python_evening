#!/usr/bin/python
my_fruits = ['apple','orange','banana','apple','orange','orange']
#my_fruits[:] = ['apple','orange','banana','apple','orange','orange']  # different id

my_dupli = []
#new_fruits = my_fruits[:]

#for value in my_fruits:
for value in my_fruits[:]:
	if my_fruits.count(value) > 1:
		if value not in my_dupli:
			my_dupli.append(value)
		my_fruits.remove(value)

#print new_fruits
print my_fruits
print my_dupli


'''
In [131]: my_fruits
Out[131]: ['banana', 'apple', 'orange', 'orange']

In [132]: my_fruits = ['apple','orange','banana','apple','orange','orange']

In [133]: id(my_fruits)
Out[133]: 140382686368184

In [134]: id(my_fruits[:])
Out[134]: 140382686332616

In [135]: print my_fruits
['apple', 'orange', 'banana', 'apple', 'orange', 'orange']

In [136]: print my_fruits[:]
['apple', 'orange', 'banana', 'apple', 'orange', 'orange']

In [137]: 

'''