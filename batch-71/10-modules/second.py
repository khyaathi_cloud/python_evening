#!/usr/bin/python

import sys
sys.path.insert(0,'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-71/10-modules/extra')

import first as f

def my_add(a,b):
	''' Addition of two intergers '''
	a = int(a)
	b = int(b)
	return a + b

if __name__ == '__main__':
	print "addition of two numbers is {}".format(my_add(11,22))
	print "addition of two string is {}".format(f.my_add("linux","rocks"))