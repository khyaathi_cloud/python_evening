
# coding: utf-8

# In[ ]:


# exceptions
# filling in a google form/any govt relative forms.
# try..else..except..finally
# try: try is mandatory block of code which we want to run.
# except: try to take action on exceptions if there are any.
# else: if try is good(no exception) then go to else block.


# In[2]:


# case I
num1 = int(raw_input("please enter the number1:"))
num2 = int(raw_input("please enter the number2:"))
result = num1/num2
print result


# In[3]:


# case II
num1 = int(raw_input("please enter the number1:"))
num2 = int(raw_input("please enter the number2:"))
result = num1/num2
print result


# In[6]:


# case I: I handled exception in a global way.
try:
    num1 = int(raw_input("please enter the number1:"))
    num2 = int(raw_input("please enter the number2:"))
    result = num1/num2
except: 
    print "please enter numbers.Make sure your divisor are non-zero."
else:
    print result


# In[7]:


# case II: how to deal with exceptions.

try:
    num1 = int(raw_input("please enter the number1:"))
    num2 = int(raw_input("please enter the number2:"))
    result = num1/num2
except (ZeroDivisionError,ValueError): 
    print "please enter numbers.Make sure your divisor are non-zero."
else:
    print result


# In[9]:


# case III: one exception for each of them.

try:
    num1 = int(raw_input("please enter the number1:"))
    num2 = int(raw_input("please enter the number2:"))
    result = num1/num2
except ValueError: 
    print "please enter numbers."
except ZeroDivisionError:
    print "Please make sure your divisor is non-zero"
else:
    print result


# In[11]:


# how do i know what are various exceptions

import exceptions as e
print dir(e)


# In[12]:


# try..else..except..finally
# try: try is mandatory block of code which we want to run.
# except: try to take action on exceptions if there are any.
# else: if try is good(no exception) then go to else block.
# Finally:
# case I: if i enter valid values, try..else..finally
# case II: invalid values.. handed by exception .. try..except..finally
# case III: invalid values.. not handled by exception.try..finally..bombed with error.


# In[19]:


try:
    num1 = int(raw_input("please enter the number1:"))
    num2 = int(raw_input("please enter the number2:"))
    result = num1/num2
except ValueError: 
    print "please enter numbers."
else:
    print result
finally:
    print "all is well"
    # file.close
    # socket.close
    # database.close


# In[20]:


# raise

raise SyntaxError


# In[21]:


raise SyntaxError,"please clean your glasses."


# In[22]:


# to raise any keyword it has to be part of exception class.
raise santosh


# In[23]:


class santosh(Exception):
    pass


# In[24]:


raise santosh,"hey i am back"

