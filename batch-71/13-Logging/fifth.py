#!/usr/bin/python
# logging.basicConfig?
# logging.Formatter?
# datefmt - http://linuxcommand.org/lc3_man_pages/date1.html
# time.strftime().
# DEBUG<INFO<WARNING<ERROR<CRITICAL
# https://docs.python.org/2/library/subprocess.html
# crontab (linux) or scheduler (windows)
# https://docs.python.org/2/howto/logging.html#useful-handlers

import logging
from subprocess import Popen,PIPE

# l.basicConfig(filename="mydisk.txt",filemode='a',level=l.DEBUG,
# 			  format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
# 			  datefmt='%c')

# df -h /| tail -n 1|awk '{print $5}'|sed -e 's#%##g'
#disk_size = int(raw_input("please enter your disk size:"))

# Logger: Loggers expose the interface that application code directly uses.
# ex: logging: root
# Handlers: Handlers send the log records (created by loggers) to the appropriate destination.
# ex: filename="mydisk.txt",filemode='a'
# Filters: Filters provide a finer grained facility for determining which log records to output.
# ex: level=l.DEBUG
# Formatters: Formatters specify the layout of log records in the final output.
# ex: format='%(asctime)s - %(levelname)s - %(name)s - %(message)s'

# create logger
logger = logging.getLogger('disk Monitor')  # logger
logger.setLevel(logging.DEBUG)                # filter for logger.

# create console handler and set level to debug
ch = logging.StreamHandler() # handler
ch.setLevel(logging.DEBUG)   # filter for handler.

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter) # handler and formatter

# add ch to logger
logger.addHandler(ch)      # logger and handler.

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_size = int(p2.communicate()[0].split()[-2].split('%')[0])

if disk_size < 50:
	logger.info("Your disk is looking healthy - {}".format(disk_size))
elif disk_size < 70:
	logger.warning("Your disk looks getting fat - {}".format(disk_size))
elif disk_size < 90:
	logger.error("Your disk is puking some errors - {}.".format(disk_size))
elif disk_size < 99:
	logger.critical("Your application has gone for a sleep - {}.".format(disk_size))

# we cannot put multiple logs into a single location.
# basicConfig only can log to a file.
# we cannot name our logger.
# only applicable for small teams.