#!/usr/bin/python
# logging.basicConfig?
# logging.Formatter?
# datefmt - http://linuxcommand.org/lc3_man_pages/date1.html
# time.strftime().
# DEBUG<INFO<WARNING<ERROR<CRITICAL
# https://docs.python.org/2/library/subprocess.html
# crontab (linux) or scheduler (windows)

import logging as l
from subprocess import Popen,PIPE
l.basicConfig(filename="mydisk.txt",filemode='a',level=l.DEBUG,
			  format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
			  datefmt='%c')

# df -h /| tail -n 1|awk '{print $5}'|sed -e 's#%##g'
#disk_size = int(raw_input("please enter your disk size:"))

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_size = int(p2.communicate()[0].split()[-2].split('%')[0])

if disk_size < 50:
	l.info("Your disk is looking healthy - {}".format(disk_size))
elif disk_size < 70:
	l.warning("Your disk looks getting fat - {}".format(disk_size))
elif disk_size < 90:
	l.error("Your disk is puking some errors - {}.".format(disk_size))
elif disk_size < 99:
	l.critical("Your application has gone for a sleep - {}.".format(disk_size))

# we cannot put multiple logs into a single location.
# basicConfig only can log to a file.
# we cannot name our logger.
# only applicable for small teams.