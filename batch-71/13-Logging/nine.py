#!/usr/bin/python
import logging
#import pdb

# set up logging to file - see previous section for more details
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='myapp.log',
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console) # root => logging.getLogger
#logger1.addHandler(console)


# Now, we can log to the root logger, or any other logger. First the root...
logging.info('Jackdaws love my big sphinx of quartz.')

# Now, define a couple of other loggers which might represent areas in your
# application:

# if loggers are defined without any filters - DEFAULT WARNING IS ENABLED.
logger1 = logging.getLogger('myapp.area1')
logger2 = logging.getLogger('myapp.area2')


#pdb.set_trace()
# TODO: why logger1 is not working.
logger2.debug('Quick zephyrs blow, vexing daft Jim.')
logger2.info('How quickly daft jumping zebras vex.')
logger1.warning('Jail zesty vixen who grabbed pay from quack.')
logger1.error('The five boxing wizards jump quickly.')