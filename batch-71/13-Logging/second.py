#!/usr/bin/python
# logging.basicConfig?
# logging.Formatter?
# datefmt - http://linuxcommand.org/lc3_man_pages/date1.html
# DEBUG<INFO<WARNING<ERROR<CRITICAL

import logging as l
l.basicConfig(filename="mylog.txt",filemode='a',level=l.DEBUG,
			  format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
			  datefmt='%c')

l.debug("This is a debug message.")
l.info("This is a information message.")
l.warning("This is a warning message.")
l.error("This is an error message.")
l.critical("This is a critical message.")