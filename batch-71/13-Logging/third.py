#!/usr/bin/python
# logging.basicConfig?
# logging.Formatter?
# datefmt - http://linuxcommand.org/lc3_man_pages/date1.html
# DEBUG<INFO<WARNING<ERROR<CRITICAL

import logging as l
l.basicConfig(filename="mydisk.txt",filemode='a',level=l.DEBUG,
			  format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
			  datefmt='%c')

disk_size = int(raw_input("please enter your disk size:"))

if disk_size < 50:
	l.info("Your disk is looking healthy - {}".format(disk_size))
elif disk_size < 70:
	l.warning("Your disk looks getting fat - {}".format(disk_size))
elif disk_size < 90:
	l.error("Your disk is puking some errors - {}.".format(disk_size))
elif disk_size < 99:
	l.critical("Your application has gone for a sleep - {}.".format(disk_size))