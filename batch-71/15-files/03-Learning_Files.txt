# why files ?
This is for persistent storage of data.

# what all we can do with files ?

- read -> reading a file.
- write -> writing to a file.
- append -> append to a file.

# what are the differnt modes.

r -> read mode - you can only read the file.
w -> write mode - you can only write the file.
    * if file is not there,a new file gets created and you write into that file.
    * if file is there,the previous data get truncated to zero.
a -> append mode - you can only append the file.Add new contents to the bottom of the file.
r+ -> read/write mode - you can both read and write into the file.
b  -> binary mode - rb,wb,ab,r+b - this makes sure your files dont loose the data.

khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-71/15-files$ ipython
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
Type "copyright", "credits" or "license" for more information.

IPython 2.4.1 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: ls
03-Learning_Files.txt  file1.txt

In [2]: f = open("file1.txt")

In [3]: print f,type(f)
<open file 'file1.txt', mode 'r' at 0x7fdc4e7e4e40> <type 'file'>

In [4]: f.
f.close       f.errors      f.isatty      f.newlines    f.readinto    f.seek        f.truncate    f.xreadlines  
f.closed      f.fileno      f.mode        f.next        f.readline    f.softspace   f.write       
f.encoding    f.flush       f.name        f.read        f.readlines   f.tell        f.writelines  

In [4]: f.name
Out[4]: 'file1.txt'

In [5]: f.mode
Out[5]: 'r'

In [6]: f
Out[6]: <open file 'file1.txt', mode 'r' at 0x7fdc4e7e4e40>

In [7]: f = open("file1.txt","r"_
   ...: 
KeyboardInterrupt

In [7]: f = open("file1.txt","r")

In [8]: print f,type(f)
<open file 'file1.txt', mode 'r' at 0x7fdc4e7e4ed0> <type 'file'>

In [10]: # f.read

In [11]: f.read?
Type:        builtin_function_or_method
String form: <built-in method read of file object at 0x7fdc4e7e4ed0>
Docstring:
read([size]) -> read at most size bytes, returned as a string.

If the size argument is negative or omitted, read until EOF is reached.
Notice that when in non-blocking mode, less data than what was requested
may be returned, even if no size parameter was given.

In [12]: f.read(2)
Out[12]: 'Th'

In [13]: f.read(4)
Out[13]: 'is i'

In [14]: f.read(2)
Out[14]: 's '

In [15]: f.read()
Out[15]: 'my line 1.\nThis is my line 2.\nThis is my line 3.\nThis is my line 4.\nThis is my line 5.\nThis is my line 6.'

In [16]: f.read()
Out[16]: ''

In [17]: # f.tell

In [18]: f.tell()
Out[18]: 113

In [19]: # f.seek

In [20]: f.seek(0)

In [21]: f.tell()
Out[21]: 0

In [10]: # f.read

In [11]: f.read?
Type:        builtin_function_or_method
String form: <built-in method read of file object at 0x7fdc4e7e4ed0>
Docstring:
read([size]) -> read at most size bytes, returned as a string.

If the size argument is negative or omitted, read until EOF is reached.
Notice that when in non-blocking mode, less data than what was requested
may be returned, even if no size parameter was given.

In [12]: f.read(2)
Out[12]: 'Th'

In [13]: f.read(4)
Out[13]: 'is i'

In [14]: f.read(2)
Out[14]: 's '

In [15]: f.read()
Out[15]: 'my line 1.\nThis is my line 2.\nThis is my line 3.\nThis is my line 4.\nThis is my line 5.\nThis is my line 6.'

In [16]: f.read()
Out[16]: ''

In [17]: # f.tell

In [18]: f.tell()
Out[18]: 113

In [19]: # f.seek

In [20]: f.seek(0)

In [21]: f.tell()
Out[21]: 0

In [22]: f.read()
Out[22]: 'This is my line 1.\nThis is my line 2.\nThis is my line 3.\nThis is my line 4.\nThis is my line 5.\nThis is my line 6.'

In [23]: f.read()
Out[23]: ''

In [24]: f.tell()
Out[24]: 113

In [26]: # f.readline

In [27]: f.readline?
Type:        builtin_function_or_method
String form: <built-in method readline of file object at 0x7fdc4e7e4ed0>
Docstring:
readline([size]) -> next line from the file, as a string.

Retain newline.  A non-negative size argument limits the maximum
number of bytes to return (an incomplete line may be returned then).
Return an empty string at EOF.

In [28]: f.tell()
Out[28]: 113

In [29]: f.seek(0)

In [30]: f.readline()
Out[30]: 'This is my line 1.\n'

In [31]: f.readline()
Out[31]: 'This is my line 2.\n'

In [32]: f.readline()
Out[32]: 'This is my line 3.\n'

In [33]: f.readline()
Out[33]: 'This is my line 4.\n'

In [34]: f.readline()
Out[34]: 'This is my line 5.\n'

In [35]: f.readline()
Out[35]: 'This is my line 6.'

In [36]: f.readline()
Out[36]: ''

In [38]: # f.readlines

In [39]: f.readlines?
Type:        builtin_function_or_method
String form: <built-in method readlines of file object at 0x7fdc4e7e4ed0>
Docstring:
readlines([size]) -> list of strings, each a line from the file.

Call readline() repeatedly and return a list of the lines so read.
The optional size argument, if given, is an approximate bound on the
total number of bytes in the lines returned.

In [40]: f.seek(0)

In [41]: my_lines = f.readlines()

In [42]: print my_lines
['This is my line 1.\n', 'This is my line 2.\n', 'This is my line 3.\n', 'This is my line 4.\n', 'This is my line 5.\n', 'This is my line 6.']

In [43]: 

In [44]: # f.xreadlines

In [45]: f.xreadlines?
Type:        builtin_function_or_method
String form: <built-in method xreadlines of file object at 0x7fdc4e7e4ed0>
Docstring:
xreadlines() -> returns self.

For backward compatibility. File objects now include the performance
optimizations previously implemented in the xreadlines module.

In [46]: # f.readlines has all the features of f.readline

In [47]: f.readinto?
Type:        builtin_function_or_method
String form: <built-in method readinto of file object at 0x7fdc4e7e4ed0>
Docstring:   readinto() -> Undocumented.  Don't use this; it may go away.

In [49]: f.
f.close       f.errors      f.isatty      f.newlines    f.readinto    f.seek        f.truncate    f.xreadlines  
f.closed      f.fileno      f.mode        f.next        f.readline    f.softspace   f.write       
f.encoding    f.flush       f.name        f.read        f.readlines   f.tell        f.writelines  

In [49]: g = open("file2.txt","w")

In [50]: print g,type(g)
<open file 'file2.txt', mode 'w' at 0x7fdc4d7fc030> <type 'file'>

In [51]: g.write?
Type:        builtin_function_or_method
String form: <built-in method write of file object at 0x7fdc4d7fc030>
Docstring:
write(str) -> None.  Write string str to file.

Note that due to buffering, flush() or close() may be needed before
the file on disk reflects the data written.

In [52]: f.write("line1 is my first line.\nline2 is my second line.\nline3 is my third line.\nline4 is my fourth line.")
---------------------------------------------------------------------------
IOError                                   Traceback (most recent call last)
<ipython-input-52-2c70a2fd53f6> in <module>()
----> 1 f.write("line1 is my first line.\nline2 is my second line.\nline3 is my third line.\nline4 is my fourth line.")

IOError: File not open for writing

In [53]: g.write("line1 is my first line.\nline2 is my second line.\nline3 is my third line.\nline4 is my fourth line.")

In [54]: # input(keyboard) -> I/O buffer -> cpu -> I/O buffer -> output(disk)

In [55]: g.flush?
Type:        builtin_function_or_method
String form: <built-in method flush of file object at 0x7fdc4d7fc030>
Docstring:   flush() -> None.  Flush the internal I/O buffer.

In [56]: g.flush()

In [58]: g
Out[58]: <open file 'file2.txt', mode 'w' at 0x7fdc4d7fc030>

In [59]: my_lines
Out[59]: 
['This is my line 1.\n',
 'This is my line 2.\n',
 'This is my line 3.\n',
 'This is my line 4.\n',
 'This is my line 5.\n',
 'This is my line 6.']

In [60]: g.writelines?
Type:        builtin_function_or_method
String form: <built-in method writelines of file object at 0x7fdc4d7fc030>
Docstring:
writelines(sequence_of_strings) -> None.  Write the strings to the file.

Note that newlines are not added.  The sequence can be any iterable object
producing strings. This is equivalent to calling write() for each string.

In [61]: g.writelines(my_lines)

In [62]: g.close?
Type:        builtin_function_or_method
String form: <built-in method close of file object at 0x7fdc4d7fc030>
Docstring:
close() -> None or (perhaps) an integer.  Close the file.

Sets data attribute .closed to True.  A closed file cannot be used for
further I/O operations.  close() may be called more than once without
error.  Some kinds of file objects (for example, opened by popen())
may return an exit status upon closing.

In [63]: g.close()

In [64]: g
Out[64]: <closed file 'file2.txt', mode 'w' at 0x7fdc4d7fc030>

In [65]: g.
g.close       g.errors      g.isatty      g.newlines    g.readinto    g.seek        g.truncate    g.xreadlines  
g.closed      g.fileno      g.mode        g.next        g.readline    g.softspace   g.write       
g.encoding    g.flush       g.name        g.read        g.readlines   g.tell        g.writelines  


In [70]: g
Out[70]: <closed file 'file2.txt', mode 'w' at 0x7fdc4d7fc030>

In [71]: g.write("hey there i am writing a new line"
   ....: )
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-71-198c18f59236> in <module>()
----> 1 g.write("hey there i am writing a new line"
      2 )

ValueError: I/O operation on closed file

In [72]: # g.closed

In [73]: if g.closed:
   ....:     print "The file is closed"
   ....: else:
   ....:     print "file is open"
   ....:     
The file is closed

In [74]: g.closed
Out[74]: True

In [75]: f
Out[75]: <open file 'file1.txt', mode 'r' at 0x7fdc4e7e4ed0>

In [76]: f.closed
Out[76]: False

In [78]: # try..except..else..finally

In [79]: try:
   ....:     g.write("hey there i am writing a new line")
   ....: except:
   ....:     print "buddy the file is closed"
   ....: else:
   ....:     print "i can write into the file"
   ....: finally:
   ....:     g.close()
   ....:     
buddy the file is closed

In [80]: g
Out[80]: <closed file 'file2.txt', mode 'w' at 0x7fdc4d7fc030>

In [81]: 

In [82]: # with 

In [83]: with open("file2.txt","a") as h:
   ....:     h.write("hey there i am adding a new line.")
   ....:     

In [84]: h
Out[84]: <closed file 'file2.txt', mode 'a' at 0x7fdc4d7fc1e0>

In [85]: g
Out[85]: <closed file 'file2.txt', mode 'w' at 0x7fdc4d7fc030>

In [86]: f
Out[86]: <open file 'file1.txt', mode 'r' at 0x7fdc4e7e4ed0>

In [87]: 

In [88]: g.
g.close       g.errors      g.isatty      g.newlines    g.readinto    g.seek        g.truncate    g.xreadlines  
g.closed      g.fileno      g.mode        g.next        g.readline    g.softspace   g.write       
g.encoding    g.flush       g.name        g.read        g.readlines   g.tell        g.writelines  

In [88]: g.truncate?
Type:        builtin_function_or_method
String form: <built-in method truncate of file object at 0x7fdc4d7fc030>
Docstring:
truncate([size]) -> None.  Truncate the file to at most size bytes.

Size defaults to the current file position, as returned by tell().

In [89]: # g.softspace,g.newlines,g.fileno,g.errors,g.encoding ## google

In [90]: 


In [91]: my_string='Jun 15 18:51:02 khyaathi-Technologies sublime_text[4425]: Source ID 9724695 was not found when attempting to remove it'

In [92]: import re

In [103]: m = re.search('k.*-t.*?s',my_string,re.I)

In [104]: print m,type(m)
<_sre.SRE_Match object at 0x7fdc4d758d98> <type '_sre.SRE_Match'>

In [105]: m.start()
Out[105]: 16

In [106]: m.end()
Out[106]: 37

In [107]: my_string[:16]
Out[107]: 'Jun 15 18:51:02 '

In [108]: my_string[:16] + my_string[37:]
Out[108]: 'Jun 15 18:51:02  sublime_text[4425]: Source ID 9724695 was not found when attempting to remove it'


### install the excel module

khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-71/15-files$ pip install excel
Collecting excel
Collecting xlrd (from excel)
  Downloading https://files.pythonhosted.org/packages/07/e6/e95c4eec6221bfd8528bcc4ea252a850bffcc4be88ebc367e23a1a84b0bb/xlrd-1.1.0-py2.py3-none-any.whl (108kB)
    100% |████████████████████████████████| 112kB 668kB/s 
Installing collected packages: xlrd, excel
Successfully installed excel-1.0.0 xlrd-1.1.0
You are using pip version 8.1.2, however version 10.0.1 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.


###
topics you can looku into
- json
- yaml
- web scrapping
- xml


