#!/usr/bin/python
# break: break will take you out of a loop.
# exit: it will take you completely out of the program.
# task: i want to make sure you can run the game exactly 3 times.

import sys

number = 7
#test = True

play = raw_input("do you want to play the game:")
if play == 'n':
	sys.exit()

print "welcome to the guess game"

#while test: # while works on truth of a condition.
while True:
	answer = int(raw_input("please enter the number:"))
	if answer > number:
		print "The number you guessed is slightly larger."
	elif answer < number:
		print "The number you guessed is slightly smaller"
	elif answer == number:
		print "Congo.. !! The answer is equal to the number"
		break
		#test = False

print "Thank you for playing the game."
