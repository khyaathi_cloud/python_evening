#!/usr/bin/python
# soft and deep copy.

my_fruits = ['banana','apple','apple','banana','cherry','apple']
# my_fruits[:] = ['apple','banana','apple','banana','cherry','apple']
# my_fruits = ['banana','apple','banana','cherry','apple']
#my_fruits = ['apple','banana','apple','banana','cherry','apple']

#my_fruits1 = my_fruits (softcopy)
#my_fruits1 = copy.deepcopy(my_fruits) (deepcopy)
#my_fruits1 = my_fruits[:] (deepcopy)

'''
# output
my_fruits = ['apple','banana','cherry']
my_dupli = ['apple','banana']

'''

my_dupli = []

for value in my_fruits[:]:
	if my_fruits.count(value) > 1:
		if value not in my_dupli:
			my_dupli.append(value)
		my_fruits.remove(value)

print my_fruits