#!/usr/bin/python
import sys
sys.path.insert(0,'/home/khyaathi/Documents/git_repos/bitbucket/python-evenings/batch-72/11-Modules/extra')
## task : how can you make the path dynamic

import first

def my_add(a,b):
	''' add two integers '''
	a = int(a)
	b = int(b)
	return a + b

if __name__ == '__main__':
	print "addition of two numbers is {}".format(my_add(11,22))
	print "addition of two strings is {}".format(first.my_add("linux"," rocks"))