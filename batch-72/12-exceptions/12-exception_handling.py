
# coding: utf-8

# In[1]:

# exception handling
# exception handling is pre-produciton


# In[2]:

# case I
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[3]:

# case II
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[4]:

# case III
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[5]:

# valueerror and zero division error are called exceptions.
# Far more meaning representation of expression.
# python has other exceptions also to deal with.


# In[6]:

import exceptions as e


# In[7]:

print dir(e)


# In[8]:

## try..except..else..finally
# try -> try actually is main block.
# except -> in case try block gives an exception what to do ?
# else -> if try block has no exception go to the else block.


# In[10]:

# case I
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except:
    print "please make sure you are entering numbers and denominator is non-zero"
else:
    print result


# In[11]:

# case II
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except:
    print "please make sure you are entering numbers and denominator is non-zero"
else:
    print result


# In[13]:

# case III
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except:
    print "please make sure you are entering numbers and denominator is non-zero"
else:
    print result


# In[14]:

# except is a broad term. It covers all exceptions.


# In[17]:

## i am only trying to capture the ValueError and ZeroDivisionError exceptions.
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except (ValueError,ZeroDivisionError):
    print "please make sure you are entering numbers and denominator is non-zero"
else:
    print result


# In[18]:

## each exception to have a different unique address.
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please make sure you are entering numbers."
except ZeroDivisionError:
    print "please make sure your denominator is non-zero"
else:
    print result


# In[19]:

try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please make sure you are entering numbers."
except ZeroDivisionError:
    print "please make sure your denominator is non-zero"
else:
    print result


# In[ ]:

## try..except..else..finally
# try -> try actually is main block.
# except -> in case try block gives an exception what to do ?
# else -> if try block has no exception go to the else block.
# finally ->
# case I: enter all valid values.. try..else..finally.
# case II: enter an invalid value..handed by exceptions.. try..except..finally
# case III: enter an invalid value.. not handled by exception .. try..finally..bombed with a exception.


# In[21]:

# case I
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please make sure you are entering numbers."
else:
    print result
finally:
    print "All iZ well !!"
    # close files
    # close databases
    # close sockets


# In[22]:

# case II
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please make sure you are entering numbers."
else:
    print result
finally:
    print "All iZ well !!"


# In[23]:

# case III
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "please make sure you are entering numbers."
else:
    print result
finally:
    print "All iZ well !!"


# In[29]:

## how to check for a exception.

try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except Exception as e:
    print "hey exception who are you - {} - {}".format(e.args,e.message)
    print dir(e)
else:
    print result


# In[30]:

# raise
# how are these exceptions raised ?

raise SyntaxError


# In[31]:

# custom message
raise SyntaxError,"Please clean your glasses buddy !!!"


# In[32]:

## you cannot raise everything.. the word has to be part of the exception class.

#ex:
raise santosh


# In[33]:

# creating an exception class:
# note: I am yet to teach classes :)

class santosh(Exception):
    pass


# In[34]:

raise santosh,"I am back"


# In[ ]:



