#!/usr/bin/python
# debug < info < warning < error < critical
# logging.basicConfig
# man date
# crontab - linux
# scheduler - windows
# time.strftime()
# logging: https://docs.python.org/2/howto/logging.html#useful-handlers

import logging
from subprocess import Popen,PIPE

# logging.basicConfig(filename='myfile.log',filemode='a',level=logging.DEBUG,
# 					format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
# 					datefmt='%c')


# Loggers expose the interface that application code directly uses.
# ex: root
# Handlers send the log records (created by loggers) to the appropriate destination.
# ex: filename='myfile.log',filemode='a'
# Filters provide a finer grained facility for determining which log records to output.
# ex:level=logging.DEBUG,
# Formatters specify the layout of log records in the final output.
# ex: format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',datefmt='%c'

# create logger
logger = logging.getLogger('Monitor disk') # logger name
logger.setLevel(logging.DEBUG)             # logger filter

# create console handler and set level to debug
ch = logging.StreamHandler()  # handler name.
ch.setLevel(logging.DEBUG)    # handlers filter.

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter) # HANDLER AND FORMATTER

# add ch to logger
logger.addHandler(ch) # LOGGING AND HANDLER



p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_size = int(p2.communicate()[0].split()[-2].split('%')[0])

# disk_size = int(raw_input("please enter the disk size:"))

if disk_size < 50:
	logger.info("The disk looks healthy at {}.".format(disk_size))
elif disk_size < 70:
	logger.warning("Your disk is about to become fat at {}.".format(disk_size))
elif disk_size < 85:
	logger.error("Your disk is puking errors at {}.".format(disk_size))
elif disk_size < 100:
	logger.critical("Your application is dead {}.".format(disk_size))

'''
* root logger is one of the issues.
* i can only login to a file.
* this is only for a small team.
'''