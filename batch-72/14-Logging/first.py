#!/usr/bin/python
# debug < info < warning < error < critical
'''
The default level is WARNING, which means that only events of 
this level and above will be tracked, unless the logging package 
is configured to do otherwise.
'''

import logging

logging.debug("This is an debug message.")
logging.info("This is an info message.")
logging.warning("This is an warning message.")
logging.error("This is an error message.")
logging.critical("This is an critical message.")

'''

khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-72/14-Logging$ python first.py 
WARNING:root:This is an warning message.
ERROR:root:This is an error message.
CRITICAL:root:This is an critical message.
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-72/14-Logging$ 

'''