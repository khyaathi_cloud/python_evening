#!/usr/bin/python
# debug < info < warning < error < critical
# logging.basicConfig
# man date
# crontab - linux
# scheduler - windows
# time.strftime()

import logging
from subprocess import Popen,PIPE

logging.basicConfig(filename='myfile.log',filemode='a',level=logging.DEBUG,
					format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
					datefmt='%c')
# df -h / | tail -n 1|awk '{print $5}'|sed -e 's#%##g'

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_size = int(p2.communicate()[0].split()[-2].split('%')[0])

# disk_size = int(raw_input("please enter the disk size:"))

if disk_size < 50:
	logging.info("The disk looks healthy at {}.".format(disk_size))
elif disk_size < 70:
	logging.warning("Your disk is about to become fat at {}.".format(disk_size))
elif disk_size < 85:
	logging.error("Your disk is puking errors at {}.".format(disk_size))
elif disk_size < 100:
	logging.critical("Your application is dead {}.".format(disk_size))

'''
* root logger is one of the issues.
* i can only login to a file.
* this is only for a small team.
'''