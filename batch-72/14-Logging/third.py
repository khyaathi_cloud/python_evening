#!/usr/bin/python
# debug < info < warning < error < critical
# logging.basicConfig
# man date

import logging

logging.basicConfig(filename='myfile.log',filemode='a',level=logging.DEBUG,
					format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
					datefmt='%c')

disk_size = int(raw_input("please enter the disk size:"))

if disk_size < 50:
	logging.info("The disk looks healthy at {}.".format(disk_size))
elif disk_size < 70:
	logging.warning("Your disk is about to become fat at {}.".format(disk_size))
elif disk_size < 85:
	logging.error("Your disk is puking errors at {}.".format(disk_size))
elif disk_size < 100:
	logging.critical("Your application is dead {}.".format(disk_size))
