#!/usr/bin/python
import re
reg1 = re.compile('k.*?n',re.I)

# how to read the /var/log/syslog file.
with open('/var/log/syslog') as syslog:
	for line in syslog:
		m = reg1.search(line)
		print line[:m.start()] + line[m.end():]