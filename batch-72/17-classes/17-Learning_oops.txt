oop - object oriented programming.

Programming
+ modular based programming - functions,modules.
+ class based programming - object programming.

A - Abstraction
P - Polymorphism
I - Inheritance
E - Encapsulation


A - Abstraction  - description of generalization of a object.

blueprint:

car
- color
- model
- wheels
- body
- make

models:
 - Maruti swift
 - honda jazz
 - jaguar
 - Audi
 - bmw

blueprint:

villa
  - rooms
  - floors
  - tiles
  - how big

ntr - villa yellow
ktr - villa pink


blueprint: 

bank              - class/blueprint
 - Account id     - variable/data
 - balance 
 - deposit()      - function/method
 - withdraw()
 - balance_display()

Nagraju  - objects/instances
 - Account id - 455342
 - balance - 1000

Nikhil - objects/instances
 - Account id - 654739
 - balance - 2000



P - Polymorphism

Poly - many
morphism - forms

Interest
 + saving interest -> 3%
 + recuring interst -> 6%
 + fixed intrest -> 7%
 + eductional loan -> 14%
 + personalloan interst -> 11%
 + home loan -> 8%


function behaves multiple ways.

In [1]: a = 1

In [2]: b = 2

In [3]: a + b
Out[3]: 3

In [4]: c = "python"

In [5]: d = "rocks "

In [6]: c + d
Out[6]: 'pythonrocks '


I - Inheritance

|grandmother(lefthander,green eyes)
|mother(right hander,mole right eye/doctor) - superclasses/parentclass
-
|father(left handed/doctor)
-
|son(mole on right eye/engineer)/daughter(left handed/pilot) - subclass/childclass
|daughter(grandmother,mother) - (greeneyes,righthander)


E - Encapsulation - binding the method and data in a close way.Binding of data into a single unit(dharani)
feature - public, protected and private.

bank              - class/blueprint
 - Account id     - variable/data
 - balance 
 - deposit()      - function/method
 - withdraw()
 - balance_display()


 + sowmya  - cashier 

 - deposit()
 - withdraw() 
dharani.balance = 1000
dharani.balance = 4000


 + dharani - somya friend- 1000
 + dharani - 1000

 ex: pendrive

# magic methods
 https://rszalski.github.io/magicmethods/

 In [1]: a = 10

In [2]: b = 10

In [3]: a + b
Out[3]: 20

In [4]: type(a)
Out[4]: int

In [5]: type(b)
Out[5]: int

In [6]: dir(a)
Out[6]: 
['__abs__',
 '__add__',
 '__and__',
 '__class__',
 '__cmp__',
 '__coerce__',
 '__delattr__',
 '__div__',
 '__divmod__',
 '__doc__',
 '__float__',
 '__floordiv__',
 '__format__',
 '__getattribute__',
 '__getnewargs__',
 '__hash__',
 '__hex__',
 '__index__',
 '__init__',
 '__int__',
 '__invert__',
 '__long__',
 '__lshift__',
 '__mod__',
 '__mul__',
 '__neg__',
 '__new__',
 '__nonzero__',
 '__oct__',
 '__or__',
 '__pos__',
 '__pow__',
 '__radd__',
 '__rand__',
 '__rdiv__',
 '__rdivmod__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__rfloordiv__',
 '__rlshift__',
 '__rmod__',
 '__rmul__',
 '__ror__',
 '__rpow__',
 '__rrshift__',
 '__rshift__',
 '__rsub__',
 '__rtruediv__',
 '__rxor__',
 '__setattr__',
 '__sizeof__',
 '__str__',
 '__sub__',
 '__subclasshook__',
 '__truediv__',
 '__trunc__',
 '__xor__',
 'bit_length',
 'conjugate',
 'denominator',
 'imag',
 'numerator',
 'real']

In [7]: a.__add__(b)
Out[7]: 20

In [9]: a = 1

In [10]: b = 2

In [11]: a.__add__(b)
Out[11]: 3

In [12]: c = "python"

In [13]: d = " rocks"

In [14]: c.__add__(d)
Out[14]: 'python rocks'

In [15]: e = 1/2

In [16]: f = 1/2

In [17]: e + f
Out[17]: 0

In [18]: 


