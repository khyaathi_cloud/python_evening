#!/usr/bin/python
# https://github.com/tuxfux-hlp/Python-examples/blob/master/opps/Good_links.txt
# https://anandology.com/python-practice-book/object_oriented_programming.html#special-class-methods
class RationalNumber:
    """
    Rational Numbers with support for arthmetic operations.

        >>> a = RationalNumber(1, 2)
        >>> b = RationalNumber(1, 3)
        >>> a + b
        5/6
    """
    def __init__(self, numerator, denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self, other):
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)

        n = self.n * other.d + self.d * other.n
        d = self.d * other.d
        return RationalNumber(n, d)


    def __str__(self):
        return "%s/%s" % (self.n, self.d)

    __repr__ = __str__


# main
# case I
a = RationalNumber(1, 2) # a.n=1,a.d=2
b = RationalNumber(1, 3) # b.n=1,b.d=3
print a + b # a.__add__(b)

# case II
c = 2
d = 3
print c + d

# case III
a = RationalNumber(1, 2)
b = 3 # b = RationalNumber(3,1)
# 1/2 + 3/1
print a + b