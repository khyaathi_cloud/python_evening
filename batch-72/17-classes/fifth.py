#!/usr/bin/python
class Account:
	def __init__(self): # constructor/method
		self.balance = 0  # class data -> Account.balance
	def my_deposit(self,amount):
		self.balance = self.balance + amount
		return self.balance
	def my_withdraw(self,amount):
		self.balance = self.balance -  amount
		return self.balance
	def my_balance(self): # class method
		return "my balance amount is {}".format(self.balance)


# Lets see the Account class.
print Account
print dir(Account)
'''
print Account.my_balance()
__main__.Account
['__doc__', '__init__', '__module__', 'my_balance']
Traceback (most recent call last):
  File "fifth.py", line 12, in <module>
    print Account.my_balance()
TypeError: unbound method my_balance() must be called with Account instance as first argument (got nothing instead)
'''
## dhani
dhani = Account()
print dhani
print dir(dhani)
dhani.my_deposit(20000)
print dhani.my_balance()
dhani.my_withdraw(2000)
print dhani.my_balance()