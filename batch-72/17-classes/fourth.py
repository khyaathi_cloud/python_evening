#!/usr/bin/python
class Account:
	balance = 0  # class data -> Account.balance
	def my_balance(self): # class method
		return "my balance amount is {}".format(self.balance)

# kumar
kumar = Account
print type(kumar)

# nikhil
nikhil = Account() #  nikhil is a instance of Account class.
print type(nikhil)
print dir(nikhil)
print nikhil.balance
print nikhil.my_balance()
nikhil.balance = 1000
print nikhil.my_balance()

# sowmya
sowmya=Account()
print sowmya.my_balance()

'''
Traceback (most recent call last):
  File "fourth.py", line 12, in <module>
    print nikhil.my_balance()
TypeError: my_balance() takes no arguments (1 given)

# made modification of self in my_balance(self)

Traceback (most recent call last):
  File "fourth.py", line 12, in <module>
    print nikhil.my_balance()
  File "fourth.py", line 5, in my_balance
    return "my balance amount is {}".format(balance)
NameError: global name 'balance' is not defined
'''