#!/usr/bin/python
# my bank

def my_initial():
	return {'balance':0}

def my_deposit(account):
	account['balance'] = account['balance'] + 1000
	return account['balance']

def my_withdraw(account):
	account['balance'] = account['balance'] - 200
	return account['balance']

def my_enquiry(account):
	return "my account balance for is {}".format(account['balance'])

# main

# nikhil
print "Details for nikhil"
nikhil = my_initial()
print my_enquiry(nikhil)
my_deposit(nikhil)
my_deposit(nikhil)
print my_enquiry(nikhil)
my_withdraw(nikhil)
print my_enquiry(nikhil)


# rani
print "Details for rani"
rani = my_initial()
print my_enquiry(rani)
