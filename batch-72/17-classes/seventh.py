#!/usr/bin/python

class InvalidAgeException(Exception):
	def __init__(self,age):
		self.age = age

def validate_age(age):
	if age > 18:
		print "you are welcome to the party."
	else:
		raise InvalidAgeException(age)

# main
if __name__ == "__main__":
	age = int(raw_input("please enter your age:"))
	try:
		validate_age(age)
	except Exception as e:
		print "Buddy you still have to grow big - {}".format(e.age)
	else:
		pass
	finally:
		pass

	