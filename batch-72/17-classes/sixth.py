#!/usr/bin/python
class Account: # parents/super class
	def __init__(self): # constructor/method
		self.balance = 0  # class data -> Account.balance
	def my_deposit(self,amount):
		self.balance = self.balance + amount
		return self.balance
	def my_withdraw(self,amount):
		self.balance = self.balance -  amount
		return self.balance
	def my_balance(self): # class method
		return "my balance amount is {}".format(self.balance)

class MinBalAccount(Account): # childclass/sub class
	def __init__(self): # construtor for parent called cannot be inherited.
		Account.__init__(self)
	def my_withdraw(self,amount):
		if self.balance - amount < 1000:
			return "Buddy!! time to call your daddy."
		else:
			return Account.my_withdraw(self,amount)


# main
# kumar - student
print "balance details of kumar"
kumar = MinBalAccount()
kumar.my_deposit(5000)
print kumar.my_balance()
kumar.my_withdraw(3000)
print kumar.my_balance()
print kumar.my_withdraw(3000)

print "balance of dharani"
# dharani - employee
dharani = Account()
dharani.my_deposit(10000)
print dharani.my_balance()
dharani.my_withdraw(15000)
print dharani.my_balance()
