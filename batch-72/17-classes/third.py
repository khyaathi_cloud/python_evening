#!/usr/bin/python
# oop for banking
# every class we create is part of object super class.

#class Account(object):
class Account:
	balance = 0  # class data

# class
print Account       # __main__.Account
print type(Account) # <type 'classobj'>/class
print dir(Account)  # ['__doc__', '__module__', 'balance'
print Account.balance

# Instance
nikhil = Account() # instance/object
print nikhil.balance
nikhil.balance = 1000
print nikhil.balance  # instance variable

# check for my class variable.
print Account.balance  # class variable
Account.percentage='10%'
print Account.percentage
print dir(Account)

# check for instance
nikhil.newvalue=10
print dir(nikhil)
print dir(Account)

