#!/usr/bin/python
# Author:

import MySQLdb as mdb
con = mdb.connect('localhost','user70','user70','batch70')
cur = con.cursor()
cur.execute("create table student (name varchar(10),gender varchar(6))")
con.close()

'''
before

mysql> show tables;
Empty set (0.00 sec)

mysql> select database();
+------------+
| database() |
+------------+
| batch70    |
+------------+
1 row in set (0.00 sec)

mysql> 

After

mysql> select database();
+------------+
| database() |
+------------+
| batch70    |
+------------+
1 row in set (0.00 sec)

mysql> show tables;
+-------------------+
| Tables_in_batch70 |
+-------------------+
| student           |
+-------------------+
1 row in set (0.00 sec)

mysql> desc student;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| name   | varchar(10) | YES  |     | NULL    |       |
| gender | varchar(6)  | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+
2 rows in set (0.13 sec)

'''