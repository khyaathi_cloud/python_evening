#!/usr/bin/python
# http://www.pythonchallenge.com/pc/def/map.html
'''
my_hint="""g fmnc wms bgblr rpylqjyrc gr zw fylb. 
rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb
gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq 
qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb.
 lmu ynnjw ml rfc spj."""
'''

my_hint = 'map'

import string
my_alpha = list(string.ascii_letters[0:26])
my_new = []

for value in my_hint:
		if value == 'z':
			new_value = 'b'
		elif value == 'y':
			new_value = 'a'
		elif value not in my_alpha:
			new_value = value
		else:
			new_value = my_alpha[my_alpha.index(value) + 2]
		my_new.append(new_value)

print my_new
print "".join(my_new)
