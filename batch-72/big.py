#!/usr/bin/python

num1 = input("please enter your num1:")
num2 = input("please enter your num2:")
num3 = input("please enter your num3:")

if num1 > num2 and num1 > num3:
	print "{0} is greater than {1} and {2}".format(num1,num2,num3)
elif num2 > num1 and num2 > num3:
	print "{1} is greater than {0} and {2}".format(num1,num2,num3)
elif num3 > num1 and num3 > num2:
	print "{2} is greater than {1} and {0}".format(num1,num2,num3)
else:
	print "{0},{1} and {2} are all equal".format(num1,num2,num3)

'''
correct this logic
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-72$ python big.py 
please enter your num1:21
please enter your num2:21
please enter your num3:11
21,21 and 11 are all equal
'''
