#!/usr/bin/python
# break: Break takes you out of the loop.
# sys.exit: exit will take you out of the program.
# task1: restrict the number of attemts to 3.
# task2: generate a random number for number between 1 to 10.

import sys

whatsup = raw_input("do you want to play the game:")
if whatsup == 'n' or whatsup == 'no':
	sys.exit()

number = 7
#test = True

#while test:  # while works on truth of a condition
while True:
	answer = int(raw_input("guess your number:"))
	if answer > number:
		print "The number you guessed is slightly larger."
	elif answer < number:
		print "The number you guessed is slightly smaller."
	elif answer == number:
		print "Congo!! you won the game."
		break
		#test = print

False "Thank you visit us again."


'''
In [62]: import sys

In [63]: sys.exit?
Docstring:
exit([status])

Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).
Type:      builtin_function_or_method

In [64]: 
'''