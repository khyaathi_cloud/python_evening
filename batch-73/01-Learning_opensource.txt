Opensource vs Proprietory


Proprietory
------------
Any organization or owner owning a software/product.

ex: msoffice,windows

Product A   -> company A
Us(customer)-> requirements ( email to company/social media)
            -> we will get back to you.

* customer retention.
* Frustrated customers.

Opensource
------------

Product A(GPL)   -> company A(100) -> publish the source code in internet
Us(customer)-> requirements ( email to company/social media)
            -> we will get back to you.

Guy/Gal B(java/python/.net/ruby) -> source code product A + modifications
		  -> product B -> published the source code in internet
		  -> newsgroups/social media ( guys we have 5 modification)
		  -> reviews (vgood,good,bad,poor) 

Fast forward six months
------------------------
Product A -> (A.3) (100 + 400 ) modification - homogenous

* customer retention.
* Marketing.
* cost cutting.

1 bug -> 1dev(1L) + 1QA(1L) - 8Cr

code:
homogenous/hetrogenous

Money
------
* Training
* Support ( platinum,gold,silver,bronze)

Licenses
---------
GPL - GNU General Public License
* Any modification done to the GPL license product should go back to GPL.
* should be published.
OSF -> Open Source Foundation( 1960)

Principle
----------
* FREEdom to modify a product is called opensource.
* There is no piracy, you modify and redistribute.
* Huge support from community.

ex:

operating system:ubuntu,fedora,centos
programming language:python,R,ruby
Web frameworks: django,flask,bottle,angularjs,nodejs,reactjs
database: mariadb,mongodb,cassendra


* ubuntu os
* oracle virtualbox
* python tools
* we will install.






* we can modify the source code.
* Free of cost.
* source code is open to all.
* How are they making money?
* how about license?