In [10]: my_string
Out[10]: 'python'

In [11]: # A string is a sequence of values.

In [12]: # p  y  t  h  o  n

In [13]: # 0  1  2  3  4  5  # +ve indexing or left to right.

In [14]: # -6 -5 -4 -3 -2 -1 # -ve indexing or right to left.

In [15]: my_string[2]
Out[15]: 't'

In [16]: my_string[-4]
Out[16]: 't'

In [17]: my_string[2]="T"
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-17-406e9919ef9c> in <module>()
----> 1 my_string[2]="T"

TypeError: 'str' object does not support item assignment

In [18]: # slicking

In [19]: my_string[0:2]
Out[19]: 'py'

In [20]: my_string[0:3] # 0 till 3
Out[20]: 'pyt'

In [21]: my_string[3:6]
Out[21]: 'hon'

In [22]: my_string[:3] # 0 till 3
Out[22]: 'pyt'

In [23]: my_string[3:]
Out[23]: 'hon'

In [25]: my_string
Out[25]: 'python'

In [26]: my_string[:]
Out[26]: 'python'

In [27]: # [beginning:ending:steps]

In [28]: my_string[::1] # my default step can be ignored and is one
Out[28]: 'python'

In [29]: my_string[::2]
Out[29]: 'pto'

In [30]: my_string[1::2]
Out[30]: 'yhn'

In [31]: # p  y  t  h  o  n

In [32]: # 0  1  2  3  4  5  # +ve indexing or left to right.

In [33]: # -6 -5 -4 -3 -2 -1 # -ve indexing or right to left.

In [34]: my_string[0:3:2]
Out[34]: 'pt'

In [35]: my_string[0:3:2] + my_string[5]
Out[35]: 'ptn'

In [37]: # p  y  t  h  o  n

In [38]: # 0  1  2  3  4  5  # +ve indexing or left to right.

In [39]: # -6 -5 -4 -3 -2 -1 # -ve indexing or right to left.

In [40]: my_string
Out[40]: 'python'

In [41]: my_string[-6]
Out[41]: 'p'

In [42]: my_string[-6:-3]
Out[42]: 'pyt'

In [43]: my_string[-6:3]
Out[43]: 'pyt'

In [44]: my_string[-6:3:-1]
Out[44]: ''

In [45]: my_string[0:4]
Out[45]: 'pyth'

In [46]: my_string[0:4:-1]
Out[46]: ''

In [47]: my_string[4:0]
Out[47]: ''

In [48]: my_string[4:0:-1]
Out[48]: 'ohty'

In [49]: my_string[::-1]
Out[49]: 'nohtyp'

In [51]: # printing

In [52]: my_school="De Paul"

In [53]: another_school="st. xaviers"

In [54]: town="township"

In [55]: beach="blue"

In [56]: commute="bus"

In [59]: print "my school name is my_school"
my school name is my_school

In [60]: print 'my school name is my_school'
my school name is my_school

In [61]: print 'my school name is', my_school
my school name is De Paul

In [63]: print 'my school name is', my_school , "We had another school with name", another_school, 'i used to live in
    ...:  a ', town, 'we have a beach which has color', beach , 'we used to commute on', commute
my school name is De Paul We had another school with name st. xaviers i used to live in a  township we have a beach which has color blue we used to commute on bus

In [63]: print 'my school name is', my_school , "We had another school with name", another_school, 'i used to live in
    ...:  a ', town, 'we have a beach which has color', beach , 'we used to commute on', commute
my school name is De Paul We had another school with name st. xaviers i used to live in a  township we have a beach which has color blue we used to commute on bus

In [64]: # using typecasting

In [65]: # %s-string,%d-digit,%f-float,%r->raw

In [66]: print "my school name is %s.We had another school with name %s.We used to live in a %s.We have a beach which
    ...:  has color %s.we used to commute on %s" %(my_school,another_school,town,beach,commute)
my school name is De Paul.We had another school with name st. xaviers.We used to live in a township.We have a beach which has color blue.we used to commute on bus

In [67]: print "I love my school %s.My school name is %s" %(my_school,my_school)
I love my school De Paul.My school name is De Paul

In [68]: print "I love my school %s.My school name is %s" %(my_school,another_school)
I love my school De Paul.My school name is st. xaviers

In [69]: # format

In [70]: print "I love my school {}.My school name is {}".format(my_school,another_school)
I love my school De Paul.My school name is st. xaviers

In [71]: print "I love my school {0}.My school name is {0}".format(my_school,another_school)
I love my school De Paul.My school name is De Paul

In [72]: print "I love my school {ms}.My school name is {ans}".format(ms=my_school,ans=another_school)
I love my school De Paul.My school name is st. xaviers


In [74]: # input

In [75]: # raw_input and input

In [76]: # 2.x

In [77]: name = raw_input("please enter your name:")
please enter your name:kumar

In [78]: print name
kumar

In [79]: print type(name)
<type 'str'>

In [80]: roll_no = raw_input("please enter your roll:")
please enter your roll:21

In [81]: print roll_no
21

In [82]: print type(roll_no)
<type 'str'>

In [83]: 1 + 1
Out[83]: 2

In [84]: '1' + '1'
Out[84]: '11'

In [85]: # int,float,str

In [87]: roll_no = int(raw_input("please enter your roll:"))
please enter your roll:21

In [88]: print roll_no,type(roll_no)
21 <type 'int'>

In [87]: roll_no = int(raw_input("please enter your roll:"))
please enter your roll:21

In [88]: print roll_no,type(roll_no)
21 <type 'int'>

In [89]: roll_no = int(raw_input("please enter your roll:"))
please enter your roll:twenty
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-89-4b8bf265e5d8> in <module>()
----> 1 roll_no = int(raw_input("please enter your roll:"))

ValueError: invalid literal for int() with base 10: 'twenty'

In [90]: 

In [91]: # input -> 2.x and 3.x

In [92]: num = input("please enter your age:")
please enter your age:21

In [93]: print num,type(num)
21 <type 'int'>

In [94]: # raw_input will always take values as strings

In [95]: # input will take strings as string and numbers as numbers.

In [96]: 
