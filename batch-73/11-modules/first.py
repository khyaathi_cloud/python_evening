#!/usr/bin/python

version = 2.0

def my_add(a,b):
	''' this is for adding two numbers/strings '''
	return a + b

def my_sub(a,b):
	''' this is for substraction of two numbers '''
	return a - b

def my_div(a,b):
	''' this is for division of two numbers '''
	return a/b

def my_multi(a,b):
	''' this is for multiplication of numbers '''
	return a * b

## Main program
if __name__ == '__main__':    ## everything below this will not be imported.
	print "MISSILE LAUNCHED"