
# coding: utf-8

# In[8]:


# exception handling


# In[9]:


import exceptions as e
print dir(e)


# In[2]:


# case I
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[3]:


# case II
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[4]:


# case III
num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
result = num1/num2
print result


# In[5]:


# try.. except .. else .. finally
# try -> blocks which are mandatory and can create exceptions.
# except -> it will handle your exceptions
# else -> if all is true in try.. execute else.


# In[7]:


try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except:
    print "Make sure your enter numbers,denominator is non-zero."
else:
    print result


# In[10]:


# case II:
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except (ValueError,ZeroDivisionError):
    print "Make sure your enter numbers,denominator is non-zero."
else:
    print result


# In[12]:


# case III:
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "Make sure your enter numbers"
except ZeroDivisionError:
    print "Denominator is non-zero."
else:
    print result


# In[ ]:


# try.. except .. else .. finally
# try -> blocks which are mandatory and can create exceptions.
# except -> it will handle your exceptions
# else -> if all is true in try.. execute else.
# finally -> 
# case I: when i gave all valid values -> try -> else -> finally
# case II: when i gave an invalid value(handled by exception)
# -> try .. except .. finally
# # case III: when i gave an invalid value(not handled by exception)
# -> try..finally..error


# In[16]:


# case I
try:
    num1 = int(raw_input("please enter the num1:"))
    num2 = int(raw_input("please enter the num2:"))
    result = num1/num2
except ValueError:
    print "Make sure your enter numbers"
else:
    print result
finally:
    print "All is well."
    # file closure
    # socket closure
    # resourse closure


# In[17]:


# raise exceptions.
# we can only raise exceptions which are part of exception class.


# In[19]:


pint "hello world"


# In[18]:


raise SyntaxError


# In[20]:


raise SyntaxError,"please clean your glasses."


# In[21]:


raise santosh


# In[22]:


class santosh(Exception):
    pass


# In[23]:


raise santosh,"i am back"

