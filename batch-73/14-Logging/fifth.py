#!/usr/bin/python
# crontab in linux.
# scheduler in windows.

import logging
from subprocess import Popen,PIPE

# logging.basicConfig(filename="mydisk.log",filemode='a',level=logging.DEBUG,
# 					format='%(asctime)s - %(name)s -  %(levelname)s - %(message)s',
# 					datefmt='%c')

# main program

# Loggers expose the interface that application code directly uses.
# ex: logger is the name of your application.
# logging : root
# Handlers send the log records (created by loggers) to the appropriate destination.
# ex: file,console,mail,whatapp
# ex: filename="mydisk.log",filemode='a'
# Filters provide a finer grained facility for determining which log records to output.
# ex: filtering your output.
# ex: level=logging.DEBUG
# Formatters specify the layout of log records in the final output.
# ex: format
# format='%(asctime)s - %(name)s -  %(levelname)s - %(message)s',datefmt='%c'

# create logger
logger = logging.getLogger('Disk Monitor') # logger
logger.setLevel(logging.DEBUG)          # filter at logger level.

# create console handler and set level to debug
ch = logging.FileHandler('mydisk.log')  # filehandler
ch.setLevel(logging.DEBUG)    # filter at handler level.

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)  # handler and formatter

# add ch to logger
logger.addHandler(ch)       # logger and handler.


## main

P1=Popen(['df','-h','/'],stdout=PIPE)
P2=Popen(['tail','-n','-1'],stdin=P1.stdout,stdout=PIPE) 
disk_size=int(P2.communicate()[0].split()[4].split('%')[0])

if disk_size < 50:
	logger.info("The disk is looking healthy at {}.".format(disk_size))
elif disk_size < 80:
	logger.warning("The disk seems to grow in size - {}".format(disk_size))
elif disk_size < 90:
	logger.error("The disk is puking some errors - {}".format(disk_size))
elif disk_size < 100:
	logger.critical("The application has gone down - {}".format(disk_size))