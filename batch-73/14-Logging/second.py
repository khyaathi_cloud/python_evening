#!/usr/bin/python
# logging.debug < logging.info < logging.warning < logging.error < logging.critical
# logging.basicConfig?
# logging.Formatter?
# google.com -> man date

#The default level is WARNING, which means that only events of this level and above will be tracked, 
#unless the logging package is configured to do otherwise.


import logging

logging.basicConfig(filename="myapp.log",filemode='a',level=logging.DEBUG,
					format='%(asctime)s - %(name)s -  %(levelname)s - %(message)s',
					datefmt='%c')

logging.debug("This is an debug message")
logging.info("This is an information message.")
logging.warning("This is an warning message.")
logging.error("This is an error message.")
logging.critical("This is an critical message.")

'''
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-73/14-Logging$ python first.py 
WARNING:root:This is an warning message.
ERROR:root:This is an error message.
CRITICAL:root:This is an critical message.
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/python-evening/batch-73/14-Logging$ 
'''