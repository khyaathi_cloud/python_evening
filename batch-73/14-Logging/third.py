#!/usr/bin/python
# crontab in linux.
# scheduler in windows.

import logging
from subprocess import Popen,PIPE

logging.basicConfig(filename="mydisk.log",filemode='a',level=logging.DEBUG,
					format='%(asctime)s - %(name)s -  %(levelname)s - %(message)s',
					datefmt='%c')

# main program

# df -h / | tail -n 1 | awk '{print $5}'|sed -e 's#%##g'
#disk_size = int(raw_input("please enter the disk size:"))

P1=Popen(['df','-h','/'],stdout=PIPE)
P2=Popen(['tail','-n','-1'],stdin=P1.stdout,stdout=PIPE) 
disk_size=int(P2.communicate()[0].split()[4].split('%')[0])

if disk_size < 50:
	logging.info("The disk is looking healthy at {}.".format(disk_size))
elif disk_size < 80:
	logging.warning("The disk seems to grow in size - {}".format(disk_size))
elif disk_size < 90:
	logging.error("The disk is puking some errors - {}".format(disk_size))
elif disk_size < 100:
	logging.critical("The application has gone down - {}".format(disk_size))