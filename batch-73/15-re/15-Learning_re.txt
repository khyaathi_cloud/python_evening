re - regular expressions or regex

In [1]: import re

In [2]: re.
          re.compile     re.escape      re.L           re.purge       re.sre_compile re.T           re.VERBOSE     
          re.copy_reg    re.findall     re.LOCALE      re.S           re.sre_parse   re.template    re.X           
          re.DEBUG       re.finditer    re.M           re.Scanner     re.sub         re.TEMPLATE                   
          re.DOTALL      re.I           re.match       re.search      re.subn        re.U                          
          re.error       re.IGNORECASE  re.MULTILINE   re.split       re.sys         re.UNICODE                    



In [1]: import re

In [2]: my_string="python"

In [3]: re.match?
Signature: re.match(pattern, string, flags=0)
Docstring:
Try to apply the pattern at the start of the string, returning
a match object, or None if no match was found.
File:      /usr/lib/python2.7/re.py
Type:      function

In [4]: # pattern is a subset of your string

In [5]: re.match("py",my_string)
Out[5]: <_sre.SRE_Match at 0x7fc35eb2a100>

In [6]: re.match("yt",my_string)

In [7]: re.match("pyt",my_string)
Out[7]: <_sre.SRE_Match at 0x7fc35eb2a440>

In [8]: re.match("pyth",my_string)
Out[8]: <_sre.SRE_Match at 0x7fc35eb2a308>

In [9]: re.match("pytho",my_string)
Out[9]: <_sre.SRE_Match at 0x7fc35eb2a370>

In [10]: re.match("python",my_string)
Out[10]: <_sre.SRE_Match at 0x7fc35eb2a4a8>

In [11]: re.match("pythons",my_string)

In [12]: re.match("Python",my_string)

In [13]: re.match("Python",my_string,re.I)
Out[13]: <_sre.SRE_Match at 0x7fc35eb2a510>

In [15]: re.search?
Signature: re.search(pattern, string, flags=0)
Docstring:
Scan through string looking for a match to the pattern, returning
a match object, or None if no match was found.
File:      /usr/lib/python2.7/re.py
Type:      function

In [16]: my_string="python"

In [17]: re.search("yt",my_string)
Out[17]: <_sre.SRE_Match at 0x7fc35eb2a850>

In [18]: re.search("ty",my_string)

In [19]: re.search("Yt",my_string)

In [20]: re.search("Yt",my_string,re.I)
Out[20]: <_sre.SRE_Match at 0x7fc35eb2a9f0>

In [21]: 

In [22]: my_sentence="python is a great language."

In [23]: re.match("python",my_sentence)
Out[23]: <_sre.SRE_Match at 0x7fc35eb2ad30>

In [24]: re.match("language",my_sentence)

In [25]: re.search("language",my_sentence)
Out[25]: <_sre.SRE_Match at 0x7fc35eb2af38>

In [27]: # special characters

In [28]: # ^ -> caret -> beginning of a sentence or line.

In [29]: # $ -> dollar -> end of a sentence or line.

In [30]: # . -> dot -> one character.

In [31]: my_sentence="python is a great language."

In [32]: my_sentence1="one of the great languages in python."

In [33]: re.match("python",my_sentence)
Out[33]: <_sre.SRE_Match at 0x7fc35ea37098>

In [34]: re.match("python",my_sentence1)

In [35]: re.search("python",my_sentence1)
Out[35]: <_sre.SRE_Match at 0x7fc35ea371d0>

In [36]: re.search("^python",my_sentence1)

In [37]: re.search("python$",my_sentence1)

In [27]: # special characters

In [28]: # ^ -> caret -> beginning of a sentence or line.

In [29]: # $ -> dollar -> end of a sentence or line.

In [30]: # . -> dot -> one character.

In [31]: my_sentence="python is a great language."

In [32]: my_sentence1="one of the great languages in python."

In [33]: re.match("python",my_sentence)
Out[33]: <_sre.SRE_Match at 0x7fc35ea37098>

In [34]: re.match("python",my_sentence1)

In [35]: re.search("python",my_sentence1)
Out[35]: <_sre.SRE_Match at 0x7fc35ea371d0>

In [36]: re.search("^python",my_sentence1)

In [37]: re.search("python$",my_sentence1)

In [38]: re.search("python.$",my_sentence1)
Out[38]: <_sre.SRE_Match at 0x7fc35ea37370>

In [39]: re.match("python.$",my_sentence1)

In [41]: my_string="python"

In [42]: # . -> dot -> one character.

In [43]: re.match('...',my_string)
Out[43]: <_sre.SRE_Match at 0x7fc35ea37648>

In [44]: re.match('...',my_string).group()
Out[44]: 'pyt'

In [45]: re.search('...$',my_string).group()
Out[45]: 'hon'

In [46]: re.search('.....',my_string).group()
Out[46]: 'pytho'

In [47]: my_string="python\n"

In [48]: re.match('.......',my_string)

In [49]: # \n,\r,\t are not considered by .

In [50]: re.match('.......',my_string,re.DOTALL)
Out[50]: <_sre.SRE_Match at 0x7fc35ea37780>

In [51]: re.match('.......',my_string,re.DOTALL).group()
Out[51]: 'python\n'

In [53]: # globbing characters

In [54]: # * -> zero or more characters.

In [55]: # + -> one or more characters.

In [56]: # ? -> zero or one characters.

In [57]: my_film1 = "ashique"

In [58]: my_film2 = "aashique"

In [59]: my_film3 = "aaashique"

In [60]: my_film4 = "shique"

In [61]: ## compile

In [62]: reg = re.compile('a*shique',re.I)

In [63]: reg.search?
Docstring:
search(string[, pos[, endpos]]) --> match object or None.
Scan through string looking for a match, and return a corresponding
match object instance. Return None if no position in the string matches.
Type:      builtin_function_or_method

In [64]: re.search?
Signature: re.search(pattern, string, flags=0)
Docstring:
Scan through string looking for a match to the pattern, returning
a match object, or None if no match was found.
File:      /usr/lib/python2.7/re.py
Type:      function

In [65]: reg
Out[65]: re.compile(r'a*shique', re.IGNORECASE)

In [66]: print reg
<_sre.SRE_Pattern object at 0x7fc35fc39630>

In [67]: reg.search(my_film1)
Out[67]: <_sre.SRE_Match at 0x7fc35ea37c60>

In [68]: reg.search(my_film2)
Out[68]: <_sre.SRE_Match at 0x7fc35ea37bf8>

In [69]: reg.search(my_film3)
Out[69]: <_sre.SRE_Match at 0x7fc35ea379f0>

In [70]: reg.search(my_film4)
Out[70]: <_sre.SRE_Match at 0x7fc35ea37cc8>

In [72]: my_film1 = "ashique"

In [73]: my_film2 = "aashique"

In [74]: my_film3 = "aaashique"

In [75]: my_film4 = "shique"

In [76]: reg1 = re.compile('a+shique',re.I)

In [77]: reg1.match(my_film1)
Out[77]: <_sre.SRE_Match at 0x7fc35ea37e68>

In [78]: reg1.match(my_film2)
Out[78]: <_sre.SRE_Match at 0x7fc35ea37d98>

In [79]: reg1.match(my_film3)
Out[79]: <_sre.SRE_Match at 0x7fc35ea37ed0>

In [80]: reg1.match(my_film4)

In [82]: my_film1 = "ashique"

In [83]: my_film2 = "aashique"

In [84]: my_film3 = "aaashique"

In [85]: my_film4 = "shique"

In [86]: reg2 = re.compile('a?shique',re.I)

In [87]: reg2.match(my_film1)
Out[87]: <_sre.SRE_Match at 0x7fc35ea37f38>

In [88]: reg2.search(my_film2)
Out[88]: <_sre.SRE_Match at 0x7fc35ea66098>

In [89]: reg2.match(my_film2)

In [90]: reg2.search(my_film3)
Out[90]: <_sre.SRE_Match at 0x7fc35ea66100>

In [91]: reg2.search(my_film4)
Out[91]: <_sre.SRE_Match at 0x7fc35ea66168>

In [92]: 

In [93]: # anchors

In [94]: # {n} -> repeated n number of times.

In [95]: # {n,} -> repeated n or more times.

In [96]: # {n,m} -> repeated between n or m times.

In [97]: my_film1 = "ashique"

In [98]: my_film2 = "aashique"

In [99]: my_film3 = "aaashique"

In [100]: my_film4 = "shique"

In [101]: reg3 = re.compile('a{2}shique',re.I)

In [102]: reg3.search(my_film1)

In [103]: reg3.search(my_film2)
Out[103]: <_sre.SRE_Match at 0x7fc35ea66780>

In [104]: reg3.search(my_film3)
Out[104]: <_sre.SRE_Match at 0x7fc35ea666b0>

In [105]: reg3.search(my_film4)

In [107]: # task

In [108]: my_word1 = "<shiva>protaganist</shiva>"

In [109]: my_word2 = "<sati>protaganist</sati>"

In [110]: my_word3 = "<nandi>actor</nandi>"

In [111]: my_word4 = "<somras>magicportion</somras>"

In [112]: reg4 = re.compile('<.*>',re.I)

In [113]: reg4.search(my_word1)
Out[113]: <_sre.SRE_Match at 0x7fc35ea66cc8>

In [114]: reg4.search(my_word1).group()
Out[114]: '<shiva>protaganist</shiva>'

In [115]: reg4.search(my_word2).group()
Out[115]: '<sati>protaganist</sati>'

In [116]: reg4.search(my_word3).group()
Out[116]: '<nandi>actor</nandi>'

In [117]: reg4.search(my_word4).group()
Out[117]: '<somras>magicportion</somras>'

In [118]: 

In [107]: # task

In [108]: my_word1 = "<shiva>protaganist</shiva>"

In [109]: my_word2 = "<sati>protaganist</sati>"

In [110]: my_word3 = "<nandi>actor</nandi>"

In [111]: my_word4 = "<somras>magicportion</somras>"

In [112]: reg4 = re.compile('<.*>',re.I)

In [113]: reg4.search(my_word1)
Out[113]: <_sre.SRE_Match at 0x7fc35ea66cc8>

In [114]: reg4.search(my_word1).group()
Out[114]: '<shiva>protaganist</shiva>'

In [115]: reg4.search(my_word2).group()
Out[115]: '<sati>protaganist</sati>'

In [116]: reg4.search(my_word3).group()
Out[116]: '<nandi>actor</nandi>'

In [117]: reg4.search(my_word4).group()
Out[117]: '<somras>magicportion</somras>'

In [118]: # *,+,? -> greedy -> maximal matching

In [119]: # *?,+?,?? -> mininimal matching

In [120]: reg4 = re.compile('<.*?>',re.I)

In [121]: reg4.search(my_word1).group()
Out[121]: '<shiva>'

In [122]: reg4.search(my_word2).group()
Out[122]: '<sati>'

In [123]: reg4.search(my_word3).group()
Out[123]: '<nandi>'

In [124]: reg4.search(my_word4).group()
Out[124]: '<somras>'

In [129]: # character sets

In [130]: # [a-z] -> having characters a to z.

In [131]: # [0-9] -> having numbers 0 to 9.

In [132]: # [^a-z] -> characters staring with a to z.

In [133]: # ^[a-z] -> characters not starting with a to z.

In [134]: # [.*?+] -> will behave like symbols.

In [135]: my_sentence = "is python a great language?"

In [136]: re.match('[a-z]',my_sentence)
Out[136]: <_sre.SRE_Match at 0x7fc35ea08a58>

In [137]: re.match('[a-z]',my_sentence).group()
Out[137]: 'i'

In [138]: re.match('[a-z]+',my_sentence).group()
Out[138]: 'is'

In [139]: re.match('[a-z]+\s',my_sentence).group()
Out[139]: 'is '

In [140]: re.match('[a-z]+\s+',my_sentence).group()
Out[140]: 'is '

In [144]: re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+',my_sentence).group()
Out[144]: 'is python a great '

In [145]: re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+',my_sentence).group()
Out[145]: 'is python a great language'

In [146]: re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+[?]',my_sentence).group()
Out[146]: 'is python a great language?'

In [147]: re.match('([a-z]+\s+){4}[a-z]+[?]',my_sentence).group()
Out[147]: 'is python a great language?'

In [148]: re.match('(\w+\s+){4}[a-z]+[?]',my_sentence).group()
Out[148]: 'is python a great language?'

In [150]: re.match('(\w+\s+){4}[a-z]+[?]',my_sentence).group()
Out[150]: 'is python a great language?'

In [151]: # VERBOSE

In [152]: re.match('''
     ...: (\w+\s+)  # A word and a space
     ...: {4}       # above patter repeated 4 times
     ...: [a-z]+    # a new word again ->total 4 + 1=5
     ...: [?]       # followed by a question(?)
     ...: ''',my_sentence,(re.X|re.I))
Out[152]: <_sre.SRE_Match at 0x7fc35e993288>

In [153]: re.match('''
     ...: (\w+\s+)  # A word and a space
     ...: {4}       # above patter repeated 4 times
     ...: [a-z]+    # a new word again ->total 4 + 1=5
     ...: [?]       # followed by a question(?)
     ...: ''',my_sentence,(re.X|re.I)).group()
Out[153]: 'is python a great language?'

In [154]: 

In [155]: # grouping

In [156]: re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+[?]',my_sentence).group()
Out[156]: 'is python a great language?'

In [157]: re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+[?]',my_sentence).group(0)
Out[157]: 'is python a great language?'

In [158]: re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z])+\s+[a-z]+[?]',my_sentence).group()
Out[158]: 'is python a great language?'

In [159]: re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z])+\s+[a-z]+[?]',my_sentence).group(1)
Out[159]: 'python'

In [160]: re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z])+\s+[a-z]+[?]',my_sentence).group(2)
Out[160]: 't'

In [161]: re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z]+)\s+[a-z]+[?]',my_sentence).group(2)
Out[161]: 'great'

In [162]: re.match('[a-z]+\s+([a-z]+)\s+[a-z]+\s+([a-z]+)\s+[a-z]+[?]',my_sentence).groups()
Out[162]: ('python', 'great')

In [163]: re.match('[a-z]+\s+(?P<py>[a-z]+)\s+[a-z]+\s+(?P<la>[a-z]+)\s+[a-z]+[?]',my_sentence).group()
Out[163]: 'is python a great language?'

In [164]: re.match('[a-z]+\s+(?P<py>[a-z]+)\s+[a-z]+\s+(?P<la>[a-z]+)\s+[a-z]+[?]',my_sentence).group('py')
Out[164]: 'python'

In [165]: re.match('[a-z]+\s+(?P<py>[a-z]+)\s+[a-z]+\s+(?P<la>[a-z]+)\s+[a-z]+[?]',my_sentence).group('la')
Out[165]: 'great'

In [166]: re.match('[a-z]+\s+(?P<py>[a-z]+)\s+[a-z]+\s+(?P<la>[a-z]+)\s+[a-z]+[?]',my_sentence).group('py','la')
Out[166]: ('python', 'great')

In [167]: re.match('[a-z]+\s+(?P<py>[a-z]+)\s+[a-z]+\s+(?P<la>[a-z]+)\s+[a-z]+[?]',my_sentence).groups()
Out[167]: ('python', 'great')

In [169]: # match,search,findall

In [170]: my_string="python is a great language.\npython is my first love.\npython is very easy to learn"

In [171]: my_string
Out[171]: 'python is a great language.\npython is my first love.\npython is very easy to learn'

In [172]: #\r

In [173]: print my_string
python is a great language.
python is my first love.
python is very easy to learn

In [174]: #%s

In [175]: re.match("python",my_string)
Out[175]: <_sre.SRE_Match at 0x7fc35ea1d718>

In [176]: re.match("python",my_string).group()
Out[176]: 'python'

In [177]: re.search("python",my_string)
Out[177]: <_sre.SRE_Match at 0x7fc35ea1d850>

In [178]: re.search("python",my_string).group()
Out[178]: 'python'

In [179]: re.findall?
Signature: re.findall(pattern, string, flags=0)
Docstring:
Return a list of all non-overlapping matches in the string.

If one or more groups are present in the pattern, return a
list of groups; this will be a list of tuples if the pattern
has more than one group.

Empty matches are included in the result.
File:      /usr/lib/python2.7/re.py
Type:      function

In [180]: re.findall("python",my_string)
Out[180]: ['python', 'python', 'python']

In [182]: my_string="python is a great language.\npython is my first love.\npython is very easy to learn"

In [183]: re.findall("python",my_string)
Out[183]: ['python', 'python', 'python']

In [184]: re.findall("^python",my_string)
Out[184]: ['python']

In [185]: my_string
Out[185]: 'python is a great language.\npython is my first love.\npython is very easy to learn'

In [186]: # multiline

In [187]: re.findall("^python",my_string,re.M)
Out[187]: ['python', 'python', 'python']

In [188]: 

