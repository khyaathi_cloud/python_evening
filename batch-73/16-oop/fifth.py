#!/usr/bin/python
# Abstraction

# Defined the Account class.
class Account:            # class/blueprint
	def __init__(self):   # constructor
		self.balance = 0  # data
	def my_deposit(self): # method
		self.balance = self.balance + 1000
		return self.balance
	def my_withdraw(self):# method
		self.balance = self.balance - 200
		return self.balance
	def my_balance(self):# method
		return "The balance in my account is - {}".format(self.balance)

# kushal
kushal = Account()  # kushal instance/objects
print kushal.my_balance()
kushal.my_deposit()
print kushal.my_balance()
kushal.my_withdraw()
print kushal.my_balance()

# Kethan
kethan = Account() # kethan is an instance/objects
print kethan.my_balance()
