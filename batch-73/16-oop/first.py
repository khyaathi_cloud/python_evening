#!/usr/bin/python

balance = 0

def my_deposit():
	global balance
	balance = balance + 1000
	return balance

def my_withdraw():
	global balance
	balance = balance - 200
	return balance

# kushal
print "Initial balance of kushal is {}".format(balance)
my_deposit()
print "Amount after depositing the money - {}".format(balance)
my_withdraw()
print "Amount after withdraw the money - {}".format(balance)

# Kethan
print "Initial balance of kethan is {}".format(balance)
my_deposit()
print "Amount after depositing the money - {}".format(balance)
my_withdraw()
print "Amount after withdraw the money - {}".format(balance)