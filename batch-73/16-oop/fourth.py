#!/usr/bin/python
# constructor -> special method/variable are created during instance creation.
# constructor -> this is the first function which gets loaded.
# my_balance -> method -> called explicitly.


class Account:
	def __init__(self):    # constructor/special method
		self.balance = 0    # data/variable
	def my_balance(self):  # method
		return "my account balance is - {}".format(self.balance)


# Deepak

print dir(Account) # ['__doc__', '__init__', '__module__', 'my_balance']
deepak = Account() # ['__doc__', '__init__', '__module__', 'balance', 'my_balance']
print dir(deepak)
print deepak
print deepak.balance # class variable
print deepak.my_balance()

# charan
charan = Account()
charan.balance = 1000
print charan.my_balance()

## i can still access the variable of a class outside
print Account.balance

'''
# case I:
print deepak.my_balance()
Traceback (most recent call last):
  File "fourth.py", line 16, in <module>
    print deepak.my_balance()
TypeError: my_balance() takes no arguments (1 given)

# case II:
Traceback (most recent call last):
  File "fourth.py", line 16, in <module>
    print deepak.my_balance()
  File "fourth.py", line 7, in my_balance
    print "my account balance is - {}".format(balance)
NameError: global name 'balance' is not defined


'''




