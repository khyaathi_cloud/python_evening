#!/usr/bin/python
# __add__ -> poloymorphic

    """
    Rational Numbers with support for arthmetic operations.

        >>> a = RationalNumber(1, 2)
        >>> b = RationalNumber(1, 3)
        >>> a + b
        5/6
    """

class RationalNumber:
    def __init__(self, numerator, denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self, other):
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)

        n = self.n * other.d + self.d * other.n
        d = self.d * other.d
        return RationalNumber(n, d)


    def __str__(self):
        return "%s/%s" % (self.n, self.d)

    __repr__ = __str__


# Main
# case I
a = RationalNumber(1, 2) # a.n=1,a.d=2
b = RationalNumber(1, 3) # b.n=1,b.d=3
print a + b # a.__add__(b) => a.__add__(other)

# Case II
a = 2 # int
b = 3 # int
print a + b

# case III
a = "linux"
b = " rocks"
print a + b

# case IV
a = RationalNumber(1, 2) # 1/2
b = 2 # int => b = RationalNumber(2,1) # 2/1
print a + b 