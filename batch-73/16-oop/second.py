#!/usr/bin/python

def account():
 return {'balance':0}

def my_deposit(member):
	member['balance'] = member['balance'] + 1000
	return member['balance']

def my_withdraw(member):
	member['balance'] = member['balance'] - 200
	return member['balance']


# kushal
kushal = account()
print kushal # kushal = {'balance': 0}
my_deposit(kushal)
print "Amount after depositing the money - {}".format(kushal['balance'])
my_withdraw(kushal)
print "Amount after withdraw the money - {}".format(kushal['balance'])

# Kethan
Kethan = account()
print Kethan
my_deposit(Kethan)
print "Amount after depositing the money - {}".format(Kethan['balance'])
my_withdraw(Kethan)
print "Amount after withdraw the money - {}".format(Kethan['balance'])