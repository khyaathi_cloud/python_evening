#!/usr/bin/python
# Inhertiance
# Account -> parentclass/super class
# MinBalancedAccount -> childclass/sub class

# Defined the Account class.
class Account:            # class/blueprint
	def __init__(self):   # constructor
		self.balance = 0  # data
	def my_deposit(self,amount): # method
		self.balance = self.balance + amount
		return self.balance
	def my_withdraw(self,amount):# method
		self.balance = self.balance - amount
		return self.balance
	def my_balance(self):# method
		return "The balance in my account is - {}".format(self.balance)

class MinBalancedAccount(Account):
	def __init__(self):
		Account.__init__(self) # constructor has to be called explicitly
	def my_withdraw(self,amount):
		if self.balance - amount < 1000:
			return "time to call your daddy"
		else:
			return Account.my_withdraw(self,amount)


# kushal - working guy - ready credit
kushal = Account()  # kushal instance/objects
print kushal.my_balance()
kushal.my_deposit(5000)
print kushal.my_balance()
kushal.my_withdraw(2000)
print kushal.my_balance()
kushal.my_withdraw(2000)
print kushal.my_balance()
kushal.my_withdraw(2000)
print kushal.my_balance()

# Kethan - student
kethan = MinBalancedAccount() # kethan is an instance/objects
#print dir(kethan)
print "Amount for Kethan"
print kethan.my_balance()
kethan.my_deposit(5000)
print kethan.my_balance()
kethan.my_withdraw(2000)
print kethan.my_balance()
kethan.my_withdraw(2000)
print kethan.my_balance()
print kethan.my_withdraw(1000)
