#!/usr/bin/python
# class - bank account
# object - kushal bank account
# Account -> class
# Account() -> instance
# Account.balance -> class variables
# kushal -> instance
# kushal.balance -> instance variable
# kethan -> instance
# kethan.balance -> instance variable

class Account:   # class
	balance = 0  # class variable/data

print Account       # __main__.Account
print type(Account) # <type 'classobj'>/class
print type(Account()) # <type 'instance'>/object


# kushal
kushal = Account()
print kushal       # __main__.Account instance at 0x7f83248233f8>
print dir(kushal)  # ['__doc__', '__module__', 'balance']
print "kushal balance - {}".format(kushal.balance)
kushal.balance = 1000 # instance variable
print "kushal balance after - kushal.balance = 1000 -> {}".format(kushal.balance)

# kethan
kethan = Account()  # Account.balance
print "kethan balance - {}".format(kethan.balance)
kethan.balance=1000

# class variable
print "Account.balance=2000"
Account.balance = 2000  # i modified the class variable of Account class.

# charan
charan = Account()
print "charan balance - {}".format(charan.balance)  # 2000

print "kethan balance - {}".format(kethan.balance)
print "kushal balance - {}".format(kushal.balance)

## 
Account.interest = 0.08
print Account.interest
print dir(Account)
