# tuples
tuples are readonly lists.
gender - male,female,xyz
maps - latitute or longitude

In [1]: my_week = ('sun','mon','tue','wed','thus','fri','sat')

In [2]: print my_week,type(my_week)
('sun', 'mon', 'tue', 'wed', 'thus', 'fri', 'sat') <type 'tuple'>

In [3]: my_empty = ()

In [4]: print  my_empty,type(my_empty)
() <type 'tuple'>

In [5]: my_empty = tuple()

In [6]: print my_empty,type(my_empty)
() <type 'tuple'>

In [7]: my_string="python"

In [8]: print my_string,type(my_string)
python <type 'str'>

In [9]: my_string=("python")

In [10]: print my_string,type(my_string)
python <type 'str'>

In [11]: my_string=("python",)

In [12]: print my_string,type(my_string)
('python',) <type 'tuple'>

In [13]: my_string="python","linux","django","chef","puppet","ansible"

In [14]: print my_string,type(my_string)
('python', 'linux', 'django', 'chef', 'puppet', 'ansible') <type 'tuple'>

In [15]: my_string="python,linux,django,chef,puppet,ansible"

In [16]: print my_string,type(my_string)
python,linux,django,chef,puppet,ansible <type 'str'>

In [17]: 

In [18]: my_string="python,linux,django,chef,puppet,ansible"
KeyboardInterrupt

In [18]: my_string="python",

In [19]: print my_string
('python',)

In [20]: my_string="python"

In [21]: print my_string,type(my_string)
python <type 'str'>

In [22]: 

In [23]: # indexing - support 

In [24]: # slicing - support

In [25]: # iteration - support

In [26]: # in - support

In [27]: ## unpacking

In [28]: # packing - list,tuples

In [29]: my_week = ('sun','mon','tue','wed','thus','fri','sat')

In [30]: print my_week,type(my_week)
('sun', 'mon', 'tue', 'wed', 'thus', 'fri', 'sat') <type 'tuple'>

In [31]: a,b,c,d,e,f,g = my_week

In [32]: print a
sun

In [33]: print b
mon

In [34]: print c
tue

In [35]: print d
wed

In [36]: a,b,c,d,e,f = my_week
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-36-66b0ededdf2b> in <module>()
----> 1 a,b,c,d,e,f = my_week

ValueError: too many values to unpack

In [37]: a,b,c,d,e,f,g,h = my_week
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-37-ebc3706169d3> in <module>()
----> 1 a,b,c,d,e,f,g,h = my_week

ValueError: need more than 7 values to unpack

In [38]: 

In [39]: a,b,c,d,e,f = my_week[:6]

In [40]: print a
sun

In [41]: print b
mon

In [42]: print c
tue

In [43]: print d
wed

In [44]: print e
thus

In [45]: print f
fri

In [46]: 


In [47]: # lists vs tuples

In [48]: my_students = ['karthik','yesh','roja','sree','vamsi','srikant']

In [49]: my_subjects = ['python','django','flask','puppet','dockers','kuberates']

In [50]: ## beginning

In [51]: name='vamsi'

In [52]: name in my_students
Out[52]: True

In [53]: my_students.index(name)
Out[53]: 4

In [54]: my_subjects[my_students.index(name)]
Out[54]: 'dockers'

In [55]: if name in my_students:
   ....:     print "{} is going to give exam - {}".format(name,my_subjects[my_students.index(name)])
   ....:     
vamsi is going to give exam - dockers

In [56]: name="yesh"

In [57]: if name in my_students:
    print "{} is going to give exam - {}".format(name,my_subjects[my_students.index(name)])
   ....:     
yesh is going to give exam - django

In [58]: ## anti-climax

In [59]: ## principal

In [60]: my_students.sort()

In [61]: print my_students
['karthik', 'roja', 'sree', 'srikant', 'vamsi', 'yesh']

In [62]: print my_subjects
['python', 'django', 'flask', 'puppet', 'dockers', 'kuberates']

In [63]: ## D-day

In [64]: name="roja"

In [65]: if name in my_students:
    print "{} is going to give exam - {}".format(name,my_subjects[my_students.index(name)])
   ....:     
roja is going to give exam - django

In [67]: my_students = ['karthik','yesh','roja','sree','vamsi','srikant']

In [68]: my_subjects = ['python','django','flask','puppet','dockers','kuberates']

In [69]: my_exams = [('karthik','python'),('yesh','django'),('roja','flask'),('sree','puppet'),('vamsi','dockers'),('srikant','kubernates')]

In [70]: print my_exams
[('karthik', 'python'), ('yesh', 'django'), ('roja', 'flask'), ('sree', 'puppet'), ('vamsi', 'dockers'), ('srikant', 'kubernates')]

In [71]: for value in my_exams:
   ....:     print value
   ....:     
('karthik', 'python')
('yesh', 'django')
('roja', 'flask')
('sree', 'puppet')
('vamsi', 'dockers')
('srikant', 'kubernates')

In [72]: for stu,sub in my_exams:
   ....:     print stu,sub
   ....:     
karthik python
yesh django
roja flask
sree puppet
vamsi dockers
srikant kubernates

In [73]: name="karthik"

In [74]: for stu,sub in my_exams:
   ....:     if name == stu:
   ....:         print "{} is going to give exam - {}".format(stu,sub)
   ....:         
karthik is going to give exam - python

In [75]: name="srikant"

In [76]: for stu,sub in my_exams:
    if name == stu:
        print "{} is going to give exam - {}".format(stu,sub)
   ....:         
srikant is going to give exam - kubernates

In [77]: 

In [78]: # anticlimax

In [79]: 

In [79]: my_exams = [('karthik','python'),('yesh','django'),('roja','flask'),('sree','puppet'),('vamsi','dockers'),('srikant','kubernates')]

In [80]: my_exams.sort()

In [81]: print my_exams
[('karthik', 'python'), ('roja', 'flask'), ('sree', 'puppet'), ('srikant', 'kubernates'), ('vamsi', 'dockers'), ('yesh', 'django')]

In [82]: # the above is a list of tuples

In [83]: name="vamsi"

In [84]: for stu,sub in my_exams:
    if name == stu:
        print "{} is going to give exam - {}".format(stu,sub)
   ....:         
vamsi is going to give exam - dockers

In [85]: 


In [86]: my_exams = [('karthik','python'),('yesh','django'),('roja','flask'),('sree','puppet'),('vamsi','dockers'),('srikant','kubernates')]

In [87]: my_exams[3]
Out[87]: ('sree', 'puppet')

In [88]: my_exams[3][1]
Out[88]: 'puppet'

In [89]: my_exams[3][1]="python"
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-89-ac13ccc11831> in <module>()
----> 1 my_exams[3][1]="python"

TypeError: 'tuple' object does not support item assignment

In [90]: # each tuple is an element of the list

In [91]: my_exams[3]=("sree","python")

In [92]: print my_exams
[('karthik', 'python'), ('yesh', 'django'), ('roja', 'flask'), ('sree', 'python'), ('vamsi', 'dockers'), ('srikant', 'kubernates')]

In [93]: ( list(value) for value in my_exams )
Out[93]: <generator object <genexpr> at 0x7f86d242eaa0>

In [94]: [ list(value) for value in my_exams ]
Out[94]: 
[['karthik', 'python'],
 ['yesh', 'django'],
 ['roja', 'flask'],
 ['sree', 'python'],
 ['vamsi', 'dockers'],
 ['srikant', 'kubernates']]

In [95]: tuple([ list(value) for value in my_exams ])
Out[95]: 
(['karthik', 'python'],
 ['yesh', 'django'],
 ['roja', 'flask'],
 ['sree', 'python'],
 ['vamsi', 'dockers'],
 ['srikant', 'kubernates'])

In [96]: new_exams=tuple([ list(value) for value in my_exams ])

In [97]: 


In [98]: new_exams
Out[98]: 
(['karthik', 'python'],
 ['yesh', 'django'],
 ['roja', 'flask'],
 ['sree', 'python'],
 ['vamsi', 'dockers'],
 ['srikant', 'kubernates'])

In [99]: print new_exams
(['karthik', 'python'], ['yesh', 'django'], ['roja', 'flask'], ['sree', 'python'], ['vamsi', 'dockers'], ['srikant', 'kubernates'])

In [100]: print my_exams
[('karthik', 'python'), ('yesh', 'django'), ('roja', 'flask'), ('sree', 'python'), ('vamsi', 'dockers'), ('srikant', 'kubernates')]

In [101]: new_exams[3]
Out[101]: ['sree', 'python']

In [102]: new_exams[3][1]="django"

In [103]: print new_exams
(['karthik', 'python'], ['yesh', 'django'], ['roja', 'flask'], ['sree', 'django'], ['vamsi', 'dockers'], ['srikant', 'kubernates'])

In [104]: new_exams[3]=["sree","django"]
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-104-d6b6b0040457> in <module>()
----> 1 new_exams[3]=["sree","django"]

TypeError: 'tuple' object does not support item assignment

In [105]: 

In [109]: my_week
Out[109]: ('sun', 'mon', 'tue', 'wed', 'thus', 'fri', 'sat')

In [110]: my_week.
my_week.count  my_week.index  

In [110]: my_week.count?
Type:        builtin_function_or_method
String form: <built-in method count of tuple object at 0x7f86d3480910>
Docstring:   T.count(value) -> integer -- return number of occurrences of value

In [111]: my_week.count('tue'
   .....: )
Out[111]: 1

In [112]: my_week.index?
Type:        builtin_function_or_method
String form: <built-in method index of tuple object at 0x7f86d3480910>
Docstring:
T.index(value, [start, [stop]]) -> integer -- return first index of value.
Raises ValueError if the value is not present.

In [113]: my_week.index('fri')
Out[113]: 5

In [114]: 





