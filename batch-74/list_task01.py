#!/usr/bin/python
# output
# my_fruits = ['apple','banana','cherry','dates']
# my_dupli = ['apple','banana']

my_fruits = ['apple','banana','apple','banana','cherry','dates']

my_dupli = []

for fruit in my_fruits:
	if my_fruits.count(fruit) > 1:
		if fruit not in my_dupli:
			my_dupli.append(fruit)
		my_fruits.remove(fruit)

print my_fruits
print my_dupli