#!/usr/bin/python
# my_days = ['yesterday','today','tomorrow','dayafter']
# output:
# Yesterday
# TOday
# TOMorrow
# DAYAfter

my_days = ['yesterday','today','tomorrow','dayafter']
for day in my_days:
   # print day,my_days.index(day),day[my_days.index(day)],day[:my_days.index(day)]
   print day[:my_days.index(day) + 1].upper() + day[my_days.index(day) + 1:]
