khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75$ ipython
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
Type "copyright", "credits" or "license" for more information.

IPython 5.5.0 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: help()

Welcome to Python 2.7!  This is the online help utility.

If this is your first time using Python, you should definitely check out
the tutorial on the Internet at http://docs.python.org/2.7/tutorial/.

Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules.  To quit this help utility and
return to the interpreter, just type "quit".

To get a list of available modules, keywords, or topics, type "modules",
"keywords", or "topics".  Each module also comes with a one-line summary
of what it does; to list the modules whose summaries contain a given word
such as "spam", type "modules spam".

help> keyword

help> keywords

Here is a list of the Python keywords.  Enter any keyword to get more help.

and                 elif                if                  print
as                  else                import              raise
assert              except              in                  return
break               exec                is                  try
class               finally             lambda              while
continue            for                 not                 with
def                 from                or                  yield
del                 global              pass                

help> topics

Here is a list of available topics.  Enter any topic name to get more help.

ASSERTION           DEBUGGING           LITERALS            SEQUENCEMETHODS2
ASSIGNMENT          DELETION            LOOPING             SEQUENCES
ATTRIBUTEMETHODS    DICTIONARIES        MAPPINGMETHODS      SHIFTING
ATTRIBUTES          DICTIONARYLITERALS  MAPPINGS            SLICINGS
AUGMENTEDASSIGNMENT DYNAMICFEATURES     METHODS             SPECIALATTRIBUTES
BACKQUOTES          ELLIPSIS            MODULES             SPECIALIDENTIFIERS
BASICMETHODS        EXCEPTIONS          NAMESPACES          SPECIALMETHODS
BINARY              EXECUTION           NONE                STRINGMETHODS
BITWISE             EXPRESSIONS         NUMBERMETHODS       STRINGS
BOOLEAN             FILES               NUMBERS             SUBSCRIPTS
CALLABLEMETHODS     FLOAT               OBJECTS             TRACEBACKS
CALLS               FORMATTING          OPERATORS           TRUTHVALUE
CLASSES             FRAMEOBJECTS        PACKAGES            TUPLELITERALS
CODEOBJECTS         FRAMES              POWER               TUPLES
COERCIONS           FUNCTIONS           PRECEDENCE          TYPEOBJECTS
COMPARISON          IDENTIFIERS         PRINTING            TYPES
COMPLEX             IMPORTING           PRIVATENAMES        UNARY
CONDITIONAL         INTEGER             RETURNING           UNICODE
CONTEXTMANAGERS     LISTLITERALS        SCOPING             
CONVERSIONS         LISTS               SEQUENCEMETHODS1    

help> modules

Please wait a moment while I gather a list of all available modules...
--- truncated output --- -

cairosvg            jsonschema          regex               zdaemon
calendar            jupyter             reprlib             zinnia
certifi             jupyter_client      requests            zipapp
cffi                jupyter_console     requests_cache      zipfile
cftime              jupyter_core        requests_file       zipimport
cgi                 jwt                 requests_oauthlib   zlib
cgitb               keras               resource            zmq
chameleon           keras_applications  retrying            zodbpickle
chardet             keras_preprocessing rlcompleter         
Enter any module name to get more help.  Or, type "modules spam" to search
for modules whose name or summary contain the string "spam".

help> quit

You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)".  Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.


In [3]: my_string="python"

In [4]: print type(my_string)
<type 'str'>

In [5]: my_float=1.0

In [6]: print type(my_float)
<type 'float'>

In [7]: my_num = 1

In [8]: print type(my_num)
<type 'int'>

In [9]: # in python we dont need typecasting

In [10]: my_string.
          my_string.capitalize my_string.format     my_string.isupper    my_string.rindex     my_string.strip      
          my_string.center     my_string.index      my_string.join       my_string.rjust      my_string.swapcase   
          my_string.count      my_string.isalnum    my_string.ljust      my_string.rpartition my_string.title      
          my_string.decode     my_string.isalpha    my_string.lower      my_string.rsplit     my_string.translate  
          my_string.encode     my_string.isdigit    my_string.lstrip     my_string.rstrip     my_string.upper      
          my_string.endswith   my_string.islower    my_string.partition  my_string.split      my_string.zfill      
          my_string.expandtabs my_string.isspace    my_string.replace    my_string.splitlines                      
          my_string.find       my_string.istitle    my_string.rfind      my_string.startswith                      

In [3]: my_string="python"

In [4]: print type(my_string)
<type 'str'>

In [5]: my_float=1.0

In [6]: print type(my_float)
<type 'float'>

In [7]: my_num = 1

In [8]: print type(my_num)
<type 'int'>

In [9]: # in python we dont need typecasting

In [10]: my_string.upper?
Docstring:
S.upper() -> string

Return a copy of the string S converted to uppercase.
Type:      builtin_function_or_method

In [11]: my_string.upper()
Out[11]: 'PYTHON'

In [12]: my_string.center?
Docstring:
S.center(width[, fillchar]) -> string

Return S centered in a string of length width. Padding is
done using the specified fill character (default is a space)
Type:      builtin_function_or_method

In [13]: my_string.center(10,'*')
Out[13]: '**python**'

In [15]: my_float
Out[15]: 1.0

In [16]: my_float.
                   my_float.as_integer_ratio my_float.hex              my_float.real             
                   my_float.conjugate        my_float.imag                                       
                   my_float.fromhex          my_float.is_integer                                 

In [16]: my_num
Out[16]: 1

In [17]: my_num.
                 my_num.bit_length  my_num.imag        
                 my_num.conjugate   my_num.numerator   
                 my_num.denominator my_num.real        



khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75$ python
Python 2.7.12+ (default, Sep 17 2016, 12:08:02) 
[GCC 6.2.0 20160914] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> 
>>> my_string="python"
>>> 
>>> dir(my_string)
['__add__', '__class__', '__contains__', '__delattr__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__getslice__', '__gt__', '__hash__', '__init__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '_formatter_field_name_split', '_formatter_parser', 'capitalize', 'center', 'count', 'decode', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'index', 'isalnum', 'isalpha', 'isdigit', 'islower', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']
>>> 
>>> help(my_string.upper)

>>> 


In [5]: my_string="python"

In [6]: # p  y  t  h  o  n

In [7]: # 0  1  2  3  4  5  # +ve indexed or left to right

In [8]: # -6 -5 -4 -3 -2 -1 # -ve indexed or right to left

In [9]: ## indexing

In [10]: my_string[2]
Out[10]: 't'

In [11]: my_string[-1]
Out[11]: 'n'

In [12]: 

In [12]: # slicing

In [13]: # p  y  t  h  o  n

In [14]: # -6 -5 -4 -3 -2 -1 # -ve indexed or right to left

In [15]: # 0  1  2  3  4  5  # +ve indexed or left to right

In [16]: ## slicing

In [17]: # pyt -> 0 till 3 ( 3 not included)

In [18]: my_string[0:3]
Out[18]: 'pyt'
In [19]: my_string[3:7]
Out[19]: 'hon'

In [20]: my_string[:3]
Out[20]: 'pyt'

In [21]: my_string[3:]
Out[21]: 'hon'

In [22]: # concatination

In [23]: my_string[:3] + my_string[3:]
Out[23]: 'python'

In [24]: 

In [25]: # we cannot modify the value in a string

In [26]: my_string
Out[26]: 'python'

In [27]: my_string[0]
Out[27]: 'p'

In [28]: my_string[0]='T'
Traceback (most recent call last):

  File "<ipython-input-28-50967ec55ade>", line 1, in <module>
    my_string[0]='T'

TypeError: 'str' object does not support item assignment


In [29]: 


n [30]: # task1

In [31]: my_string
Out[31]: 'python'

In [32]: # output => Tython

In [33]: 'T' + my_string[1:]
Out[33]: 'Tython'

In [34]: my_new='T'

In [35]: my_new + my_string[1:]
Out[35]: 'Tython'

In [36]: my_string[2].upper() + my_string[1:]
Out[36]: 'Tython'

In [37]: my_string.replace?
Docstring:
S.replace(old, new[, count]) -> string

Return a copy of string S with all occurrences of substring
old replaced by new.  If the optional argument count is
given, only the first count occurrences are replaced.
Type:      builtin_function_or_method

In [38]: my_string.replace('p','T')
Out[38]: 'Tython'

In [39]: 

## homework: create a string and read all functions.

In [42]: # playing with numbers

In [43]: my_num=10

In [44]: 10 + 10
Out[44]: 20

In [45]: 20 + 20 / 2
Out[45]: 30

In [46]: (20 + 20) / 2
Out[46]: 20

In [47]: 25 % 2
Out[47]: 1

In [48]: 25 /2 
Out[48]: 12

In [49]: 25.0/2
Out[49]: 12.5

In [50]: # BODMAS

In [51]: 

## assignment - http://www.pythonchallenge.com/pc/def/map.html


In [54]: import math

In [55]: math.pow?
Docstring:
pow(x, y)

Return x**y (x to the power of y).
Type:      builtin_function_or_method

In [56]: math.pow(2,38)
Out[56]: 274877906944.0

In [57]: 

>>> my_school = "De Paul"
>>> another_school = "st. xaviers"
>>> my_town = "township"
>>> beach = "blue"
>>> commute = "bus"
>>> 
>>> # case I
... 
>>> print "my school name is " , my_school
my school name is  De Paul
>>> 
>>> print "my school name is my_school"
my school name is my_school
>>> 
>>> print ' my school name is my_school'
 my school name is my_school
>>> 

>>> print "My school name is ", my_school, 'Adjacent to our school we have ', another_school , 'I used to live in a small ', my_town, "we have a beach which is ", beach ,"in color", "we used to commute on ", commute
My school name is  De Paul Adjacent to our school we have  st. xaviers I used to live in a small  township we have a beach which is  blue in color we used to commute on  bus
>>> 


>>> print "my school name is " , my_school , "."
my school name is  De Paul .

>>> print "my school name is %s. I love my school %s" %(my_school,my_school)
my school name is De Paul. I love my school De Paul
>>> 
>>> print "my school name is %s.I love my school %s." %(my_school,another_school)
my school name is De Paul.I love my school st. xaviers.
>>> 

## case III
## format

>>> # format
... 
>>> my_week = "sunday\nmonday\ntuesday\nwednesday\nthursday\nfriday\nsaturday"
>>> print "%s" %(my_week)
sunday
monday
tuesday
wednesday
thursday
friday
saturday
>>> print "%r" %(my_week)
'sunday\nmonday\ntuesday\nwednesday\nthursday\nfriday\nsaturday'
>>> 
>>> # __repr__ and __str__


>>> print "my school name is {}.Adjancent to us we have {}".format(my_school,another_school)
my school name is De Paul.Adjancent to us we have st. xaviers
>>> print "my school name is {0}.Adjancent to us we have {1}".format(my_school,another_school)
my school name is De Paul.Adjancent to us we have st. xaviers
>>> print "my school name is {}.I love my school {}".format(my_school,another_school)
my school name is De Paul.I love my school st. xaviers
>>> print "my school name is {0}.I love my school {0}".format(my_school,another_school)
my school name is De Paul.I love my school De Paul
>>> 
>>> print "my school name is {ms}.I love my school {ms}".format(ms=my_school,ans=another_school)
my school name is De Paul.I love my school De Paul

>>> print "my school name is {!s}.I love my school {!s}".format(my_school,another_school)
my school name is De Paul.I love my school st. xaviers
>>> 
>>> # {} -> %r -> raw
... 

>>> ## how to take inputs
... 
>>> name = raw_input("please enter your name:")
please enter your name:kumar
>>> print name
kumar
>>> 
>>> print type(name)
<type 'str'>
>>> 
>>> num1 = raw_input("please enter the number1:")
please enter the number1:10
>>> print num1
10
>>> print type(num1)
<type 'str'>
>>> 
>>> # int(),str(),float()
... 
>>> num1 = int(raw_input("please enter the number1:")
... 
... )
please enter the number1:10
>>> print num1,type(num1)
10 <type 'int'>
>>> 
>>> # when you enter values using raw_input by default it takes it as string formate
... 
>>> 

# raw_input(2.x) , input(2.x and 3.x)


>>> num1 = input("please enter the num1:")
please enter the num1:10
>>> print num1,type(num1)
10 <type 'int'>
>>> 
>>> 

