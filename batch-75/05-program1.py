#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
task1: 
my_days = ['yesterday','today','tomorrow','dayafter']

output:
    yesterday 9
    today     5
    tomorrow  8
    dayafter  8
    
task2:

output:
    
Yesterday
TOday
TOMorrow
DAYAfter
'''
my_days = ['yesterday','today','tomorrow','dayafter']

# task1

for value in my_days:
    print value,len(value)
    
# task2
for value in my_days:
    #print value,my_days.index(value),value[my_days.index(value)]
    #print my_days.index(value),value[0:my_days.index(value) + 1]
    print value[0:my_days.index(value) + 1].upper() + value[my_days.index(value) + 1:]
