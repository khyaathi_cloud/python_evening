#!/usr/bin/env python
# -*- coding: utf-8 -*-
# soft copy and deep copy
# garbage collection

my_fruits = ['apple','apple','banana','apple','cherry','banana']

#my_fruits = ['banana','apple','cherry','banana']
#my_fruits[:]=['apple','apple','banana','apple','cherry','banana']
# my_fruits[:] -> deep copy of my_fruits in memory.
'''
output:
my_fruits = ['apple','banana','cherry']
my_dupli = ['apple','banana']
'''

my_dupli = []
for value in my_fruits[:]:
    if my_fruits.count(value) > 1:
        if value not in my_dupli:
            my_dupli.append(value)
        my_fruits.remove(value)
        
print my_fruits
print my_dupli
        


