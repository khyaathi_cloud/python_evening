#!/usr/bin/python

version=2.0

def my_add(a,b):
	''' This is for addition of two numbers or strings '''
	return a + b

def my_sub(a,b):
	''' This is for substraction of two numbers '''
	if a > b:
		return a - b
	else:
		return b - a

def my_multi(a,b):
	''' This is for multipication of two numbers '''
	return a * b

def my_div(a,b):
	''' This is for division of two numbers '''
	return a/b

# main

if __name__  == '__main__':  # everything above this get imported.
	print "MISSILE IS ON."   # everyting from this line gets run.