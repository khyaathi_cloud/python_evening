#!/usr/bin/python
# my_add and f.my_add not clasing each other namespace.

import sys
sys.path.insert(0,'/home/khyaathi/Documents/bit-tuxfux/python-evening/batch-75/08-Modules/extra')
import first as f

def my_add(a,b):
	''' This is for addition two numbers '''
	a = int(a)
	b = int(b)
	return a + b

if __name__  == '__main__':
	print "Addition of two number is {}".format(my_add(11,22))
	print "Addition of two strings is {}".format(f.my_add("linux","rocks"))