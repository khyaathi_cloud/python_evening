
# coding: utf-8

# In[ ]:

# exception handing
# getting wrong input leading to error. how to take care of those errors is called exception handling.
# try..except..else..finally


# In[2]:

import exceptions as e


# In[3]:

print dir(e)


# In[ ]:

# case I

num1 = int(raw_input("please enter the number1:"))
num2 = int(raw_input("please enter the number2:"))
result = num1/num2
print result


# In[4]:

# error 1
num1 = int(raw_input("please enter the number1:"))
num2 = int(raw_input("please enter the number2:"))
result = num1/num2
print result


# In[4]:

# error2
num1 = int(raw_input("please enter the number1:"))
num2 = int(raw_input("please enter the number2:"))
result = num1/num2
print result


# In[5]:

# try..except..else..finally
# try -> block of code which can pump errors.
# except -> what to do when we see errors.
# else -> if try block has no errors do the else block.


# In[8]:

# case I
try:
    num1 = int(raw_input("please enter the number1:"))
    num2 = int(raw_input("please enter the number2:"))
    result = num1/num2
except:
    print "please present a valid number.Make sure your denominator is non-zero"
else:
    print result


# In[9]:

# case II

try:
    num1 = int(raw_input("please enter the number1:"))
    num2 = int(raw_input("please enter the number2:"))
    result = num1/num2
except (ZeroDivisionError,ValueError):
    print "please present a valid number.Make sure your denominator is non-zero"
else:
    print result


# In[2]:

# case III
try:
    num1 = int(raw_input("please enter the number1:"))
    num2 = int(raw_input("please enter the number2:"))
    result = num1/num2
except ZeroDivisionError:
    print "Make sure your denominator is non-zero"
except ValueError:
    print "please present a valid number."
else:
    print result


# In[6]:

#case IV:finally
# try -> block of code which can pump errors.
# except -> what to do when we see errors.
# else -> if try block has no errors do the else block.
# finally -> given than you have not taken care of an exception.. finally comes into picture.
# case I: if all values are valid -> try..else..finally
# case II: if we give invalid value..handled by exceptions -> try..except..finally
# case III: if we give invalid value..not handled by exceptions -> try..finally..bombed with a error.
try:
    num1 = int(raw_input("please enter the number1:"))
    num2 = int(raw_input("please enter the number2:"))
    result = num1/num2
except ValueError:
    print "please present a valid number."
else:
    print result
finally:
    print "all is well"
    # database.close()
    # socket.close()
    # file.close()


# In[7]:

## raise
# you can only rasie exception class objects.
# given that your exception is part of exception class.
raise SyntaxError


# In[8]:

raise SyntaxError,"please clean your glasses!!!"


# In[9]:

# santosh is not part of exception class.
raise santosh


# In[11]:

# santosh a part of exception class.
class santosh(Exception):
    pass


# In[12]:

raise santosh,"hey i am back!!!"


# In[ ]:



