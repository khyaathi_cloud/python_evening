#!/usr/bin/python
# Level -> DEBUG,INFO,WARNING,ERROR,CRITICAL
# Function -> debug(),info(),warning(),error(),critical()
# default level is warning and above.

import logging as l

l.debug("This is a debug message.")
l.info("This is an information message.")
l.warning("This is a warning message.")
l.error("This is an error message.") 
l.critical("This is a critical message.")

'''
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ ls
11-Learning_Logging.txt  first.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ python first.py 
WARNING:root:This is a warning message.
ERROR:root:This is an error message.
CRITICAL:root:This is a critical message.
'''
