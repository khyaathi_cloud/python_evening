#!/usr/bin/python
# Level -> DEBUG,INFO,WARNING,ERROR,CRITICAL
# Function -> debug(),info(),warning(),error(),critical()
# default level is warning and above.
# logging.basicConfig
# logging.Formatter

import logging as l

l.basicConfig(filename="myapp.log",filemode='a',
			  level=l.DEBUG,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s ',
			  datefmt='%c')

# modes
# r -> read -> to read a file.
# w -> write -> to write to a file.
# given there is no file it create a file writes into it.
# given there is a file with content it get cleaned up and made empty.
# a -> append -> to append to a file.

l.debug("This is a debug message.")
l.info("This is an information message.")
l.warning("This is a warning message.")
l.error("This is an error message.") 
l.critical("This is a critical message.")

'''
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ ls
11-Learning_Logging.txt  first.py
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ python first.py 
WARNING:root:This is a warning message.
ERROR:root:This is an error message.
CRITICAL:root:This is a critical message.
'''
