#!/usr/bin/python
# Level -> DEBUG,INFO,WARNING,ERROR,CRITICAL
# Function -> debug(),info(),warning(),error(),critical()
# default level is warning and above.
# logging.basicConfig
# logging.Formatter
# crontab - linux
# scheduler - windows

import logging as l
from subprocess import Popen,PIPE

l.basicConfig(filename="mydisk.log",filemode='a',
			  level=l.DEBUG,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s ',
			  datefmt='%c')

# modes
# r -> read -> to read a file.
# w -> write -> to write to a file.
# given there is no file it create a file writes into it.
# given there is a file with content it get cleaned up and made empty.
# a -> append -> to append to a file.
# df -h /|tail -n 1|awk '{print $5}'|sed 's#%##g'
#disk_size = int(raw_input("please enter disk size:"))

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_size = int(p2.communicate()[0].split()[-2].split('%')[0])
#print disk_size
#print disk_size[0].split()[-2].split('%')[0]

if disk_size <= 50:
	l.info("My disk is looking health at {}.".format(disk_size))
elif disk_size < 70:
	l.warning("My disk is becoming fat {}.".format(disk_size))
elif disk_size < 90:
	l.error("Your disk is puking errors {}.".format(disk_size)) 
elif disk_size <= 100:
	l.critical("your application is sleeping now.".format(disk_size))

'''
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ python third.py 
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$  df -h /
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        45G   11G   32G  25% /
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ dd if=/dev/zero of=bigfile count=1024 bs=1M
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 5.85601 s, 183 MB/s
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ du -sh bigfile
1.1G	bigfile
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ df -h /
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        45G   12G   31G  28% /
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ python third.py 
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ rm -f bigfile 
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ python third.py 
khyaathi@khyaathipython:~/Documents/git_repos/bitbucket/python-evenings/batch-75/11-Logging$ 

## basic logging
- we can only log to console and file.
- we cannot know the application name.
- This is not production ready.
'''
