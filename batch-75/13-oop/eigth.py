#!/usr/bin/python
# https://github.com/tuxfux-hlp/Python-examples
# https://rszalski.github.io/magicmethods/
# https://anandology.com/python-practice-book/object_oriented_programming.html#special-class-methods

class RationalNumber:
    """
    Rational Numbers with support for arthmetic operations.

        >>> a = RationalNumber(1, 2)
        >>> b = RationalNumber(1, 3)
        >>> a + b
        5/6
    """
    def __init__(self, numerator, denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self, other):
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)

        n = self.n * other.d + self.d * other.n  # a.n * b.d + a.d * b.n = 3 + 2 = 5
        d = self.d * other.d                     # a.d * b.d = 6
        return RationalNumber(n, d)              # RationalNumber(5,6) => self.n=5,self.d=6

    def __str__(self):
        return "%s/%s" % (self.n, self.d)

    __repr__ = __str__


## main
a = RationalNumber(1, 2) # a.n = 1,a.d = 2
b = RationalNumber(1, 3) # b.n = 1,b.d = 3
print a + b  # a.__add__(b)

c = 10  # integer class
d  = 20
print c + d

e = RationalNumber(1, 2)
f = 20 # RationalNumber(20, 1)
print e + f