#!/usr/bin/python

class Cup:
   def __init__(self,color):
       self.color = color
       self.__content = None  # private 
   def fill(self,content):
       self.__content = content 
   def empty(self):
       self.__content = None
   def diplay(self):
       return self.__content


## main
green = Cup('green')
print dir(green)
print green.__content
#green.__content = "green tea"
#print dir(green)
# print green.__content

'''
In [272]: class Cup:
   def __init__(self,color):
       self.color = color
       self.__content = None  # private 
   def fill(self,content):
       self.__content = content 
   def empty(self):
       self.__content = None
   def diplay(self):
       return self.__content
   .....: 

In [273]: green = Cup("green")

In [274]: dir(green)
Out[274]: 
['_Cup__content',
 '__doc__',
 '__init__',
 '__module__',
 'color',
 'diplay',
 'empty',
 'fill']

In [275]: green.color
Out[275]: 'green'

In [276]: green.__content
---------------------------------------------------------------------------
AttributeError                            Traceback (most recent call last)
<ipython-input-276-219ded4221ce> in <module>()
----> 1 green.__content

AttributeError: Cup instance has no attribute '__content'

In [277]: green.fill("tea")

In [278]: green.diplay()
Out[278]: 'tea'
'''