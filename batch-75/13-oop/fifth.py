#!/usr/bin/python
# __init__ is a constuctor which gets called when instance is created.
# constructor is a special function which get called automatically when we create a instance.
# every instance is set of variable and function playing together.

class Account:
	def __init__(self):  # constructer/speical method
		self.balance = 0 # instance variable
	def my_deposit(self):    # methods
		self.balance = self.balance + 1000
		return self.balance
	def my_withdraw(self):
		self.balance = self.balance - 200
		return self.balance
	def my_balance(self):
		return "{}".format(self.balance)


print dir(Account)    # namespace of class
print dir(Account())  # namespace of instance

# gowri
gowri = Account()
#print dir(gowri)
print "balance of gowri - {}".format(gowri.my_balance())
gowri.my_deposit()
print "AFTER DEPOSIT: balance of gowri - {}".format(gowri.my_balance())
gowri.my_withdraw()
print "AFTER WITHDRAW: balance of gowri - {}".format(gowri.my_balance())

# srujan
srujan = Account()
print "balance of srujan - {}".format(srujan.my_balance())

#print gowri.balance

