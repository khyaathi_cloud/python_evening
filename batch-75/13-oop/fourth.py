#!/usr/bin/python
# self is a placeholder variable.
# self represents the instance.
# class.<variable> # class variable
# instance.variable # instace variable
# within a function - self.variable -> instance variable

class Account:
	balance = 0            #  data/variable
	def my_balance(self): # method
		return "my balance is {}".format(self.balance)


print dir(Account)

# gowri

gowri = Account()
print dir(gowri)
print gowri.my_balance()
gowri.balance=2000
print gowri.my_balance()

# srujan
srujan = Account()
print dir(srujan)
print srujan.my_balance()

'''
# error1 : put a self inside the function parameters
Traceback (most recent call last):
  File "fourth.py", line 12, in <module>
    print gowri.my_balance()
TypeError: my_balance() takes no arguments (1 given)

# error2
Traceback (most recent call last):
  File "fourth.py", line 12, in <module>
    print gowri.my_balance()
  File "fourth.py", line 6, in my_balance
    return "my balance is {}".format(balance)
NameError: global name 'balance' is not defined


'''