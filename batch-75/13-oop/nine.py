#!/usr/bin/python
# public

class Cup:
    def __init__(self):
        self.color = None
        self.content = None

    def fill(self, beverage):
        self.content = beverage

    def empty(self):
        self.content = None


##
red = Cup()
print dir(red)
red.content = "cofee"
print red.content