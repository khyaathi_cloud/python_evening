#!/usr/bin/python

def my_account():
	return {'balance':0}

def my_deposit(account):
	account['balance'] = account['balance'] + 1000
	return account['balance']

def my_withdraw(account):
	account['balance'] = account['balance'] - 200
	return account['balance']

def my_balance(account):
	return account['balance']

# Main
gowri = my_account() # {'balance':0} # gowri['balance']
print "balance of gowri - {}".format(my_balance(gowri))
my_deposit(gowri)
print "AFTER DEPOSIT: balance of gowri - {}".format(my_balance(gowri))
my_withdraw(gowri)
print "AFTER WITHDRAW: balance of gowri - {}".format(my_balance(gowri))

srujan = my_account()
print "balance of srujan - {}".format(my_balance(srujan))
