#!/usr/bin/python
# child class.
# Exception is my parent class.
# InvalidAgeException is my child class.

# customized exceptions
class InvalidAgeException(Exception):
	def __init__(self,age):
		self.age = age


def validate_age(age):
	if age >= 18:
		print "buddy welcome to the movie!!"
	else:
		raise InvalidAgeException(age)


# main
if __name__ == '__main__':
	age = int(raw_input("please enter the age:"))
	try:
		validate_age(age)
	except Exception as e:
		print "buddy you still need to grow up - {}".format(e.age)
	else:
		pass
	finally:
		pass

