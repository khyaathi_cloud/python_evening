#!/usr/bin/python
# __init__ is a constuctor which gets called when instance is created.
# constructor is a special function which get called automatically when we create a instance.
# every instance is set of variable and function playing together.

class Account:
	def __init__(self):  # constructer/speical method
		self.balance = 0 # instance variable
	def my_deposit(self,amount):    # methods
		self.balance = self.balance + amount
		return self.balance
	def my_withdraw(self,amount):
		self.balance = self.balance - amount
		return self.balance
	def my_balance(self):
		return "{}".format(self.balance)

class MinBalanceAccount(Account):
	def __init__(self):             # this step is a must for constructer.
		Account.__init__(self)      # you can never inherit a construftor.
	def my_withdraw(self,amount):
		if self.balance - amount < 1000:
			print "Buddy!! time to call you daddy"
		else:
			Account.my_withdraw(self,amount)


# gowri - employee
gowri = Account()
#print dir(gowri)
print "balance of gowri - {}".format(gowri.my_balance())
gowri.my_deposit(amount=5000)
print "AFTER DEPOSIT: balance of gowri -> {}".format(gowri.my_balance())
gowri.my_withdraw(amount=2000)
print "AFTER WITHDRAW: balance of gowri -> {}".format(gowri.my_balance())
gowri.my_withdraw(amount=2000)
print "AFTER WITHDRAW: balance of gowri -> {}".format(gowri.my_balance())
gowri.my_withdraw(amount=2000)
print "AFTER WITHDRAW: balance of gowri -> {}".format(gowri.my_balance())

# srujan - student - 1000
srujan = MinBalanceAccount()
print "balance of srujan - {}".format(srujan.my_balance())
srujan.my_deposit(amount=5000)
print "balance of srujan - {}".format(srujan.my_balance())
srujan.my_withdraw(amount=2000)
print "AFTER WITHDRAW: balance of srujan -> {}".format(srujan.my_balance())
srujan.my_withdraw(amount=2000)
print "AFTER WITHDRAW: balance of srujan -> {}".format(srujan.my_balance())
srujan.my_withdraw(amount=2000)

