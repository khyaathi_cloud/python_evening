#!/usr/bin/python
# 


class Cup:
    def __init__(self):
        self.color = None
        self._content = None # protected variable

    def fill(self, beverage):
        self._content = beverage

    def empty(self):
        self._content = None


#

yellow = Cup()
print dir(yellow)
yellow._content = "tea"
print yellow._content