#!/usr/bin/python
# variables - class variables,instance variables

# class/blueprint
class Account:      # class
     balance = 0    # data/variable


print Account       	# __main__.Account
print type(Account) 	# <type 'classobj'> /class
print type(Account())   # <type 'instance'>/objects
print Account.balance   # accessing the variable using a class .
print Account().balance # accessing the variable using an instance.

# gowri -> instance/object
gowri = Account()
print dir(gowri)
print gowri.balance # gowri balance


# srujan - Instance/object
srujan = Account()
print dir(srujan)
print srujan.balance # srujan balance
srujan.balance = 2000
print srujan.balance

# make a modificaiton to class variable
Account.balance = 3000
print Account.balance # 3000      # class variables
print gowri.balance   # 0 -> 3000 # class variables
print srujan.balance  # 2000      # instance variable