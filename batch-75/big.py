#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  8 09:17:45 2018

@author: khyaathi
"""

num1 = int(raw_input("please enter the num1:"))
num2 = int(raw_input("please enter the num2:"))
num3 = int(raw_input("please enter the num3:"))

if num1 > num2 and num1 > num3:
    print "{0} is  greater than {1} and {2}".format(num1,num2,num3)
elif num2 > num1 and num2 > num3:
    print "{1} is  greater than {2} and {0}".format(num1,num2,num3)
elif num3 > num1 and num3 > num2:
    print "{2} is  greater than {1} and {0}".format(num1,num2,num3)
else:
    print "{0},{1},{2} are all equal".format(num1,num2,num3)

'''
# set the logic right for the above program.
please enter the num1:10

please enter the num2:10

please enter the num3:2
10,10,2 are all equa
'''