#!/usr/bin/python 
# -*- coding: utf-8 -*-
# break: break is for breaking out of the loop.
# exit: exit out of the program.
##
'''
tfd1: make sure your number of chances are just 3.
   2: generate a random number for number variable.(google)

'''

import sys

answer = raw_input("Do  you want to play the game?")
if answer == 'no':
    sys.exit(0)

print "welcome to the game"

number = 7
#test = True

#while test:  # truth of a condition
while True:
    guess_num = input("please enter a num:")
    if guess_num == number:
        print "Congo!!! you guessed the right number"
        #test=False
        break
    elif guess_num < number:
        print "Buddy!! you guessed a smalled number"
    elif guess_num > number:
        print "Buddy!! you guessed a larger number"
        
        
print "Please come back!!!"

'''
outer loop
 inner loop
   break

In [42]: import sys

In [43]: sys.exit?
Docstring:
exit([status])

Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).
Type:      builtin_function_or_method

In [44]: 

'''